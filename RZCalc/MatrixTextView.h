//
//  MatrixTextView.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/16/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Core.h"
#import "RZCustomKeyboard.h"

@interface MatrixTextView : UITextField <UITextFieldDelegate, CustomKeyboardDelegate>

@property (nonatomic) NSInteger i;
@property (nonatomic) NSInteger j;

@end
