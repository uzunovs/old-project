//
//  RZCalcViewController.h
//  RZCalc
//
//  Created by Hristo Uzunov on 9/30/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "Core.h"
#import "MatrixViewController.h"
#import "AboutView.h"

typedef enum {
    Tangent = 370,
    TangentArc = 20,
    Cotangent = 30,
    CotangentArc = 40,
    Hyperbolic = 50,
    Neper = 60,
    Sine = 70,
    SineArc = 80,
    Cosine = 90,
    CosineArc = 100,
    Factorial = 110,
    Logarithm = 120,
    LogarithmNatural = 130,
    LogarithmBaseTen = 140,
    Power2 = 150,
    Power3 = 160,
    Power = 170,
    SquareRoot2 = 180,
    SquareRoot3 = 190,
    Addition = 200,
    Subtraction = 210,
    Multiplication = 220,
    Division = 230,
    OpenBracket = 240,
    CloseBracket = 250,
    PowerOf = 260,
    PiNumber = 270,
    DecimalPoint = 280,
    SineHyperbolic = 290,
    SineHyperbolicArc = 300,
    CosineHyperbolic = 310,
    CosineHyperbolicArc = 320,
    TangentHyperbolic = 330,
    TangentHyperbolicArc = 340,
    CotangentHyperbolic = 350,
    CotangentHyperbolicArc = 360,
    ChangeSign = 380,
    CommaSign = 390,
    Mod = 400
    }FunctionType;

typedef enum {
    Binary = 2,
    Octal = 8,
    Decimal = 10,
    Hexadecimal = 16
}Notations;

@interface RZCalcViewController : UIViewController <UIScrollViewDelegate, AboutViewDelegate>

@end
