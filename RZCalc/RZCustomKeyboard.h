//
//  RZCustomKeyboard.h
//  RZCalc
//
//  Created by Hristo Uzunov on 1/26/14.
//  Copyright (c) 2014 Hristo Uzunov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomKeyboardDelegate <NSObject>

-(void)input:(NSString*)string; //for capturing input from the numeric keys
-(void)deleteInput; //for deleting input

@end

@interface RZCustomKeyboard : UIView

@property (nonatomic, weak) id <CustomKeyboardDelegate> keyboardDelegate;

@end
