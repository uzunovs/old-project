//
//  RZCalcViewController.m
//  RZCalc
//
//  Created by Hristo Uzunov on 9/30/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "RZCalcViewController.h"
#import "AttributedStringStructure.h"

#define CELL_WIDTH [self.view bounds].size.width/5
#define CELL_HEIGHT [self.view bounds].size.height/11
#define SUPERSCRIPT_ARC @"\u207B\u00B9"
#define SUPERSCRIPT_TWO @"\u00B2"
#define SUPERSCRIPT_THREE @"\u00B3"
#define SUPERSCRIP_ATTRIBUTED_ELEMENT @"1"
#define SUBSCRIPT_ATTRIBUTED_ELEMENT @"-1"
#define NON_ATTRIBUTED_ELEMENT @"0"

@interface RZCalcViewController ()
@property (strong, nonatomic) IBOutlet UILabel *arcTgLabel;
@property (strong, nonatomic) IBOutlet UILabel *arcCotgLabel;
@property (strong, nonatomic) IBOutlet UILabel *piLabel;
@property (strong, nonatomic) IBOutlet UILabel *arcSinLabel;
@property (strong, nonatomic) IBOutlet UILabel *arcCosLabel;
@property (strong, nonatomic) IBOutlet UILabel *logarithmicLabel;
@property (strong, nonatomic) IBOutlet UILabel *logarithmTenLabel;
@property (strong, nonatomic) IBOutlet UILabel *modLabel;
@property (strong, nonatomic) IBOutlet UILabel *expLabel;
@property (strong, nonatomic) IBOutlet UILabel *powerThreeLabel;
@property (strong, nonatomic) IBOutlet UILabel *rootThreeLabel;
@property (strong, nonatomic) IBOutlet UILabel *decimalPointLabel;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *octaButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *binaryButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *hexaButtons;

@property (strong, nonatomic) IBOutlet UIButton *powerTwoButton;
@property (strong, nonatomic) IBOutlet UIButton *tgButton;
@property (strong, nonatomic) IBOutlet UIButton *cotgButton;
@property (strong, nonatomic) IBOutlet UIButton *eButton;
@property (strong, nonatomic) IBOutlet UIButton *sinButton;
@property (strong, nonatomic) IBOutlet UIButton *cosButton;
@property (strong, nonatomic) IBOutlet UIButton *factorielButton;
@property (strong, nonatomic) IBOutlet UIButton *changeSignButton;
@property (strong, nonatomic) IBOutlet UIButton *equalsButton;
@property (strong, nonatomic) IBOutlet UIButton *degButton;
@property (strong, nonatomic) IBOutlet UIButton *complexButton;
@property (strong, nonatomic) IBOutlet UIButton *scientificButton;
@property (strong, nonatomic) IBOutlet UIButton *shiftButton;
@property (strong, nonatomic) IBOutlet UIButton *rootTwoButton;
@property (strong, nonatomic) IBOutlet UIButton *hyperbolicButton;
@property (strong, nonatomic) IBOutlet UIButton *backspaceButton;
@property (strong, nonatomic) IBOutlet UIButton *lnButton;
@property (strong, nonatomic) IBOutlet UIButton *notationButton;
@property (strong, nonatomic) IBOutlet UIButton *decimalPointButton;
@property (strong, nonatomic) IBOutlet UIButton *optionsButton;
@property (strong, nonatomic) IBOutlet UIButton *clearInputButton;

@property (strong, nonatomic) IBOutlet UIScrollView *formulaInputScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *resultOutputScrollView;
@property (strong, nonatomic) RZStack *attributeHolder;
@property (strong, nonatomic) NSString *scientificString;
@property (strong, nonatomic) NSString *resultString;
@property (strong, nonatomic) NSArray *notations;
@property (strong, nonatomic) AboutView *about;

@property (nonatomic) BOOL isNumericInput;
@property (nonatomic) BOOL isClosingBracket;
@property (nonatomic) BOOL isAns;
@property (nonatomic) BOOL isDegrees;
@property (nonatomic, strong) NSArray *hexaTitles;
@property (nonatomic, strong) NSArray *decTitles;
@property (nonatomic, strong) NSDictionary *mainDictionary;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *resizableButtons;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *movableLabels;


- (IBAction)delete:(id)sender;
- (IBAction)tangentInput:(id)sender;
- (IBAction)numberInput:(id)sender;
- (IBAction)basicFunctionInut:(id)sender;
- (IBAction)equals;
- (IBAction)cosineInput:(id)sender;
- (IBAction)sineInput:(id)sender;
- (IBAction)cotangentInput:(id)sender;
- (IBAction)hyperbolicInput:(id)sender;
- (IBAction)neperNumberInput:(id)sender;
- (IBAction)powerOfTwoInput:(id)sender;
- (IBAction)squareRootBaseTwoInput:(id)sender;
- (IBAction)activateOptions;
- (IBAction)shiftFunctions:(id)sender;
- (IBAction)factorialInput:(id)sender;
- (IBAction)logarithmInput:(id)sender;
- (IBAction)changeNotation:(id)sender;
- (IBAction)changeSign:(id)sender;
- (IBAction)changeToRad:(UIButton *)sender;
- (IBAction)complexToReal:(UIButton*)sender;

@end

@implementation RZCalcViewController

-(NSDictionary*)mainDictionary
{
    if (!_mainDictionary) {
        _mainDictionary =  @{@"370": @"tg",
                             @"20": @"tg-1",
                             @"30": @"cotg",
                             @"40": @"cotg-1",
                             @"50": @"hyp",
                             @"60": @"e",
                             @"70": @"sin",
                             @"80": @"sin-1",
                             @"90": @"cos",
                             @"100": @"cos-1",
                             @"110": @"!",
                             @"120": @"log$$",
                             @"130": @"ln",
                             @"140": @"lg",
                             @"150": @"^2",
                             @"160": @"^3",
                             @"170": @"^",
                             @"180": @"@2@√",
                             @"190": @"@3@√",
                             @"200": @"+",
                             @"210": @"-",
                             @"220": @"×",
                             @"230": @"÷",
                             @"240": @"(",
                             @"250": @")",
                             @"260": @"^",
                             @"270": @"π",
                             @"280": @".",
                             @"290": @"sinh",
                             @"300": @"sinh-1",
                             @"310": @"cosh",
                             @"320": @"cosh-1",
                             @"330": @"tgh",
                             @"340": @"tgh-1",
                             @"350": @"cotgh",
                             @"360": @"cotgh-1",
                             @"380": @"±",
                             @"390": @","
                             };
    }
    
    return _mainDictionary;
}

-(NSArray*)hexaTitles
{
    if (!_hexaTitles) {
        _hexaTitles = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", nil];
    }
    
    return _hexaTitles;
}

-(NSArray*)decTitles
{
    if (!_decTitles) {
        _decTitles = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", nil];
    }
    
    return _decTitles;
}

-(RZStack*)attributeHolder
{
    if (!_attributeHolder) {
        _attributeHolder = [[RZStack alloc] init];
    }
    
    return _attributeHolder;
}

-(void)setScientificString:(NSString *)scientificString
{
    _scientificString = scientificString;
}

-(void)setResultString:(NSString *)resultString
{
    _resultString = resultString;
}

-(AboutView*)about
{
    if (!_about) {
        CGRect frame = [[UIScreen mainScreen] bounds];
        frame.origin.x += 5;
        frame.origin.y += 5;
        frame.origin.y = - (frame.origin.y + frame.size.height);
        frame.size.width -= 10;
        frame.size.height -= 10;
        _about = [[AboutView alloc] initWithFrame:frame];
        _about.delegate = self;
    }
    
    return _about;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect bounds = [self.view bounds];
    if ((bounds.size.width > 480 || bounds.size.height > 480) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self changeFrames];
    }
    self.scientificString = @"";
    self.isNumericInput = NO;
    self.isClosingBracket = NO;
    self.isAns = NO;
    NSMutableAttributedString *tempString;
    self.backspaceButton.enabled = NO;
    
    tempString = [[NSMutableAttributedString alloc] initWithString:@"tg-1"];
    [tempString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:NSMakeRange([tempString length] - 2, 2)];
    self.arcTgLabel.attributedText = tempString;
    tempString = [[NSMutableAttributedString alloc] initWithString:@"cotg-1"];
    [tempString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:NSMakeRange([tempString length] - 2, 2)];
    self.arcCotgLabel.attributedText = tempString;
    tempString = [[NSMutableAttributedString alloc] initWithString:@"sin-1"];
    [tempString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:NSMakeRange([tempString length] - 2, 2)];
    self.arcSinLabel.attributedText = tempString;
    tempString = [[NSMutableAttributedString alloc] initWithString:@"loga"];
    [tempString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"-1" range:NSMakeRange([tempString length] - 1, 1)];
    self.logarithmicLabel.attributedText = tempString;
    tempString = [[NSMutableAttributedString alloc] initWithString:@"cos-1"];
    [tempString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:NSMakeRange([tempString length] - 2, 2)];
    self.arcCosLabel.attributedText = tempString;
    tempString = [[NSMutableAttributedString alloc] initWithString:@"x3"];
    [tempString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:NSMakeRange([tempString length] - 1, 1)];
    self.powerThreeLabel.attributedText = tempString;
    [self.powerTwoButton setTitle:@"x\u00B2" forState:UIControlStateNormal];
    self.factorielButton.selected = NO;
    
    UILongPressGestureRecognizer *press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(takeAnswer:)];
    press.minimumPressDuration = 0.5;
    [self.equalsButton addGestureRecognizer:press];
    
    self.shiftButton.selected = NO;
    self.resultString = @"0";
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.resultString];
    [self displayInputFromString:attrString withSource:NO];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    self.notations = @[[NSNumber numberWithInteger:Binary], [NSNumber numberWithInteger:Octal], [NSNumber numberWithInteger:Decimal], [NSNumber numberWithInteger:Hexadecimal]];
    self.scientificString = @"";
    
    if ([self.degButton.titleLabel.text isEqualToString:@"DEG"]) {
        self.isDegrees = YES;
    }
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(decimalLongPress:)];
    lpgr.minimumPressDuration = 0.5;
    
    [self.decimalPointButton addGestureRecognizer:lpgr];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changeFrames
{
    CGRect frame;
    NSInteger count = [self.resizableButtons count];
    --count;
    for (NSInteger i = count; i >= 0; --i) {
        if (i <= count && i > count - 5) {
            frame = ((UIButton*)self.resizableButtons[i]).frame;
            frame.size.height += 10;
            ((UIButton*)self.resizableButtons[i]).frame = frame;
        }
        else if (i <= count - 5 && i > count - 10)
        {
            frame = ((UIButton*)self.resizableButtons[i]).frame;
            frame.origin.y += 10;
            frame.size.height += 13;
            ((UIButton*)self.resizableButtons[i]).frame = frame;
        }
        else if (i <= count - 10 && i > count - 15)
        {
            frame = ((UIButton*)self.resizableButtons[i]).frame;
            frame.origin.y += 23;
            frame.size.height += 13;
            ((UIButton*)self.resizableButtons[i]).frame = frame;
        }
        else if (i <= count - 15 && i > count - 20)
        {
            frame = ((UIButton*)self.resizableButtons[i]).frame;
            frame.origin.y += 36;
            frame.size.height += 13;
            ((UIButton*)self.resizableButtons[i]).frame = frame;
        }
        else if (i <= count - 20 && i > count - 25)
        {
            frame = ((UIButton*)self.resizableButtons[i]).frame;
            frame.origin.y += 49;
            frame.size.height += 13;
            ((UIButton*)self.resizableButtons[i]).frame = frame;
        }
        else if (i <= count - 25 && i > count - 30)
        {
            frame = ((UIButton*)self.resizableButtons[i]).frame;
            frame.origin.y += 62;
            frame.size.height += 13;
            ((UIButton*)self.resizableButtons[i]).frame = frame;
        }
        else if (i <= count - 30 && i > count - 35)
        {
            frame = ((UIButton*)self.resizableButtons[i]).frame;
            frame.origin.y += 75;
            frame.size.height += 13;
            ((UIButton*)self.resizableButtons[i]).frame = frame;
        }
    }
    
    count = [self.movableLabels count];
    --count;
    for (NSInteger i = count; i >= 0; --i) {
        if (i <= count && i > count - 5) {
            frame = ((UILabel*)self.movableLabels[i]).frame;
            frame.origin.y += 10;
            ((UILabel*)self.movableLabels[i]).frame = frame;
        }
        else if (i <= count - 5 && i > count - 10)
        {
            frame = ((UILabel*)self.movableLabels[i]).frame;
            frame.origin.y += 23;
            ((UILabel*)self.movableLabels[i]).frame = frame;
        }
        else if (i <= count - 10 && i > count - 15)
        {
            frame = ((UILabel*)self.movableLabels[i]).frame;
            frame.origin.y += 36;
            ((UILabel*)self.movableLabels[i]).frame = frame;
        }
        else if (i <= count - 15 && i > count - 20)
        {
            frame = ((UILabel*)self.movableLabels[i]).frame;
            frame.origin.y += 49;
            ((UILabel*)self.movableLabels[i]).frame = frame;
        }
        else if (i <= count - 20 && i > count - 25)
        {
            frame = ((UILabel*)self.movableLabels[i]).frame;
            frame.origin.y += 62;
            ((UILabel*)self.movableLabels[i]).frame = frame;
        }
        else if (i <= count - 25 && i > count - 30)
        {
            frame = ((UILabel*)self.movableLabels[i]).frame;
            frame.origin.y += 75;
            ((UILabel*)self.movableLabels[i]).frame = frame;
        }
    }
}

-(void)takeAnswer:(UILongPressGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        if (self.isAns)
        {
            self.scientificString = self.resultString;
            [self stringToDisplay:self.scientificString];
        }
        else
        {
            if ([self.resultString characterAtIndex:0] == '-') {
                self.scientificString = [NSString stringWithFormat:@"%@(%@)", self.scientificString, self.resultString];
            }
            else
            {
                self.scientificString = [self.scientificString stringByAppendingString:self.resultString];
            }
            [self stringToDisplay:self.scientificString];
        }
    }
}

-(void)decimalLongPress:(UILongPressGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        if ([self.complexButton.currentTitle isEqualToString:@"Complex"]) {
            [self inputParser:CommaSign];
        }
    }
}

- (IBAction)delete:(id)sender {
    
    if ([self.scientificString length] > 0) {
        AttributedStringStructure *attribute = [self.attributeHolder peekLast];
        if (attribute.start + attribute.stop == [self.scientificString length]) {
            if (([attribute.type isEqualToString:SUPERSCRIP_ATTRIBUTED_ELEMENT] || [attribute.type isEqualToString:SUBSCRIPT_ATTRIBUTED_ELEMENT]) && [((AttributedStringStructure*)[self.attributeHolder peekAtIndex:[self.attributeHolder lastObjectIndex] - 1]).type isEqualToString:NON_ATTRIBUTED_ELEMENT]) {
                self.scientificString = [self.scientificString substringToIndex:((AttributedStringStructure*)[self.attributeHolder peekAtIndex:[self.attributeHolder lastObjectIndex] - 1]).start];
                [self.attributeHolder popObject];
                [self.attributeHolder popObject];
            }
            else
            {
                self.scientificString = [self.scientificString substringToIndex:attribute.start];
                [self.attributeHolder popObject];
            }
        }
        else
        {
            if ([self.scientificString characterAtIndex:[self.scientificString length] - 1] == '@' || [self.scientificString characterAtIndex:[self.scientificString length] - 1] == '$') {
                for (NSInteger i = [self.scientificString length] - 2; i >= 0; --i) {
                    if ([self.scientificString characterAtIndex:i] == '@' || [self.scientificString characterAtIndex:i] == '$') {
                        self.scientificString = [self.scientificString substringToIndex:i];
                        break;
                    }
                }
            }
            else
            {
                if (self.isClosingBracket) {
                    self.isClosingBracket = NO;
                }
                if ([self.scientificString characterAtIndex:[self.scientificString length] - 2] >= '0' && [self.scientificString characterAtIndex:[self.scientificString length] - 2] <= '9') {
                    self.isNumericInput = YES;
                }
                else
                {
                    self.isNumericInput = NO;
                }
                if ([self.scientificString characterAtIndex:[self.scientificString length] - 2] == ')') {
                    self.isClosingBracket = YES;
                }
                self.scientificString = [self.scientificString substringToIndex:[self.scientificString length] - 1];
            }
            
        }
    }
    if ([self.scientificString isEqualToString:@""]) {
        self.backspaceButton.enabled = NO;
        
    }
    
    [self stringToDisplay:self.scientificString];
}

- (IBAction)tangentInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.currentTitle isEqualToString:@"tg"]) {
        [self inputParser:Tangent];
    }
    else if ([button.titleLabel.text isEqualToString:@"tg\u207B\u00B9"])
    {
        [self inputParser:TangentArc];
    }
    else if ([button.titleLabel.text isEqualToString:@"tgh"])
    {
        [self inputParser:TangentHyperbolic];
    }
    else if ([button.titleLabel.text isEqualToString:@"tgh\u207B\u00B9"])
    {
        [self inputParser:TangentHyperbolicArc];
    }
    else
    {
        [self showAlert:@"Something went wrong! Check input!"];
    }
}

- (IBAction)numberInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    NSInteger number = [button.currentTitle integerValue];
    if ([self.notationButton.titleLabel.text isEqualToString:@"HEX"]) {
        for (NSInteger i = 0; i < [self.hexaTitles count]; ++i) {
            if([button.titleLabel.text isEqualToString:self.hexaTitles[i]])
            {
                number = 10 + i;
                break;
            }
        }
    }
    
    [self inputParser:number];
}

- (IBAction)basicFunctionInut:(id)sender {
    UIButton *button = (UIButton*)sender;
    NSString *functionString = button.titleLabel.text;
    NSUInteger number;
    if ([functionString isEqualToString:@"+"]) {
        number = Addition;
    }
    else if ([functionString isEqualToString:@"-"])
    {
        number = Subtraction;
    }
    else if ([functionString isEqualToString:@"×"])
    {
        number = Multiplication;
    }
    else if ([functionString isEqualToString:@"÷"])
    {
        number = Division;
    }
    else if ([functionString isEqualToString:@"("])
    {
        number = OpenBracket;
    }
    else if ([functionString isEqualToString:@")"])
    {
        number = CloseBracket;
    }
    else if ([functionString isEqualToString:@"^"])
    {
        number = PowerOf;
    }
    else if ([functionString isEqualToString:@"."])
    {
        number = DecimalPoint;
    }
    else if ([functionString isEqualToString:@","])
    {
        number = CommaSign;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Wrong input" message:@"Something went wrong!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [self inputParser:number];
}

-(BOOL)checkBrackets:(NSString*)exp
{
    NSInteger bracketCount = 0;
    NSInteger count = [exp length];
    
    for (NSInteger i = 0; i < count; ++i) {
        if ([exp characterAtIndex:i] == '(') {
            ++bracketCount;
        }
        else if ([exp characterAtIndex:i] == ')')
        {
            --bracketCount;
        }
    }
    
    return (bracketCount == 0) ? YES : NO;
}

-(BOOL)checkOperations:(NSString*)exp
{
    NSInteger index;
    NSArray *array = [[NSArray alloc] initWithObjects:@"+", @"-", @"÷", @"×", nil];
    for (NSInteger i = 0; i < [exp length]; ++i) {
        if ([array containsObject:[exp substringWithRange:NSMakeRange(i, 1)]]) {
            if (i == index + 1) {
                return NO;
            }
            index = i;
        }
    }
    
    return YES;
}

- (IBAction)equals {
    Core *core = [[Core alloc] init];
    if ([self checkBrackets:self.scientificString] == NO || [self checkOperations:self.scientificString] == NO) {
        [self showAlert:@"Wrong input"];
        return;
    }
    Terminal *terminal = [core scientific:self.scientificString];
    if (!terminal.error_msg) {
        if ([self.complexButton.currentTitle isEqualToString:@"Real"]) {
            self.resultString = [NSString stringWithFormat:@"%g", [terminal.realPart doubleValue]];
        }
        else
        {
            self.resultString = [NSString stringWithFormat:@"%g,%g", [terminal.realPart doubleValue], [terminal.imaginaryPart doubleValue]];
        }
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.resultString];
        [self displayInputFromString:attrString withSource:NO];
        self.isAns = YES;
        [self.attributeHolder eraseStack];
        self.backspaceButton.enabled = NO;
    }
    else
    {
        self.isAns = NO;
        [self showAlert:terminal.error_msg];
    }
}

- (IBAction)cosineInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"cos"]) {
        [self inputParser:Cosine];
    }
    else if ([button.titleLabel.text isEqualToString:@"cos\u207B\u00B9"])
    {
        [self inputParser:CosineArc];
    }
    else if ([button.titleLabel.text isEqualToString:@"cosh"])
    {
        [self inputParser:CosineHyperbolic];
    }
    else if ([button.titleLabel.text isEqualToString:@"cosh\u207B\u00B9"])
    {
        [self inputParser:CosineHyperbolicArc];
    }
    else
    {
        [self showAlert:@"Something went wrong! Check input!"];
    }
}

- (IBAction)sineInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"sin"]) {
        [self inputParser:Sine];
    }
    else if ([button.titleLabel.text isEqualToString:@"sin\u207B\u00B9"])
    {
        [self inputParser:SineArc];
    }
    else if ([button.titleLabel.text isEqualToString:@"sinh"])
    {
        [self inputParser:SineHyperbolic];
    }
    else if ([button.titleLabel.text isEqualToString:@"sinh\u207B\u00B9"])
    {
        [self inputParser:SineHyperbolicArc];
    }
    else
    {
        [self showAlert:@"Something went wrong! Check input!"];
    }
}

- (IBAction)cotangentInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"cotg"]) {
        [self inputParser:Cotangent];
    }
    else if ([button.titleLabel.text isEqualToString:@"cotg\u207B\u00B9"])
    {
        [self inputParser:CotangentArc];
    }
    else if ([button.titleLabel.text isEqualToString:@"cotgh"])
    {
        [self inputParser:CotangentHyperbolic];
    }
    else if ([button.titleLabel.text isEqualToString:@"cotgh\u207B\u00B9"])
    {
        [self inputParser:CotangentHyperbolicArc];
    }
    else
    {
        [self showAlert:@"Something went wrong! Check input!"];
    }
}

- (IBAction)hyperbolicInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"hyp"]) {
        self.hyperbolicButton.selected = !self.hyperbolicButton.selected;
    }
    
    if (self.hyperbolicButton.selected) {
        [self.hyperbolicButton setBackgroundColor:[UIColor colorWithRed:0 green:(CGFloat)153/255 blue:1 alpha:1]];
    }
    else
    {
        [self.hyperbolicButton setBackgroundColor:[UIColor colorWithRed:0 green:(CGFloat)119/255 blue:(CGFloat)206/255 alpha:1]];
    }
    
    if (self.hyperbolicButton.selected && !self.shiftButton.selected) {
        [self.tgButton setTitle:@"tgh" forState:UIControlStateNormal];
        [self.cotgButton setTitle:@"cotgh" forState:UIControlStateNormal];
        [self.sinButton setTitle:@"sinh" forState:UIControlStateNormal];
        [self.cosButton setTitle:@"cosh" forState:UIControlStateNormal];
        
        self.arcTgLabel.text = @"tgh\u207B\u00B9";
        self.arcCotgLabel.text = @"cotgh\u207B\u00B9";
        self.arcSinLabel.text = @"sinh\u207B\u00B9";
        self.arcCosLabel.text = @"cosh\u207B\u00B9";
    }
    else if (self.hyperbolicButton.selected && self.shiftButton.selected)
    {
        [self.tgButton setTitle:@"tgh\u207B\u00B9" forState:UIControlStateNormal];
        [self.cotgButton setTitle:@"cotgh\u207B\u00B9" forState:UIControlStateNormal];
        [self.sinButton setTitle:@"sinh\u207B\u00B9" forState:UIControlStateNormal];
        [self.cosButton setTitle:@"cosh\u207B\u00B9" forState:UIControlStateNormal];
        
        self.arcTgLabel.text = @"tgh";
        self.arcCotgLabel.text = @"cotgh";
        self.arcSinLabel.text = @"sinh";
        self.arcCosLabel.text = @"cosh";
    }
    else if (!self.hyperbolicButton.selected && !self.shiftButton.selected)
    {
        [self shiftFunctions:[[UIButton alloc] init]];
    }
    else if (!self.hyperbolicButton.selected && self.shiftButton.selected)
    {
        [self shiftFunctions:[[UIButton alloc] init]];
    }
}

- (IBAction)neperNumberInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"e"]) {
        [self inputParser:Neper];
    }
    else
    {
        [self inputParser:PiNumber];
    }
}

- (IBAction)powerOfTwoInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"x\u00B2"]) {
        [self inputParser:Power2];
    }
    else
    {
        [self inputParser:Power3];
    }
}

- (IBAction)squareRootBaseTwoInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"√x"]) {
        [self inputParser:SquareRoot2];
    }
    else
    {
        [self inputParser:SquareRoot3];
    }
}

- (IBAction)activateOptions {
    [self.view addSubview:self.about];
    CGRect frame = self.about.frame;
    frame.origin.y = frame.origin.y + frame.size.height + 20;
    [AboutView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionLayoutSubviews
                     animations:^{self.about.frame = frame;}
                        completion:^(BOOL finished){
                        }];
}

-(void)back
{
    [self.about displayAbourFrame];
    CGRect frame = self.about.frame;
    frame.origin.y = - frame.size.height - 15;
    [AboutView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{self.about.frame = frame;}
                     completion:^(BOOL finished){
                         [self.about removeFromSuperview];
                     }];
}

- (IBAction)shiftFunctions:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"shift"]) {
        self.shiftButton.selected = !self.shiftButton.selected;
    }
    
    if (self.shiftButton.selected) {
        [self.shiftButton setBackgroundColor:[UIColor colorWithRed:(CGFloat)148/255.0f green:(CGFloat)148/255.0f blue:(CGFloat)148/255.0f alpha:1]];
    }
    else
    {
        [self.shiftButton setBackgroundColor:[UIColor colorWithRed:(CGFloat)208/255.0f green:(CGFloat)208/255.0f blue:(CGFloat)208/255.0f alpha:1]];
    }
    
    if (self.shiftButton.selected && !self.hyperbolicButton.selected) {
        [self.tgButton setTitle:@"tg\u207B\u00B9" forState:UIControlStateNormal];
        [self.cotgButton setTitle:@"cotg\u207B\u00B9" forState:UIControlStateNormal];
        [self.eButton setTitle:@"π" forState:UIControlStateNormal];
        [self.sinButton setTitle:@"sin\u207B\u00B9" forState:UIControlStateNormal];
        [self.cosButton setTitle:@"cos\u207B\u00B9" forState:UIControlStateNormal];
        [self.powerTwoButton setTitle:@"x\u00B3" forState:UIControlStateNormal];
        [self.rootTwoButton setTitle:@"∛x" forState:UIControlStateNormal];
        [self.factorielButton setTitle:@"log\u2090" forState:UIControlStateNormal];
        [self.lnButton setTitle:@"lg" forState:UIControlStateNormal];
        [self.changeSignButton setTitle:@"mod" forState:UIControlStateNormal];
        if ([self.complexButton.currentTitle isEqualToString:@"Real"]) {
            [self.decimalPointButton setTitle:@"," forState:UIControlStateNormal];
            self.decimalPointLabel.text = @".";
        }
        
        self.arcTgLabel.text = @"tg";
        self.arcCotgLabel.text = @"cotg";
        self.arcSinLabel.text = @"sin";
        self.arcCosLabel.text = @"cos";
        self.logarithmicLabel.text = @"n!";
        self.logarithmTenLabel.text = @"ln";
        self.rootThreeLabel.text = @"√x";
        self.powerThreeLabel.text = @"x\u00B2";
        self.piLabel.text = @"e";
        self.modLabel.text = @"±";
        if ([self.notationButton.titleLabel.text isEqualToString:@"HEX"]) {
            NSInteger i = 0;
            for (UIButton *button in self.hexaButtons) {
                [button setTitle:self.hexaTitles[i] forState:UIControlStateNormal];
                ++i;
            }
        }
    }
    else if (!self.shiftButton.selected && !self.hyperbolicButton.selected)
    {
        [self.tgButton setTitle:@"tg" forState:UIControlStateNormal];
        [self.cotgButton setTitle:@"cotg" forState:UIControlStateNormal];
        [self.eButton setTitle:@"e" forState:UIControlStateNormal];
        [self.sinButton setTitle:@"sin" forState:UIControlStateNormal];
        [self.cosButton setTitle:@"cos" forState:UIControlStateNormal];
        [self.powerTwoButton setTitle:@"x\u00B2" forState:UIControlStateNormal];
        [self.rootTwoButton setTitle:@"√x" forState:UIControlStateNormal];
        [self.factorielButton setTitle:@"n!" forState:UIControlStateNormal];
        [self.lnButton setTitle:@"ln" forState:UIControlStateNormal];
        [self.changeSignButton setTitle:@"±" forState:UIControlStateNormal];
        if ([self.complexButton.currentTitle isEqualToString:@"Real"]) {
            [self.decimalPointButton setTitle:@"." forState:UIControlStateNormal];
            self.decimalPointLabel.text = @",";
        }
        
        self.arcTgLabel.text = @"tg\u207B\u00B9";
        self.arcCotgLabel.text = @"cotg\u207B\u00B9";
        self.arcSinLabel.text = @"sin\u207B\u00B9";
        self.arcCosLabel.text = @"cos\u207B\u00B9";
        self.logarithmicLabel.text = @"log\u2090";
        self.logarithmTenLabel.text = @"lg";
        self.rootThreeLabel.text = @"∛x";
        self.powerThreeLabel.text = @"x\u00B3";
        self.piLabel.text = @"π";
        self.modLabel.text = @"mod";
        if ([self.notationButton.titleLabel.text isEqualToString:@"HEX"]) {
            NSInteger i = 0;
            for (UIButton *button in self.hexaButtons) {
                [button setTitle:self.decTitles[i] forState:UIControlStateNormal];
                ++i;
            }
        }
        self.hyperbolicButton.backgroundColor = [UIColor colorWithRed:0 green:(CGFloat)119/255.0f blue:(CGFloat)206/255.0f alpha:1];
    }
    else if (self.shiftButton.selected && self.hyperbolicButton.selected)
    {
        [self hyperbolicInput:[[UIButton alloc] init]];
    }
    else if (!self.shiftButton.selected && self.hyperbolicButton.selected)
    {
        [self hyperbolicInput:[[UIButton alloc] init]];
    }
}

- (IBAction)factorialInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"n!"]) {
        [self inputParser:Factorial];
    }
    else
    {
        self.factorielButton.selected = !self.factorielButton.selected;
        if (self.factorielButton.selected) {
            [self inputParser:Logarithm];
            [self.factorielButton setBackgroundColor:[UIColor colorWithRed:0 green:(CGFloat)153/255.0f blue:1 alpha:1]];
        }
        else
        {
            [self.factorielButton setBackgroundColor:[UIColor colorWithRed:0 green:(CGFloat)119/255.0f blue:(CGFloat)206/255.0f alpha:1]];
            UIButton *button = [[UIButton alloc] init];
            [button setTitle:@"shift" forState:UIControlStateNormal];
            [self shiftFunctions:button];
        }
    }
}

- (IBAction)logarithmInput:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.titleLabel.text isEqualToString:@"ln"]) {
        [self inputParser:LogarithmNatural];
    }
    else
    {
        [self inputParser:LogarithmBaseTen];
    }
}

- (IBAction)changeNotation:(id)sender {
    UIButton *button = (UIButton*)sender;
    NSArray *values = [self.mainDictionary allValues];
    NSRange range = NSMakeRange(0, 0);
    for (NSInteger i = 0; i < [values count]; ++i) {
        range = [self.scientificString rangeOfString:values[i]];
        if (range.location != 0 && range.length != 0) {
            [self showAlert:@"Wrong input!"];
            return;
        }
    }
    if ([button.titleLabel.text isEqualToString:@"DEC"]) {
        [button setTitle:@"HEX" forState:UIControlStateNormal];
        [self convertNotation:Hexadecimal];
    }
    else if ([button.titleLabel.text isEqualToString:@"HEX"])
    {
        [button setTitle:@"BIN" forState:UIControlStateNormal];
        for (UIButton *button in self.binaryButtons) {
            button.enabled = NO;
            [button setBackgroundColor:[UIColor darkGrayColor]];
        }
        [self convertNotation:Binary];
    }
    else if ([button.titleLabel.text isEqualToString:@"BIN"])
    {
        [button setTitle:@"OCT" forState:UIControlStateNormal];
        for (UIButton *button in self.binaryButtons) {
            button.enabled = YES;
            NSNumberFormatter *nft = [[NSNumberFormatter alloc] init];
            CGFloat green = 78.0/255.0f;
            CGFloat blue = 172.0/255.0f;
            [nft setPositiveFormat:@"0.##"];
            
            green = [[nft stringFromNumber:[NSNumber numberWithFloat:green]] doubleValue];
            blue = [[nft stringFromNumber:[NSNumber numberWithFloat:blue]] doubleValue];
            
            [button setBackgroundColor:[UIColor colorWithRed:0 green:green blue:blue alpha:1]];
        }
        for (UIButton *button in self.octaButtons) {
            button.enabled = NO;
            [button setBackgroundColor:[UIColor darkGrayColor]];
        }
        [self convertNotation:Octal];
    }
    else
    {
        [button setTitle:@"DEC" forState:UIControlStateNormal];
        for (UIButton *button in self.octaButtons) {
            button.enabled = YES;
            CGFloat green = 78.0/255.0f;
            CGFloat blue = 172.0/255.0f;
            [button setBackgroundColor:[UIColor colorWithRed:0 green:green blue:blue alpha:1]];
        }
        [self convertNotation:Decimal];
    }
}

- (IBAction)changeSign:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([button.currentTitle isEqualToString:@"±"]) {
        if ([self.resultString characterAtIndex:0] == '-') {
            self.resultString = [self.resultString substringFromIndex:1];
            NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.resultString];
            [self displayInputFromString:attrString withSource:NO];
        }
        else
        {
            self.resultString = [NSString stringWithFormat:@"-%@", self.resultString];
            NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.resultString];
            [self displayInputFromString:attrString withSource:NO];
        }
    }
    else
    {
        [self inputParser:Mod];
    }
}

- (IBAction)changeToRad:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:@"DEG"]) {
        [sender setTitle:@"RAD" forState:UIControlStateNormal];
        self.isDegrees = NO;
    }
    else
    {
        [sender setTitle:@"DEG" forState:UIControlStateNormal];
        self.isDegrees = YES;
    }
}

- (IBAction)complexToReal:(UIButton*)sender {
    if ([sender.currentTitle isEqualToString:@"Real"]) {
        [self.complexButton setTitle:@"Complex" forState:UIControlStateNormal];
        self.decimalPointLabel.text = @",";
        self.scientificString = @"";
        self.resultString = @"0";
        self.isNumericInput = NO;
        self.isClosingBracket = NO;
        self.isAns = NO;
        [self.attributeHolder eraseStack];
        [self stringToDisplay:self.scientificString];
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.resultString];
        [self displayInputFromString:attrString withSource:NO];
    }
    else
    {
        [self.complexButton setTitle:@"Real" forState:UIControlStateNormal];
        self.decimalPointLabel.text = @"";
        self.scientificString = @"";
        self.resultString = @"0";
        self.isNumericInput = NO;
        self.isClosingBracket = NO;
        self.isAns = NO;
        [self.attributeHolder eraseStack];
        [self stringToDisplay:self.scientificString];
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.resultString];
        [self displayInputFromString:attrString withSource:NO];
    }
}

-(void)stringToDisplay:(NSString*)string
{
    NSMutableDictionary *specialCharacters = [[NSMutableDictionary alloc] init];
    for (NSInteger i = 0; i < [string length]; ++i) {
        if ([string characterAtIndex:i] == '@') {
            for (NSInteger j = 0; j < [self.attributeHolder countOfStackArray]; ++j) {
                if (((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start > i && !((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted) {
                    --((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start;
                    ((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted = YES;
                }
            }
            [specialCharacters setObject:@"@" forKey:[NSString stringWithFormat:@"%ld", (long)i]];
        }
        else if ([string characterAtIndex:i] == '$')
        {
            for (NSInteger j = 0; j < [self.attributeHolder countOfStackArray]; ++j) {
                if (((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start > i && !((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted) {
                    --((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start;
                    ((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted = YES;
                }
            }
            [specialCharacters setObject:@"$" forKey:[NSString stringWithFormat:@"%ld", (long)i]];
        }
        else if ([string characterAtIndex:i] == 'r')
        {
            for (NSInteger j = 0; j < [self.attributeHolder countOfStackArray]; ++j) {
                if (((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start > i && !((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted) {
                    --((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start;
                    ((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted = YES;
                }
            }
            [specialCharacters setObject:@"r" forKey:[NSString stringWithFormat:@"%ld", (long)i]];
        }
        else if ([string characterAtIndex:i] == 'd')
        {
            for (NSInteger j = 0; j < [self.attributeHolder countOfStackArray]; ++j) {
                if (((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start > i && !((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted) {
                    --((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).start;
                    ((AttributedStringStructure*)[self.attributeHolder peekAtIndex:j]).isIndexAdjusted = YES;
                }
            }
            [specialCharacters setObject:@"d" forKey:[NSString stringWithFormat:@"%ld", (long)i]];
        }
    }
    
    if ([[specialCharacters allKeys] count] != 0) {
        string = [string stringByReplacingOccurrencesOfString:@"@" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"$" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"r" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"d" withString:@""];
    }
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:string];
    NSRange range;
    
    NSMutableArray *keys = [[specialCharacters allKeys] mutableCopy];
    if ([keys count] != 0) {
        for (NSInteger i = 0; i < [keys count] - 1; ++i) {
            for (NSInteger j = i + 1; j < [keys count]; ++j) {
                if ([keys[i] integerValue] > [keys[j] integerValue]) {
                    id swap = keys[j];
                    [keys replaceObjectAtIndex:j withObject:keys[i]];
                    [keys replaceObjectAtIndex:i withObject:swap];
                }
            }
        }
    }
    
    if ([keys count] != 0) {
        for (NSInteger i = 0; i < [keys count] - 1; ++i) {
            NSString *firstString = [specialCharacters valueForKey:keys[i]];
            NSString *secondString = [specialCharacters valueForKey:keys[i + 1]];
            if ([firstString isEqualToString:secondString] && i % 2 == 0) {
                if ([firstString isEqualToString:@"@"]) {
                    range = NSMakeRange([keys[i] integerValue] - i, [keys[i + 1] integerValue] - [keys[i] integerValue] - 1);
                    [attrString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:range];
                }
                else if ([firstString isEqualToString:@"$"])
                {
                    range = NSMakeRange([keys[i] integerValue] - i, [keys[i + 1] integerValue] - [keys[i] integerValue] - 1);
                    [attrString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"-1" range:range];
                }
            }
        }
    }
    
    for (AttributedStringStructure *attribute in self.attributeHolder.stackArray) {
        if ([attribute isKindOfClass:[AttributedStringStructure class]]) {
            if ([attribute.type isEqualToString:SUPERSCRIP_ATTRIBUTED_ELEMENT] || [attribute.type isEqualToString:SUBSCRIPT_ATTRIBUTED_ELEMENT]) {
                [attrString addAttribute:(NSString*)kCTSuperscriptAttributeName value:attribute.type range:NSMakeRange(attribute.start, attribute.stop)];
            }
        }
    }
    
    [self displayInputFromString:attrString withSource:YES];
}

-(void)addElementWithRange:(NSRange)range andType:(ElementTypes)type
{
    AttributedStringStructure *element = [[AttributedStringStructure alloc] init];
    element.start = range.location;
    element.stop = range.length;
    if (type == SuperScriptAttributedElement) {
        element.type = SUPERSCRIP_ATTRIBUTED_ELEMENT;
    }
    else if (type == SubSscriptattributedElement)
    {
        element.type = SUBSCRIPT_ATTRIBUTED_ELEMENT;
    }
    else
    {
        element.type = NON_ATTRIBUTED_ELEMENT;
    }
    
    [self.attributeHolder pushObject:element];
    NSString *indexes = @"";
    for (NSInteger i = 0; i < [self.scientificString length]; ++i) {
        indexes = [NSString stringWithFormat:@"%@%ld", indexes, (long)i];
    }
}

-(void)inputParser:(NSUInteger)input
{
    if (!self.backspaceButton.enabled) {
        self.backspaceButton.enabled = YES;
    }
    NSString *regularString = @"";
    if (input < 15) {
        if (input < 10) {
            if (self.isClosingBracket && !self.factorielButton.selected) {
                regularString = [NSString stringWithFormat:@"%@×%lu", self.scientificString, (unsigned long)input];
            }
            else if (!self.isClosingBracket && !self.factorielButton.selected)
            {
                regularString = [NSString stringWithFormat:@"%@%lu", self.scientificString, (unsigned long)input];
            }
            else if (self.factorielButton.selected)
            {
                regularString = [self.scientificString substringToIndex:[self.scientificString length] - 2];
                regularString = [regularString stringByAppendingString:[NSString stringWithFormat:@"%lu$(", (unsigned long)input]];
            }
            if (self.isAns)
            {
                regularString = [NSString stringWithFormat:@"%lu", (unsigned long)input];
                [self.attributeHolder eraseStack];
            }
        }
        else
        {
            if (self.isClosingBracket && !self.factorielButton.selected) {
                regularString = [NSString stringWithFormat:@"%@×%@", self.scientificString, self.hexaTitles[input - 10]];
            }
            else if (!self.isClosingBracket && !self.factorielButton.selected)
            {
                regularString = [NSString stringWithFormat:@"%@%@", self.scientificString, self.hexaTitles[input - 10]];
            }
        }
        self.isClosingBracket = NO;
        self.isNumericInput = YES;
    }
    
    switch (input) {
        case TangentArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×tg-1(", self.scientificString];
                
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@tg-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"tg-1(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 5, 2) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
        
        case Tangent:
            if (self.isDegrees) {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×tg(d", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@tg(d", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"tg(d";
                }
            }
            else
            {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×tg(r", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@tg(r", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"tg(r";
                }
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 4, 2) andType:NonAttributedElement];
            self.isClosingBracket = NO;
            self.isNumericInput = NO;
            break;

            
        case Cotangent:
            if (self.isDegrees) {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×cotg(d", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@cotg(d", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"cotg(d";
                }
            }
            else
            {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×cotg(r", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@cotg(r", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"cotg(r";
                }
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 6, 4) andType:NonAttributedElement];
            self.isClosingBracket = NO;
            self.isNumericInput = NO;
            break;
            
        case CotangentArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×cotg-1(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@cotg-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"cotg-1(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 7, 2) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isClosingBracket = NO;
            self.isNumericInput = NO;
            break;
            
        case Cosine:
            if (self.isDegrees) {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×cos(d", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@cos(d", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"cos(d";
                }
            }
            else
            {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×cos(r", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@cos(r", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"cos(r";
                }
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 5, 3) andType:NonAttributedElement];
            self.isClosingBracket = NO;
            self.isNumericInput = NO;
            break;
            
        case CosineArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×cos-1(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@cos-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"cos-1(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 6, 3) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Sine:
            if (self.isDegrees) {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×sin(d", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@sin(d", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"cos(d";
                }
            }
            else
            {
                if (self.isNumericInput || self.isClosingBracket) {
                    regularString = [NSString stringWithFormat:@"%@×sin(r", self.scientificString];
                }
                else
                {
                    regularString = [NSString stringWithFormat:@"%@sin(r", self.scientificString];
                }
                if (self.isAns) {
                    regularString = @"sin(r";
                }
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 5, 3) andType:NonAttributedElement];
            self.isClosingBracket = NO;
            self.isNumericInput = NO;
            break;

            
        case SineArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×sin-1(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@sin-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"sin-1";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 6, 3) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case SineHyperbolic:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×sinh(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@sinh(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"sinh(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 5, 4) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case SineHyperbolicArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×sinh-1(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@sinh-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"sinh-1(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 7, 2) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case CosineHyperbolic:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×cosh(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@cosh(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"cosh(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 5, 4) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case CosineHyperbolicArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×cosh-1(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@cosh-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"cosh-1(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 7, 2) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case TangentHyperbolic:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×tgh(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@tgh(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"tgh(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 4, 3) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case TangentHyperbolicArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×tgh-1(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@tgh-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"tgh-1(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 6, 2) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case CotangentHyperbolic:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×cotgh(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@cotgh(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"cotgh(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 6, 5) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case CotangentHyperbolicArc:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×cotgh-1(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@cotgh-1(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"cotgh-1(";
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 8, 2) andType:NonAttributedElement];
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:SuperScriptAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Power2:
            regularString = [NSString stringWithFormat:@"%@^2", self.scientificString];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Power3:
            regularString = [NSString stringWithFormat:@"%@^3", self.scientificString];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case SquareRoot2:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×@2@√(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@@2@√(", self.scientificString];
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 5, 4) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case SquareRoot3:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×@3@√(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@@3@√(", self.scientificString];
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 5, 4) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Hyperbolic:
            break;
            
        case Neper:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×e", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@e", self.scientificString];
            }
            self.isNumericInput = YES;
            self.isClosingBracket = NO;
            break;
            
        case PiNumber:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×π", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@π", self.scientificString];
            }
            self.isNumericInput = YES;
            self.isClosingBracket = NO;
            break;
            
        case Factorial:
            regularString = [NSString stringWithFormat:@"%@!", self.scientificString];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case LogarithmNatural:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×ln(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@ln(", self.scientificString];
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case LogarithmBaseTen:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×lg(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@lg(", self.scientificString];
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 3, 2) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Logarithm:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×log$$(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@log$$(", self.scientificString];
            }
            [self addElementWithRange:NSMakeRange([regularString length] - 6, 3) andType:NonAttributedElement];
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case ChangeSign:
            if (self.isAns) {
                regularString = @"-";
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@-", self.scientificString];
            }
            break;
            
        case Addition:
            if (self.isAns) {
                regularString = [NSString stringWithFormat:@"%@+", self.resultString];
                [self.attributeHolder eraseStack];
                self.isAns = NO;
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@+", self.scientificString];
            }
            //self.isAns = NO;
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Subtraction:
            if (self.isAns) {
                regularString = [NSString stringWithFormat:@"%@-", self.resultString];
                [self.attributeHolder eraseStack];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@-", self.scientificString];
            }
            //self.isAns = NO;
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Multiplication:
            if (self.isAns) {
                regularString = [NSString stringWithFormat:@"%@×", self.resultString];
                [self.attributeHolder eraseStack];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@×", self.scientificString];
            }
            //self.isAns = NO;
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case Division:
            if (self.isAns) {
                regularString = [NSString stringWithFormat:@"%@÷", self.resultString];
                [self.attributeHolder eraseStack];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@÷", self.scientificString];
            }
            //self.isAns = NO;
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case OpenBracket:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@(", self.scientificString];
            }
            
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case CloseBracket:
            regularString = [NSString stringWithFormat:@"%@)", self.scientificString];
            self.isClosingBracket = YES;
            self.isNumericInput = NO;
            break;
            
        case PowerOf:
            if (self.isAns) {
                regularString = [NSString stringWithFormat:@"%@^", self.resultString];
                [self.attributeHolder eraseStack];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@^", self.scientificString];
            }
            self.isNumericInput = NO;
            self.isClosingBracket = NO;
            break;
            
        case DecimalPoint:
            if ([self.scientificString isEqualToString:@""]) {
                regularString = [NSString stringWithFormat:@"0."];
            }
            else if (self.isAns) {
                regularString = [NSString stringWithFormat:@"0."];
                [self.attributeHolder eraseStack];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@.", self.scientificString];
            }
            
            break;
            
        case CommaSign:
            if ([self.scientificString isEqualToString:@""]) {
                [self showAlert:@"Wrong input"];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@,", self.scientificString];
            }
            break;
            
        case Mod:
            if (self.isNumericInput || self.isClosingBracket) {
                regularString = [NSString stringWithFormat:@"%@×mod(", self.scientificString];
            }
            else
            {
                regularString = [NSString stringWithFormat:@"%@mod(", self.scientificString];
            }
            if (self.isAns) {
                regularString = @"mod(";
            }
            
            
        default:
            break;
    }
    
    self.isAns = NO;
    if (self.shiftButton.selected && !self.factorielButton.selected) {
        UIButton *button = [[UIButton alloc] init];
        [button setTitle:@"shift" forState:UIControlStateNormal];
        [self shiftFunctions:button];
    }
    
    if (self.hyperbolicButton.selected) {
        self.hyperbolicButton.selected = NO;
        [self shiftFunctions:[[UIButton alloc] init]];
    }
    
    self.scientificString = regularString;
    if (![self.scientificString isEqualToString:@""]) {
        [self stringToDisplay:self.scientificString];
    }
    
}

- (IBAction)clearInput:(id)sender {
    self.scientificString = @"";
    self.resultString = @"0";
    self.isNumericInput = NO;
    self.isClosingBracket = NO;
    self.isAns = NO;
    [self.attributeHolder eraseStack];
    [self stringToDisplay:self.scientificString];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.resultString];
    [self displayInputFromString:attrString withSource:NO];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Matrix 2"]) {
        MatrixViewController *matrixViewController = [segue destinationViewController];
        [matrixViewController.navigationController setNavigationBarHidden:YES];
    }
}

-(void)displayInputFromString:(NSAttributedString*)attrString withSource:(BOOL)source //ДА = вход, НЕ = изход
{
    NSString *tempString = [attrString string];
    
    if (source) {
        self.formulaInputScrollView.contentSize = CGSizeZero;
        for (UILabel *label in [self.formulaInputScrollView subviews]) {
            if ([label isKindOfClass:[UILabel class]]) {
                [label removeFromSuperview];
            }
        }
    }
    else
    {
        self.resultOutputScrollView.contentSize = CGSizeZero;
        for (UILabel *label in [self.resultOutputScrollView subviews]) {
            if ([label isKindOfClass:[UILabel class]]) {
                [label removeFromSuperview];
            }
        }
    }
    
    UIFont *font;
    if (source) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            font = [UIFont systemFontOfSize:19];
        }
        else
        {
            font = [UIFont systemFontOfSize:43];
        }
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            font = [UIFont systemFontOfSize:62];
        }
        else
        {
            font = [UIFont systemFontOfSize:140];
        }
        
    }
    
    CGSize mySize;
    if (source) {
        mySize = [tempString sizeWithAttributes:[NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName]];
        //mySize = [tempString sizeWithFont:font];
    }
    else
    {
        mySize = [self.resultString sizeWithAttributes:[NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName]];
        //mySize = [self.resultString sizeWithFont:font];
    }
    
    if (source) {
        mySize.height = self.formulaInputScrollView.frame.size.height;
    }
    else
    {
        mySize.height = self.resultOutputScrollView.frame.size.height;
    }
    
    if (source) {
        self.formulaInputScrollView.contentSize = mySize;
    }
    else
    {
        self.resultOutputScrollView.contentSize = mySize;
    }
    
    UILabel *label;
    if (mySize.width < self.formulaInputScrollView.frame.size.width) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(self.formulaInputScrollView.frame.size.width - mySize.width, 0, mySize.width, mySize.height)];
    }
    else
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, mySize.width, mySize.height)];
    }
    
    if (source) {
        label.attributedText = attrString;
    }
    else
    {
        label.text = self.resultString;
    }

    label.font = font;
    label.textColor = [UIColor colorWithRed:(CGFloat)208/255 green:(CGFloat)208/255 blue:(CGFloat)208/255 alpha:1];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentRight;
    
    
    if (source) {
        [self.formulaInputScrollView addSubview:label];
    }
    else
    {
        [self.resultOutputScrollView addSubview:label];
    }
    
    if (source) {
        [self.formulaInputScrollView scrollRectToVisible:CGRectMake(label.frame.size.width - self.formulaInputScrollView.frame.size.width, 0, label.frame.size.width, label.frame.size.height) animated:NO];
    }
    else
    {
        [self.resultOutputScrollView scrollRectToVisible:CGRectMake(label.frame.size.width - self.resultOutputScrollView.frame.size.width, 0, label.frame.size.width, label.frame.size.height) animated:NO];
    }
}

-(void)showAlert:(NSString*)alertMessage
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)convertNotation:(NSInteger)targetNotation
{
    BOOL isNumber = NO;
    BOOL isInt = YES;
    NSInteger doublePartCounter = 0;
    NSInteger targetBase = targetNotation;
    NSInteger value;
    NSInteger cntBase = 0;
    
    switch (targetNotation) {
        case Binary:
            cntBase = Hexadecimal;
            break;
            
        case Octal:
            cntBase = Binary;
            break;
            
        case Decimal:
            cntBase = Octal;
            break;
        
        case Hexadecimal:
            cntBase = Decimal;
            break;
        
        default:
            break;
    }
    
    double intPart = 0;
    double doublePart = 0.0;
    double doubleMultiplier = 1.0 / cntBase;
    NSInteger size = [self.scientificString length];
    NSString *exp = self.scientificString;
    self.scientificString = @"";
    NSString *convertingBuffer = @"";
    
    for (NSInteger i = 0; i < size; ++i) {
        if (([exp characterAtIndex:i] >= '0' && [exp characterAtIndex:i] <= '9') || [exp characterAtIndex:i] == '.' || ([exp characterAtIndex:i] >= 'A' && [exp characterAtIndex:i] <= 'F')) {
            isNumber = YES;
            if ([exp characterAtIndex:i] == '.') {
                isInt = NO;
                continue;
            }
            if ([exp characterAtIndex:i] >= '0' && [exp characterAtIndex:i] <= '9') {
                value = [exp characterAtIndex:i] - '0';
            }
            else
            {
                value = 10 + [exp characterAtIndex:i] - 'A';
            }
            
            if (isInt) {
                intPart *= cntBase;
                intPart += value;
                intPart = round(intPart);
            }
            else
            {
                doublePart += value * doubleMultiplier;
                doubleMultiplier *= 1.0 / cntBase;
            }
            
            if (i + 1 != size) {
                continue;
            }
        }
        
        if (isNumber) {
            while (intPart > 0) {
                value = (NSInteger)round((intPart - floor(intPart / targetBase) * targetBase));
                intPart /= targetBase;
                intPart = floor(intPart);
                if (value < 10) {
                    convertingBuffer = [NSString stringWithFormat:@"%@%c", convertingBuffer, (char)(value + '0')];
                }
                else
                {
                    convertingBuffer = [NSString stringWithFormat:@"%@%c", convertingBuffer, (char)(value - 10 + 'A')];
                }
            }
            if ([convertingBuffer length] == 0) {
                self.scientificString = [self.scientificString stringByAppendingString:@"0"];
            }
            
            for (NSInteger j = [convertingBuffer length] - 1; j >= 0; --j) {
                @synchronized(self.scientificString)
                {
                    self.scientificString = [NSString stringWithFormat:@"%@%c", self.scientificString, [convertingBuffer characterAtIndex:j]];
                }
                
            }
            
            if (doublePart != 0.0) {
                @synchronized(self.scientificString)
                {
                    self.scientificString = [self.scientificString stringByAppendingString:@"."];
                    doublePartCounter = 0;
                    while (doublePart != 0.0 && doublePartCounter < 8) {
                        doublePart *= targetBase;
                        value = (int)doublePart;
                        if (value < 10) {
                            self.scientificString = [NSString stringWithFormat:@"%@%ld", self.scientificString, (long)value];
                        }
                        else
                        {
                            self.scientificString = [NSString stringWithFormat:@"%@%c", self.scientificString, (char)(value - 10 + 'A')];
                        }
                        
                        doublePart = round(doublePart - value);
                        ++doublePartCounter;
                    }
                    
                    doublePart = 0.0;
                }
                
                convertingBuffer = @"";
                doubleMultiplier = 1.0 / cntBase;
                isInt = YES;
                isNumber = NO;
            }
                
        }
        
        if (!(([exp characterAtIndex:i] >= '0' && [exp characterAtIndex:i] <= '9') || [exp characterAtIndex:i] == '.' || ([exp characterAtIndex:i] >= 'A' && [exp characterAtIndex:i] <= 'F'))) {
            @synchronized(self.scientificString)
            {
                self.scientificString = [NSString stringWithFormat:@"%@%c", self.scientificString, [exp characterAtIndex:i]];
            }
        }
    }
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.scientificString];
    
    [self displayInputFromString:attrString withSource:YES];
}

@end
