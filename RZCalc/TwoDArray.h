//
//  TwoDArray.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/9/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwoDArray : NSObject

@property (nonatomic, strong) NSMutableArray *rows;

-(id)initWithRows:(NSUInteger)rows andColumns:(NSUInteger)columns;

+(id)sectionArrayWithRows:(NSUInteger)rows andColumns:(NSUInteger)columns;
-(id)objectInRow:(NSUInteger)row andColumn:(NSUInteger)column;
-(void)setObject:(id)obj inRow:(NSUInteger)row andColumn:(NSUInteger)column;

@end
