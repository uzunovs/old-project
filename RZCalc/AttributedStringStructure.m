//
//  AttributedStringStructure.m
//  RZCalc
//
//  Created by Hristo Uzunov on 11/15/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "AttributedStringStructure.h"

@implementation AttributedStringStructure

-(id)init
{
    self = [super init];
    if (self) {
        self.start = UndefinedElemet;
        self.stop = UndefinedElemet;
        self.type = [NSString stringWithFormat:@"%d", UndefinedElemet];
        self.isIndexAdjusted = NO;
    }
    
    return self;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"\nStart: %ld\nStop: %ld\nType: %@\nIndex Adjusted: %d", (long)self.start, (long)self.stop, self.type, self.isIndexAdjusted];
}

@end
