//
//  MatrixButton.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/22/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "MatrixButton.h"

@interface MatrixButton ()

@property (nonatomic) NSInteger number;

@end

@implementation MatrixButton

- (id)initWithFrame:(CGRect)frame andIndex:(NSInteger)number
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        self.number = number;
        self.selected = NO;
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            self.titleLabel.font = [UIFont systemFontOfSize:11];
        }
        else
        {
            self.titleLabel.font = [UIFont systemFontOfSize:25];
        }
        
        [self setTitleColor:[UIColor colorWithRed:0 green:(CGFloat)35/255.0f blue:(CGFloat)59/255.0f alpha:1] forState:UIControlStateNormal];
        [self setButtonLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)setButtonLabel
{
    NSArray *lettersArray = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    NSString *labelString;
    if (self.number < [lettersArray count]) {
        labelString = [NSString stringWithFormat:@"Matrix\n%@", lettersArray[self.number]];
    }
    else if (self.number >= [lettersArray count] && self.number < [lettersArray count] * [lettersArray count])
    {
        labelString = [NSString stringWithFormat:@"Matrix\n%@%@", lettersArray[(int)floor(self.number / 26)], lettersArray[self.number % 26]];
    }
    else
    {
        labelString = @"You have got\nto be JOKING!";
    }
    
    [self setTitle:labelString forState:UIControlStateNormal];
}

-(void)selectButton
{
    self.highlighted = !self.highlighted;
}

@end
