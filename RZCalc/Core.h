//
//  Core.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/10/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Terminal.h"
#import "Matrix.h"
#import <math.h>
#import <float.h>
#import <complex.h>
#import "RZStack.h"

@interface Core : NSObject

-(Terminal*)scientific:(NSString*)exp;
-(Matrix*)matrixTranspose:(Matrix*)a;
-(Matrix*)matrixInverse:(Matrix*)a;
-(Terminal*)matrixDeterminant:(Matrix*)a;
-(NSInteger)matrixRank:(Matrix*)a;
-(Matrix*)gauseElimination:(Matrix*)a;
-(Terminal*)powerOfBase:(Terminal*)baseValue andPowerFactor:(Terminal*)powerValue;

@end
