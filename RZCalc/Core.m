//
//  Core.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/10/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "Core.h"

@interface Core ()

-(Terminal*)sin:(NSString*)exp;
-(Terminal*)sin1:(NSString*)exp;
-(Terminal*)sinh:(NSString*)exp;
-(Terminal*)sinh1:(NSString*)exp;
-(Terminal*)cos:(NSString*)exp;
-(Terminal*)cos1:(NSString*)exp;
-(Terminal*)cosh:(NSString*)exp;
-(Terminal*)cosh1:(NSString*)exp;
-(Terminal*)tg:(NSString*)exp;
-(Terminal*)tg1:(NSString*)exp;
-(Terminal*)tgh:(NSString*)exp;
-(Terminal*)tgh1:(NSString*)exp;
-(Terminal*)cotg:(NSString*)exp;
-(Terminal*)cotg1:(NSString *)exp;
-(Terminal*)cotgh:(NSString*)exp;
-(Terminal*)cotgh1:(NSString*)exp;
-(Terminal*)mod:(NSString*)exp;
-(Terminal*)logWithExp:(NSString*)exp andBase:(NSString*)base;
-(Terminal*)lg:(NSString*)exp;
-(Terminal*)ln:(NSString*)exp;
-(Terminal*)facturial:(NSString*)exp;
-(Terminal*)root:(NSString*)exp andRootPower:(NSString*)rootPower;
-(double)gammaFunction:(double)value;
-(BOOL)isSinh1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCosh1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isTgh1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCotgh1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isSinh:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCosh:(NSString*)exp index:(NSInteger)index;
-(BOOL)isTgh:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCotgh:(NSString*)exp index:(NSInteger)index;
-(BOOL)isSin1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCos1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isTg1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCotg1:(NSString*)exp index:(NSInteger)index;
-(BOOL)isSin:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCos:(NSString*)exp index:(NSInteger)index;
-(BOOL)isTg:(NSString*)exp index:(NSInteger)index;
-(BOOL)isCotg:(NSString*)exp index:(NSInteger)index;
-(BOOL)isMod:(NSString*)exp index:(NSInteger)index;
-(BOOL)isLog:(NSString*)exp index:(NSInteger)index;
-(BOOL)isLg:(NSString*)exp index:(NSInteger)index;
-(BOOL)isLn:(NSString*)exp index:(NSInteger)index;
-(NSString*)extractSuperScriptBackwards:(NSString*)exp index:(NSInteger)index;
-(NSString*)extractSubOrSuperScriptForwards:(NSString*)exp index:(NSInteger)index mathSymbol:(NSString*)symbol;
-(NSInteger)findFuncStart:(NSString*)exp index:(NSInteger)index;
-(NSInteger)findFuncEnd:(NSString*)exp index:(NSInteger)index;
-(NSInteger)findComplexStart:(NSString*)stringBuilder index:(NSInteger)index;
-(NSInteger)findComplexEnd:(NSString*)stringBuilder index:(NSInteger)index;
-(void)changePriority:(Terminal*)terminal;

@end

@implementation Core

-(Terminal*)scientific:(NSString *)exp
{
    NSInteger funcStart, funcEnd, complexStart, complexEnd, expLenght, terminalsSize, started = -1, offset = 0;
    double complexReal, complexImaginary;
    Terminal *terminal, *leftTerminal, *rightTerminal;
    NSString *expBuilder = @"";
    NSMutableArray *terminals, *computedTerminals;
    RZStack *rpnStack, *compute;
    NSMutableDictionary *complexMap, *functionMap, *complexJump, *rawComplexMap;
    
    terminals = [[NSMutableArray alloc] init];
    computedTerminals = [[NSMutableArray alloc] init];
    rpnStack = [[RZStack alloc] init];
    compute = [[RZStack alloc] init];
    complexMap = [[NSMutableDictionary alloc] init];
    functionMap = [[NSMutableDictionary alloc] init];
    complexJump = [[NSMutableDictionary alloc] init];
    rawComplexMap = [[NSMutableDictionary alloc] init];
    
    expLenght = [exp length];
    for (NSInteger i = 0; i < expLenght;) {
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@","]) {
            complexStart = [self findComplexStart:exp index:i];
            complexEnd = [self findComplexEnd:exp index:i];
            complexReal = [[self scientific:[exp substringWithRange:NSMakeRange(complexStart + 1, i - complexStart - 1)]].realPart doubleValue];
            complexImaginary = [[self scientific:[exp substringWithRange:NSMakeRange(i + 1, complexEnd - i - 1)]].realPart doubleValue];
            [rawComplexMap setValue:[[Terminal alloc] initWithNumbers:complexReal and:complexImaginary] forKey:[NSString stringWithFormat:@"%ld", (long)complexStart]];
            i = complexEnd + 1;
            [complexJump setValue:[NSNumber numberWithInteger:i] forKey:[NSString stringWithFormat:@"%ld", (long)complexStart]];
        }
        else
        {
            ++i;
        }
    }

    for (NSInteger i = 0; i < expLenght; ) {
        if ([[rawComplexMap allKeys] containsObject:[NSString stringWithFormat:@"%ld", (long)i]]) {
            [complexMap setValue:[rawComplexMap objectForKey:[NSString stringWithFormat:@"%ld", (long)i]] forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [expBuilder stringByAppendingString:@"~"];
            i = [[complexJump valueForKey:[NSString stringWithFormat:@"%ld", (long)i]] integerValue];
            continue;
        }
        
        if ([self isSinh1:exp index:i]) {
            funcStart = i + 7;
            funcEnd = [self findFuncEnd:exp index:i + 6];
            terminal = [self sinh1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCosh1:exp index:i]) {
            funcStart = i + 7;
            funcEnd = [self findFuncEnd:exp index:i + 6];
            terminal = [self cosh1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isTgh1:exp index:i]) {
            funcStart = i + 6;
            funcEnd = [self findFuncEnd:exp index:i + 5];
            terminal = [self tgh1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCotgh1:exp index:i]) {
            funcStart = i + 7;
            funcEnd = [self findFuncEnd:exp index:i + 6];
            terminal = [self cotgh1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isSinh:exp index:i]) {
            funcStart = i + 5;
            funcEnd = [self findFuncEnd:exp index:i + 4];
            terminal = [self sinh:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCosh:exp index:i]) {
            funcStart = i + 5;
            funcEnd = [self findFuncEnd:exp index:i + 4];
            terminal = [self cosh:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isTgh:exp index:i]) {
            funcStart = i + 4;
            funcEnd = [self findFuncEnd:exp index:i + 3];
            terminal = [self tgh:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCotgh:exp index:i]) {
            funcStart = i + 5;
            funcEnd = [self findFuncEnd:exp index:i + 4];
            terminal = [self cotgh:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isSin1:exp index:i]) {
            funcStart = i + 6;
            funcEnd = [self findFuncEnd:exp index:i + 5];
            terminal = [self sin1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCos1:exp index:i]) {
            funcStart = i + 6;
            funcEnd = [self findFuncEnd:exp index:i + 5];
            terminal = [self cos1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isTg1:exp index:i]) {
            funcStart = i + 5;
            funcEnd = [self findFuncEnd:exp index:i + 4];
            terminal = [self tg1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCotg1:exp index:i]) {
            funcStart = i + 7;
            funcEnd = [self findFuncEnd:exp index:i + 6];
            terminal = [self cotg1:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isSin:exp index:i]) {
            funcStart = i + 4;
            funcEnd = [self findFuncEnd:exp index:i + 3];
            terminal = [self sin:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCos:exp index:i]) {
            funcStart = i + 4;
            funcEnd = [self findFuncEnd:exp index:i + 3];
            terminal = [self cos:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isTg:exp index:i]) {
            funcStart = i + 3;
            funcEnd = [self findFuncEnd:exp index:i + 2];
            terminal = [self tg:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isCotg:exp index:i]) {
            funcStart = i + 5;
            funcEnd = [self findFuncEnd:exp index:i + 4];
            terminal = [self cotg:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isMod:exp index:i]) {
            funcStart = i + 4;
            funcEnd = [self findFuncEnd:exp index:i + 3];
            [functionMap setValue:[self mod:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]] forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isLog:exp index:i]) {
            funcStart = [self findFuncStart:exp index:i + 3];
            funcEnd = [self findFuncEnd:exp index:i + 3];
            [functionMap setValue:[self logWithExp:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)] andBase:[self extractSubOrSuperScriptForwards:exp index:i mathSymbol:@"$"]] forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isLg:exp index:i]) {
            funcStart = i + 3;
            funcEnd = [self findFuncEnd:exp index:i + 2];
            [functionMap setValue:[self lg:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]] forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([self isLn:exp index:i]) {
            funcStart = i + 3;
            funcEnd = [self findFuncEnd:exp index:i + 2];
            [functionMap setValue:[self ln:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]] forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"√"]) {
            funcStart = i + 2;
            funcEnd = [self findFuncEnd:exp index:i + 1];
            expBuilder = [expBuilder substringToIndex:[expBuilder length] - 3];
            [functionMap setValue:[self root:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)] andRootPower:[self extractSuperScriptBackwards:exp index:i]] forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [NSString stringWithFormat:@"%@`", expBuilder];
            i = funcEnd + 1;
            continue;
        }
        if ([exp characterAtIndex:i] == '!') {
            funcStart = funcEnd = i;
            for (NSInteger j = i - 1; j >= -1; --j) {
                if ((j != -1) && (([[exp substringWithRange:NSMakeRange(j, 1)] integerValue] >= 0 && [[exp substringWithRange:NSMakeRange(j, 1)] integerValue] <= 9) || [[exp substringWithRange:NSMakeRange(j, 1)] isEqualToString:@"."])) {
                    continue;
                }
                else
                {
                    funcStart = j + 1;
                    break;
                }
            }
            expBuilder = [expBuilder substringToIndex:funcEnd - funcStart];
            terminal = [self facturial:[exp substringWithRange:NSMakeRange(funcStart, funcEnd - funcStart)]];
            if (terminal.error_msg) {
                return terminal;
            }
            [functionMap setValue:terminal forKey:[NSString stringWithFormat:@"%lu", (unsigned long)[expBuilder length]]];
            expBuilder = [expBuilder stringByAppendingString:@"`"];
            i = funcEnd + 1;
            continue;
        }
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"e"]) {
            expBuilder = [NSString stringWithFormat:@"%@2.718281828459045", expBuilder];
            ++i;
            continue;
        }
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"π"]) {
            expBuilder = [NSString stringWithFormat:@"%@3.14159265358979323846264", expBuilder];
            ++i;
            continue;
        }
        expBuilder = [NSString stringWithFormat:@"%@%@", expBuilder, [exp substringWithRange:NSMakeRange(i, 1)]];
        ++i;
    }
    
    expLenght = [expBuilder length];
    for (NSInteger i = 0; i < expLenght; ++i) {
        if (([expBuilder characterAtIndex:i] >= '0' && [expBuilder characterAtIndex:i] <= '9') || [[expBuilder substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"."]) {
            if (started == -1) {
                started = i;
            }
            if (i + 1 != expLenght) {
                continue;
            }
            offset = 1;
        }
        if (started != -1) {
            [terminals addObject:[[Terminal alloc] initWithString:[expBuilder substringWithRange:NSMakeRange(started, i - started + offset)]]];
            if (offset == 1) {
                break;
            }
            started = -1;
        }
        if ([expBuilder characterAtIndex:i] == '~') {
            [terminals addObject:[complexMap valueForKey:[NSString stringWithFormat:@"%ld", (long)i]]];
        }
        else if ([[expBuilder substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"`"])
        {
            [terminals addObject:[functionMap valueForKey:[NSString stringWithFormat:@"%ld", (long)i]]];
        }
        else
        {
            //TODO: нещо тука се случва, когато подам cos на ! като аргумент. Да се разследва и да го оправя
            
            [terminals addObject:[[Terminal alloc] initWithSymbolString:[expBuilder substringWithRange:NSMakeRange(i, 1)]]];
        }
    }
    
    terminalsSize = [terminals count];
    for (NSInteger i = 0; i < terminalsSize; ++i) {
        if (((Terminal*)[terminals objectAtIndex:i]).type == NUMBER) {
            [computedTerminals addObject:[terminals objectAtIndex:i]];
        }
        else
        {
            if (((Terminal*)[terminals objectAtIndex:i]).type == MINUS && i + 1 < terminalsSize  &&  ((Terminal*)[terminals objectAtIndex:i + 1]).type == NUMBER) {
                if (i == 0) {
                    ((Terminal*)[terminals objectAtIndex:i + 1]).realPart = [NSNumber numberWithDouble:-[((Terminal*)[terminals objectAtIndex:i + 1]).realPart doubleValue]];
                    ((Terminal*)[terminals objectAtIndex:i + 1]).imaginaryPart = [NSNumber numberWithDouble:-[((Terminal*)[terminals objectAtIndex:i + 1]).imaginaryPart doubleValue]];
                    continue;
                } else if (((Terminal*)[terminals objectAtIndex:i - 1]).type == LEFT_BRACKET) {
                    ((Terminal*)[terminals objectAtIndex:i + 1]).realPart = [NSNumber numberWithDouble:-[((Terminal*)[terminals objectAtIndex:i + 1]).realPart doubleValue]];
                    ((Terminal*)[terminals objectAtIndex:i + 1]).imaginaryPart = [NSNumber numberWithDouble:-[((Terminal*)[terminals objectAtIndex:i + 1]).imaginaryPart doubleValue]];
                    continue;
                }
            }
            if ([rpnStack.stackArray count] != 0) {
                if (((Terminal*)[terminals objectAtIndex:i]).priority > ((Terminal*)[rpnStack peekLast]).priority) {
                    [self changePriority:(Terminal*)[terminals objectAtIndex:i]];
                    [rpnStack pushObject:terminals[i]];
                }
                else
                {
                    terminal = (Terminal*)[rpnStack popObject];
                    if (terminal.type != LEFT_BRACKET) {
                        [computedTerminals addObject:terminal];
                    }
                    if (((Terminal*)[terminals objectAtIndex:i]).type != RIGHT_BRACKET || terminal.type != LEFT_BRACKET) {
                        --i;
                    }
                }
            }
            else
            {
                [self changePriority:(Terminal*)[terminals objectAtIndex:i]];
                [rpnStack pushObject:terminals[i]];
            }
        }
    }
    while ([rpnStack.stackArray count] != 0) {
        [computedTerminals addObject:[rpnStack popObject]];
    }
    
    /*for (int i = 0; i < [computedTerminals count];  ++i) {
        Terminal *t = [computedTerminals objectAtIndex:i];
    }*/
    
    for (NSInteger i = 0; i < [computedTerminals count]; ++i) {
        if (((Terminal*)[computedTerminals objectAtIndex:i]).type == NUMBER) {
            [compute pushObject:[computedTerminals objectAtIndex:i]];
        }
        else
        {
            rightTerminal = [compute popObject];
            if ([compute.stackArray count] > 0) {
                leftTerminal = [compute popObject];
            }
            else
            {
                leftTerminal = [[Terminal alloc] initWithNumbers:0 and:0];
            }
            switch (((Terminal*)[computedTerminals objectAtIndex:i]).type) {
                case PLUS:
                    [compute pushObject:[leftTerminal add:rightTerminal]];
                    break;
                case MINUS:
                    [compute pushObject:[leftTerminal subtract:rightTerminal]];
                    break;
                case MULTIPLY:
                    [compute pushObject:[leftTerminal multiplyBy:rightTerminal]];
                    break;
                case DIVIDE:
                    [compute pushObject:[leftTerminal divideBy:rightTerminal]];
                    break;
                case POWER:
                    [compute pushObject:[self powerOfBase:leftTerminal andPowerFactor:rightTerminal]];
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    /*for (int i = 0; i < [compute.stackArray count];  ++i) {
        Terminal *t = [compute peekAtIndex:i];
        NSLog(@"%ld!%f!%f", (long)t.type, [t.realPart doubleValue], [t.imaginaryPart doubleValue]);
    }*/
    terminal = [compute popObject];
    return terminal;
}

-(Matrix*)matrixTranspose:(Matrix *)a
{
    Matrix *result = [[Matrix alloc] initWithSizeX:a.m andY:a.n];
    for (NSInteger i = 0; i < a.n; ++i) {
        for (NSInteger j = 0; j < a.m; ++j) {
            [result.data setObject:[[Terminal alloc] initWithNumbers:[((Terminal*)[a.data objectInRow:i andColumn:j]).realPart doubleValue] and:[((Terminal*)[a.data objectInRow:i andColumn:j]).imaginaryPart doubleValue]] inRow:j andColumn:i];
        }
    }
    
    return result;
}

-(Matrix*)matrixInverse:(Matrix *)a
{
    NSInteger rightRow, size;
    Terminal *zero, *one, *swap, *divider;
    Matrix *result = [[Matrix alloc] initWithSizeX:a.n andY:a.m * 2];
    zero = [[Terminal alloc] initWithNumbers:0.0 and:0.0];
    one = [[Terminal alloc] initWithNumbers:1.0 and:0.0];
    
    for (NSInteger i = a.n - 1; i >= 0; --i) {
        for (NSInteger j = a.m - 1; j >= 0; --j) {
            [result.data setObject:[[Terminal alloc] initWithNumbers:[((Terminal*)[a.data objectInRow:i andColumn:j]).realPart doubleValue] and:[((Terminal*)[a.data objectInRow:i andColumn:j]).imaginaryPart doubleValue]] inRow:i andColumn:j];
        }
    }
    
    for (NSInteger i = a.n - 1; i >= 0; --i) {
        for (NSInteger j = (a.m * 2) - 1; j >= a.m; --j) {
            if ((i + a.m) == j) {
                [result.data setObject:one inRow:i andColumn:j];
            }
            else
            {
                [result.data setObject:zero inRow:i andColumn:j];
            }
        }
    }
    
    
    //up-down elimination
    for (NSInteger i = 0; i < result.n; ++i) {
        if (i > result.n || i >= result.m) {
            break;
        }
        
        rightRow = i;
        while (rightRow < result.n && ((Terminal*)[result.data objectInRow:rightRow andColumn:i]).isZeroSelfCheck) {
            ++rightRow;
        }
        if (rightRow == result.n) {
            continue;
        }
        if (rightRow != i) {
            for (NSInteger j = result.m - 1; j >= 0; --j) {
                swap = [result.data objectInRow:i andColumn:j];
                [result.data setObject:[result.data objectInRow:rightRow andColumn:j] inRow:i andColumn:j];
                [result.data setObject:swap inRow:rightRow andColumn:j];
            }
        }
        
        for (NSInteger j = i + 1; j < result.n; ++j) {
            divider = [((Terminal*)[result.data objectInRow:j andColumn:i]) divideBy:((Terminal*)[result.data objectInRow:i andColumn:i])];
            for (NSInteger k = i; k < result.m; ++k) {
                [result.data setObject:([(Terminal*)[result.data objectInRow:j andColumn:k] subtract:([(Terminal*)[result.data objectInRow:i andColumn:k] multiplyBy:divider])]) inRow:j andColumn:k];
            }
        }
    }
    
    //down-up elimination
    for (NSInteger i = 0; i < result.n; ++i) {
        if (i >= result.n || i >= result.m) {
            break;
        }
        
        rightRow = i;
        while (rightRow >= 0 && [((Terminal*)[result.data objectInRow:rightRow andColumn:i]) isZeroSelfCheck]) {
            --rightRow;
        }
        if (rightRow == -1) {
            continue;
        }
        
        if (rightRow != i) {
            for (NSInteger j = result.m - 1; j >= 0; --j) {
                swap = [result.data objectInRow:i andColumn:j];
                [result.data setObject:[result.data objectInRow:rightRow andColumn:j] inRow:i andColumn:j];
                [result.data setObject:swap inRow:rightRow andColumn:j];
            }
        }
        
        for (NSInteger j = i - 1; j >= 0; --j) {
            divider = [((Terminal*)[result.data objectInRow:j andColumn:i]) divideBy:((Terminal*)[result.data objectInRow:i andColumn:i])];
            for (NSInteger k = i; k < result.m; ++k) {
                [result.data setObject:([(Terminal*)[result.data objectInRow:j andColumn:k] subtract:([(Terminal*)[result.data objectInRow:i andColumn:k] multiplyBy:divider])]) inRow:j andColumn:k];
            }
        }
    }
    
    if (a.n < a.m) {
        size = a.n;
    }
    else
    {
        size = a.m;
    }
    
    for (NSInteger i = 0; i < result.n; ++i) {
        divider = [one divideBy:((Terminal*)[result.data objectInRow:i andColumn:i])];
        for (NSInteger j = 0; j < result.m; ++j) {
            [result.data setObject:[((Terminal*)[result.data objectInRow:i andColumn:j]) multiplyBy:divider] inRow:i andColumn:j];
        }
    }
    
    for (NSInteger i = result.n - 1; i >= 0; --i) {
        for (NSInteger j = a.m - 1; j >= 0; --j) {
            [result.data setObject:[result.data objectInRow:i andColumn:j + a.m] inRow:i andColumn:j];
        }
    }
    
    [result setMDimension:a.m];
    
    return result;
}

-(Terminal*)matrixDeterminant:(Matrix *)a
{
    Terminal *result;
    Matrix *temp;
    
    if (a.n != a.m) {
        result = [[Terminal alloc] initWithNumbers:0.0 and:0.0];
        result.error_msg = @"Not a square matrix";
        return result;
    }
    
    temp = [self gauseElimination:a];
    result = [[Terminal alloc] initWithNumbers:1.0 and:0.0];
    for (NSInteger i = a.n - 1; i >= 0; --i) {
        result = [result multiplyBy:((Terminal*)[temp.data objectInRow:i andColumn:i])];
    }
    
    return result;
}

-(NSInteger)matrixRank:(Matrix *)a
{
    BOOL isValidRow;
    NSInteger result = a.n;
    Matrix *temp = [self gauseElimination:a];
    
    for (NSInteger i = temp.n - 1; i >= 0; --i) {
        isValidRow = NO;
        for (NSInteger j = temp.m - 1; j >= 0; --j) {
            if (![((Terminal*)[temp.data objectInRow:i andColumn:j]) isZeroSelfCheck]) {
                isValidRow = YES;
                break;
            }
        }
        if (!isValidRow) {
            --result;
        }
    }
    
    return result;
}

-(Terminal*)sin:(NSString *)exp
{
    Terminal *terminal = [self scientific:[exp substringFromIndex:1]];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        if ([exp characterAtIndex:0] == 'r') {
            terminal.realPart = [NSNumber numberWithDouble: sin([terminal.realPart doubleValue])];
        }
        else
        {
            terminal.realPart = [NSNumber numberWithDouble:sin([terminal.realPart doubleValue] * M_PI / 180)];
        }
    }
    else
    {
        terminal.error_msg = @"Complex number as sin() argument";
    }
    
    return terminal;
}

-(Terminal*)sin1:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0 && [terminal.realPart doubleValue] >= -1 && [terminal.realPart doubleValue] <= 1) {
        terminal.realPart = [NSNumber numberWithDouble:asin([terminal.realPart doubleValue])];
    }
    else
    {
        terminal.error_msg = @"Complex number as sin-1() argument";
    }
    
    return terminal;
}

-(Terminal*)sinh:(NSString *)exp
{
    
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:sinh([terminal.realPart doubleValue])];
    }
    else
    {
        terminal.error_msg = @"Comlex number as sinh() argument";
    }
    
    return terminal;
}

-(Terminal*)sinh1:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:log([terminal.realPart doubleValue] + sqrt([terminal.realPart doubleValue] * [terminal.realPart doubleValue] + 1))];
    }
    else
    {
        terminal.error_msg = @"Complex number as sinh-1() argument";
    }
    
    return terminal;
}

-(Terminal*)cos:(NSString *)exp
{
    Terminal *terminal = [self scientific:[exp substringFromIndex:1]];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        if ([exp characterAtIndex:0] == 'r') {
            terminal.realPart = [NSNumber numberWithDouble:cos([terminal.realPart doubleValue])];
        }
        else
        {
            terminal.realPart = [NSNumber numberWithDouble:cos([terminal.realPart doubleValue] * M_PI / 180)];
        }
    }
    else
    {
        terminal.error_msg = @"Complex number as cos() argument";
    }
    
    return terminal;
}

-(Terminal*)cos1:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0 && [terminal.realPart doubleValue] >= -1 && [terminal.realPart doubleValue] <= 1) {
        terminal.realPart = [NSNumber numberWithDouble:acos([terminal.realPart doubleValue])];
    }
    else
    {
        terminal.error_msg = @"Complex number as cos-1() argument";
    }
    
    return terminal;
}

-(Terminal*)cosh:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:cosh([terminal.realPart doubleValue])];
    }
    else
    {
        terminal.error_msg = @"Complex number as cosh() function";
    }
    
    return terminal;
}

-(Terminal*)cosh1:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:log([terminal.realPart doubleValue] + sqrt([terminal.realPart doubleValue] * [terminal.realPart doubleValue] - 1))];
    }
    else
    {
        terminal.error_msg = @"Complex number as cosh-1() argument";
    }
    
    return terminal;
}

-(Terminal*)tg:(NSString *)exp
{
    Terminal *terminal = [self scientific:[exp substringFromIndex:1]];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        if ([exp characterAtIndex:0] == 'r') {
            terminal.realPart = [NSNumber numberWithDouble:tan([terminal.realPart doubleValue])];
        }
        else
        {
            terminal.realPart = [NSNumber numberWithDouble:tan([terminal.realPart doubleValue] * M_PI / 180)];
        }
    }
    else
    {
        terminal.error_msg = @"Complex number as tg() argument";
    }
    
    return terminal;
}

-(Terminal*)tg1:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:atan([terminal.realPart doubleValue])];
    }
    else
    {
        terminal.error_msg = @"Complex number as tan-1() argument";
    }
    
    return terminal;
}

-(Terminal*)tgh:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:tanh([terminal.realPart doubleValue])];
    }
    else{
        terminal.error_msg = @"Complex number as tanh() argument";
    }
    
    return terminal;
}

-(Terminal*)tgh1:(NSString *)exp
{
    Terminal* terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0 && [terminal.realPart doubleValue] > -1 && [terminal.realPart doubleValue] < 1) {
        terminal.realPart = [NSNumber numberWithDouble:0.5 * log((1 + [terminal.realPart doubleValue]) / (1 - [terminal.realPart doubleValue]))];
    }
    else
    {
        terminal.error_msg = @"Complex number as tanh-1() argument";
    }
    
    return terminal;
}

-(Terminal*)cotg:(NSString *)exp
{
    Terminal *terminal = [self scientific:[exp substringFromIndex:1]];
    if([terminal.imaginaryPart doubleValue] == 0)
    {
        if ([exp characterAtIndex:0] == 'r') {
            terminal.realPart = [NSNumber numberWithDouble:1.0/tan([terminal.realPart doubleValue])];
        }
        else
        {
            terminal.realPart = [NSNumber numberWithDouble:1.0 / tan([terminal.realPart doubleValue] * M_PI / 180)];
        }
    }
    else
    {
        terminal.error_msg = @"Complex number as cotg() arggument";
    }
    if ([terminal.realPart doubleValue] > DBL_MAX) {
        terminal.realPart = [NSNumber numberWithDouble:DBL_MAX];
    }
    
    return terminal;
}

-(Terminal*)cotg1:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:0.5 * M_PI - atan([terminal.realPart doubleValue])];
    }
    else
    {
        terminal.error_msg = @"Complex number as cotg-1() argument";
    }
    
    return terminal;
}

-(Terminal*)cotgh:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0) {
        terminal.realPart = [NSNumber numberWithDouble:1.0 / tanh([terminal.realPart doubleValue])];
    }
    else
    {
        terminal.error_msg = @"Complec number as cotgh() argument";
    }
    if ([terminal.realPart doubleValue] > DBL_MAX) {
        terminal.realPart = [NSNumber numberWithDouble:DBL_MAX];
    }
    
    return terminal;
}

-(Terminal*)cotgh1:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0 && ([terminal.realPart doubleValue] > 1 || [terminal.realPart doubleValue] < -1)) {
        terminal.realPart = [NSNumber numberWithDouble:0.5 * log(([terminal.realPart doubleValue] + 1) / ([terminal.realPart doubleValue] - 1))];
    }
    else
    {
        terminal.error_msg = @"Complex number as cotgh-1() argument";
    }
    
    return terminal;
}

-(Terminal*)mod:(NSString *)exp
{
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0.0) {
        if ([terminal.realPart doubleValue] < 0.0) {
            terminal.realPart = [NSNumber numberWithDouble:-[terminal.realPart doubleValue]];
        }
    }
    else
    {
        terminal.realPart = [NSNumber numberWithDouble:sqrt([terminal.realPart doubleValue] * [terminal.realPart doubleValue] + [terminal.imaginaryPart doubleValue] * [terminal.imaginaryPart doubleValue])];
        terminal.imaginaryPart = [NSNumber numberWithDouble:0.0];
    }
    return terminal;
}

-(Terminal*)logWithExp:(NSString *)exp andBase:(NSString *)base
{
    Terminal *terminal = [self scientific:exp];
    
    return [Terminal logWithExponent:terminal andBase:[base doubleValue]];
}

-(Terminal*)lg:(NSString *)exp
{
    return [self logWithExp:exp andBase:@"10"];
}

-(Terminal*)ln:(NSString *)exp
{
    return [self logWithExp:exp andBase:@"2.718281828459045"];
}

-(Terminal*)facturial:(NSString *)exp
{
    double initValue;
    Terminal *terminal = [self scientific:exp];
    if ([terminal.imaginaryPart doubleValue] == 0 && [terminal.realPart doubleValue] >= 0) {
        if (floor([terminal.realPart doubleValue]) == [terminal.realPart doubleValue]) {
            initValue = [terminal.realPart doubleValue];
            terminal.realPart = [NSNumber numberWithDouble:1.0];
            for (NSInteger i = 2; i <= initValue; ++i) {
                terminal.realPart = [NSNumber numberWithDouble:i * [terminal.realPart doubleValue]];
            }
        }
        else
        {
            terminal.realPart = [NSNumber numberWithDouble:[self gammaFunction:[terminal.realPart doubleValue] + 1]];
        }
    }
    else
    {
        terminal.error_msg = @"Complex or negative value as facturial() argument";
    }
    
    return terminal;
}

-(Terminal*)root:(NSString *)exp andRootPower:(NSString *)rootPower
{
    Terminal *terminal = [self scientific:exp];
    return [self powerOfBase:terminal andPowerFactor:[[Terminal alloc] initWithNumbers:1.0/[rootPower integerValue] and:0.0]];
}

-(double)gammaFunction:(double)value
{
    NSInteger prec = 16;
    double accum;
    NSMutableArray *cSpace;
    cSpace = [[NSMutableArray alloc] init];
    double kFactor = 1.0;
    [cSpace addObject:[NSNumber numberWithDouble:sqrt(2.0 * M_PI)]];
    for (NSInteger k = 1; k < prec; ++k) {
        [cSpace addObject:[NSNumber numberWithDouble:exp(prec - k) * pow(prec - k, k - 0.5) / kFactor]];
        kFactor *= -k;
    }
    
    accum = [[cSpace objectAtIndex:0] doubleValue];
    for (NSInteger k = 1; k < prec; ++k) {
        accum += [[cSpace objectAtIndex:k] doubleValue] / (value + k);
    }
    
    accum *= exp(-(value + prec)) * pow(value + prec, value + 0.5);
    
    return accum / value;
}

-(BOOL)isSinh1:(NSString *)exp index:(NSInteger)index
{
    if (index + 5 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 6)] isEqualToString:@"sinh-1"];
}

-(BOOL)isCosh1:(NSString *)exp index:(NSInteger)index
{
    if (index + 5 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 6)] isEqualToString:@"cosh-1"];
}

-(BOOL)isTgh1:(NSString *)exp index:(NSInteger)index
{
    if (index + 4 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 5)] isEqualToString:@"tgh-1"];
}

-(BOOL)isCotgh1:(NSString *)exp index:(NSInteger)index
{
    if (index + 5 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 6)] isEqualToString:@"coth-1"];
}

-(BOOL)isSinh:(NSString *)exp index:(NSInteger)index
{
    if (index + 3 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 4)] isEqualToString:@"sinh"];
}

-(BOOL)isCosh:(NSString *)exp index:(NSInteger)index
{
    if (index + 3 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 4)] isEqualToString:@"cosh"];
}

-(BOOL)isTgh:(NSString *)exp index:(NSInteger)index
{
    if (index + 2 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 3)] isEqualToString:@"tgh"];
}

-(BOOL)isCotgh:(NSString *)exp index:(NSInteger)index
{
    if (index + 3 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 4)] isEqualToString:@"coth"];
}

-(BOOL)isSin1:(NSString *)exp index:(NSInteger)index
{
    if (index + 4 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 5)] isEqualToString:@"sin-1"];
}

-(BOOL)isCos1:(NSString *)exp index:(NSInteger)index
{
    if (index + 4  >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 5)] isEqualToString:@"cos-1"];
}

-(BOOL)isTg1:(NSString *)exp index:(NSInteger)index
{
    if (index + 3 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 4)] isEqualToString:@"tg-1"];
}

-(BOOL)isCotg1:(NSString *)exp index:(NSInteger)index
{
    if (index + 5 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 6)] isEqualToString:@"cotg-1"];
}

-(BOOL)isSin:(NSString *)exp index:(NSInteger)index
{
    if (index + 2 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 3)] isEqualToString:@"sin"];
}

-(BOOL)isCos:(NSString *)exp index:(NSInteger)index
{
    if (index + 2 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 3)] isEqualToString:@"cos"];
}

-(BOOL)isTg:(NSString *)exp index:(NSInteger)index
{
    if (index + 1 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 2)] isEqualToString:@"tg"];
}

-(BOOL)isCotg:(NSString *)exp index:(NSInteger)index
{
    if (index + 3 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 4)] isEqualToString:@"cotg"];
}

-(BOOL)isMod:(NSString *)exp index:(NSInteger)index
{
    if (index + 2 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 3)] isEqualToString:@"mod"];
}

-(BOOL)isLog:(NSString *)exp index:(NSInteger)index
{
    if (index + 2 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 3)] isEqualToString:@"log"];
}

-(BOOL)isLg:(NSString *)exp index:(NSInteger)index
{
    if (index + 1 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 2)] isEqualToString:@"lg"];
}

-(BOOL)isLn:(NSString *)exp index:(NSInteger)index
{
    if (index + 1 >= [exp length]) {
        return NO;
    }
    return [[exp substringWithRange:NSMakeRange(index, 2)] isEqualToString:@"ln"];
}

-(NSString*)extractSuperScriptBackwards:(NSString *)exp index:(NSInteger)index
{
    NSInteger endIndex = -1;
    for (NSInteger i = index; i >= 0; --i) {
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"@"]) {
            if (endIndex != -1) {
                return [exp substringWithRange:NSMakeRange(i + 1, endIndex - i - 1)];
            }
            else
            {
                endIndex = i;
            }
        }
    }
    
    return @"";
}

-(NSString*)extractSubOrSuperScriptForwards:(NSString *)exp index:(NSInteger)index mathSymbol:(NSString *)symbol
{
    NSInteger startIndex = -1;
    NSInteger expLenght = [exp length];
    for (NSInteger i = index; i < expLenght; ++i) {
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:symbol]) {
            if (startIndex != -1) {
                return [exp substringWithRange:NSMakeRange(startIndex, i - startIndex)];
            }
            else
            {
                startIndex = i + 1;
            }
        }
    }
    
    return @"";
}

-(NSInteger)findFuncStart:(NSString *)exp index:(NSInteger)index
{
    NSInteger expLenght = [exp length] - 1;
    for (NSInteger i = index; i < expLenght; ++i) {
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"("]) {
            return i + 1;
        }
    }
    
    return index;
}

-(NSInteger)findFuncEnd:(NSString *)exp index:(NSInteger)index
{
    NSInteger i, expLenght, bracketCount = 0;
    
    expLenght = [exp length];
    for (i = index; i < expLenght; ++i) {
        if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"("]) {
            ++bracketCount;
        }
        else if ([[exp substringWithRange:NSMakeRange(i, 1)] isEqualToString:@")"])
        {
            --bracketCount;
            if (bracketCount == 0) {
                break;
            }
        }
    }
    
    return i;
}

-(NSInteger)findComplexStart:(NSString *)stringBuilder index:(NSInteger)index
{
    NSInteger i, bracketCount = 0;
    for (i = index; i >= 0; --i) {
        if ([[stringBuilder substringWithRange:NSMakeRange(i, 1)] isEqualToString:@")"]) {
            ++bracketCount;
        }
        else if ([[stringBuilder substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"("])
        {
            --bracketCount;
            if (bracketCount == -1) {
                break;
            }
        }
    }
    
    return i;
}


-(NSInteger)findComplexEnd:(NSString *)stringBuilder index:(NSInteger)index
{
    NSInteger i, size, bracketCount = 0;
    size = [stringBuilder length];
    for (i = index; i < size; ++i) {
        if ([[stringBuilder substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"("]) {
            ++bracketCount;
        }
        else if ([[stringBuilder substringWithRange:NSMakeRange(i, 1)] isEqualToString:@")"])
        {
            --bracketCount;
            if (bracketCount == -1) {
                break;
            }
        }
    }
    
    return i;
}

-(void)changePriority:(Terminal *)terminal
{
    switch (terminal.type) {
        case LEFT_BRACKET:
            terminal.priority = 1;
            break;
            
            case POWER:
            terminal.priority = 4;
            break;
    }
}

-(Matrix*)gauseElimination:(Matrix *)a
{
    NSInteger rightRow;
    Terminal *divider, *swap;
    Matrix *result = [[Matrix alloc] initWithSizeX:a.n andY:a.m];
    
    for (NSInteger i = result.n - 1; i >= 0; --i) {
        for (NSInteger j = result.m - 1; j >= 0; --j) {
            [result.data setObject:[[Terminal alloc] initWithNumbers:[((Terminal*)[a.data objectInRow:i andColumn:j]).realPart doubleValue] and:[((Terminal*)[a.data objectInRow:i andColumn:j]).imaginaryPart doubleValue]] inRow:i andColumn:j];
        }
    }
    
    for (NSInteger i = 0; i < result.n; ++i) {
        if (i >= result.n || i >= result.m) {
            break;
        }
        
        rightRow = i;
        while (rightRow < result.n && ((Terminal*)[result.data objectInRow:rightRow andColumn:i]).isZeroSelfCheck) {
            ++rightRow;
        }
        if (rightRow == result.n) {
            continue;
        }
        
        if (rightRow != i) {
            for (NSInteger j = result.m - 1; j >= 0; --j) {
                swap = [result.data objectInRow:i andColumn:j];
                [result.data setObject:[result.data objectInRow:rightRow andColumn:j] inRow:i andColumn:j];
                [result.data setObject:swap inRow:rightRow andColumn:j];
            }
        }
        
        for (NSInteger j = i + 1; j < result.n; ++j) {
            divider = [((Terminal*)[result.data objectInRow:j andColumn:i]) divideBy:((Terminal*)[result.data objectInRow:i andColumn:i])];
            for (NSInteger k = i; k < result.m; ++k) {
                [result.data setObject:[((Terminal*)[result.data objectInRow:j andColumn:k]) subtract:[((Terminal*)[result.data objectInRow:i andColumn:k]) multiplyBy:divider]] inRow:j andColumn:k];
            }
        }
    }
    
    return result;
}

-(Terminal*)powerOfBase:(Terminal *)baseValue andPowerFactor:(Terminal *)powerValue
{
    BOOL isComplexMethod = NO;
    NSInteger powerRealPartCasted = 0;
    
    if ([powerValue.realPart doubleValue] != 0) {
        isComplexMethod = YES;
    }
    else
    {
        powerRealPartCasted = (NSInteger)[powerValue.realPart doubleValue];
        if ([powerValue.realPart doubleValue] - (double)powerRealPartCasted != 0.0) {
            isComplexMethod = YES;
        }
    }
    
    if (isComplexMethod) {
        double complex cPower, cBase = [baseValue.realPart doubleValue] + [baseValue.imaginaryPart doubleValue] * I;
        cPower = [powerValue.realPart doubleValue] + [powerValue.imaginaryPart doubleValue] * I;
        cBase = cpow(cBase, cPower);
        
        return [[Terminal alloc] initWithNumbers:creal(cBase) and:cimag(cBase)];
    }
    
    if (powerRealPartCasted == 0) {
        return [[Terminal alloc] initWithNumbers:1 and:0];
    }
    if (powerRealPartCasted == 1) {
        return baseValue;
    }
    if ((powerRealPartCasted & 1) != 0) {
        powerValue.realPart = [NSNumber numberWithDouble:[powerValue.realPart doubleValue] - 1];
        return [baseValue multiplyBy:[self powerOfBase:baseValue andPowerFactor:powerValue]];
    }
    
    powerValue.realPart = [NSNumber numberWithDouble:0.5 * [powerValue.realPart doubleValue]];
    Terminal *arg = [self powerOfBase:baseValue andPowerFactor:powerValue];
    
    return [arg multiplyBy:arg];
}

@end
