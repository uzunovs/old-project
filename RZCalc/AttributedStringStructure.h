//
//  AttributedStringStructure.h
//  RZCalc
//
//  Created by Hristo Uzunov on 11/15/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    NonAttributedElement = 0,
    SuperScriptAttributedElement = 1,
    SubSscriptattributedElement = -1,
    UndefinedElemet = 2
}ElementTypes;

@interface AttributedStringStructure : NSObject

@property (nonatomic) NSInteger start;
@property (nonatomic) NSInteger stop;
@property (nonatomic, strong) NSString *type;
@property (nonatomic) BOOL isIndexAdjusted;

-(id)init;
-(NSString*)description;

@end
