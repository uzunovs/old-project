//
//  main.m
//  RZCalc
//
//  Created by Hristo Uzunov on 9/30/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RZCalcAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RZCalcAppDelegate class]));
    }
}
