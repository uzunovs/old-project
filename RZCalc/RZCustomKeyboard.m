//
//  RZCustomKeyboard.m
//  RZCalc
//
//  Created by Hristo Uzunov on 1/26/14.
//  Copyright (c) 2014 Hristo Uzunov. All rights reserved.
//

#import "RZCustomKeyboard.h"

@interface RZCustomKeyboard ()

@property (nonatomic, strong) UIView *keyboard;

@end

@implementation RZCustomKeyboard

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 533, 768, 225);
        self.backgroundColor = [UIColor colorWithRed:(CGFloat)208/255.0f green:(CGFloat)208/255.0f blue:(CGFloat)208/255.0f alpha:1];
        CGFloat edgeIndent = 25;
        CGFloat buttonIndent = 84;
        CGFloat size = 50;
        for (NSInteger i = 0; i < 10; ++i) {
            UIButton *button;
            if (i < 5) {
                button = [[UIButton alloc] initWithFrame:CGRectMake(edgeIndent + i * (buttonIndent + size), edgeIndent, size, size)];
                [button setTitle:[NSString stringWithFormat:@"%ld", (long)i + 1] forState:UIControlStateNormal];
            }
            else
            {
                button = [[UIButton alloc] initWithFrame:CGRectMake(edgeIndent + (i - 5) * (buttonIndent + size), 2 * (edgeIndent + size), size, size)];
                [button setTitle:[NSString stringWithFormat:@"%ld", (long)i + 1] forState:UIControlStateNormal];
                if (i + 1 == 10) {
                    [button setTitle:@"0" forState:UIControlStateNormal];
                }
            }
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor colorWithRed:0 green:(CGFloat)119/255.0f blue:206/255.0f alpha:1]];
            [button addTarget:self action:@selector(customInput:) forControlEvents:UIControlEventTouchUpInside];
            button.layer.masksToBounds = NO;
            button.layer.cornerRadius = 10;
            button.layer.shadowRadius = 1;
            button.layer.shadowColor = [[UIColor blackColor] CGColor];
            button.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
            button.layer.shadowOpacity = 1;
            [self addSubview:button];
        }
        
        UIButton *deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(693, 83, size, size)];
        [deleteButton setTitle:@"⌫" forState:UIControlStateNormal];
        [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [deleteButton setBackgroundColor:[UIColor colorWithRed:0 green:(CGFloat)119/255.0f blue:206/255.0f alpha:1]];
        [deleteButton addTarget:self action:@selector(deleteButtonAction) forControlEvents:UIControlEventTouchUpInside];
        deleteButton.layer.masksToBounds = NO;
        deleteButton.layer.cornerRadius = 10;
        deleteButton.layer.shadowRadius = 1;
        deleteButton.layer.shadowColor = [[UIColor blackColor] CGColor];
        deleteButton.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
        deleteButton.layer.shadowOpacity = 1;
        [self addSubview:deleteButton];
    }
    
    return self;
}

-(void)customInput:(UIButton*)sender
{
    [self.keyboardDelegate input:sender.currentTitle];
}

-(void)deleteButtonAction
{
    [self.keyboardDelegate deleteInput];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
