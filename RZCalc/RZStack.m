//
//  RZStack.m
//  RZCalc
//
//  Created by Hristo Uzunov on 11/5/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "RZStack.h"
#import "Terminal.h"

@interface RZStack ()

@property (nonatomic) NSInteger currentCount;

@end

@implementation RZStack

@synthesize stackArray = _stackArray;

static int counter = 0;

-(id)init
{
    self = [super init];
    if (self) {
        self.currentCount = counter;
        ++counter;
    }
    
    return self;
}

-(void)setStackArray:(NSMutableArray *)stackArray
{
    _stackArray = stackArray;
    //NSLog(@"Stack is set to: %@ for stack: %ld", _stackArray, (long)self.currentCount);
}

-(NSMutableArray*)stackArray
{
    if (!_stackArray) {
        _stackArray = [[NSMutableArray alloc] init];
    }
    
    //NSLog(@"Stack is: %@ for stack: %ld", _stackArray, (long)self.currentCount);
    
    return _stackArray;
}

-(void)pushObject:(id)object
{
    [self.stackArray addObject:object];
}

-(id)popObject
{
    id object = [self.stackArray lastObject];
    [self.stackArray removeLastObject];
    
    return object;
}

-(id)peekFirst
{
    id object = [self.stackArray firstObject];
    
    return object;
}

-(id)peekLast
{
    id object = [self.stackArray lastObject];
    
    return object;
}

-(id)peekAtIndex:(NSInteger)index
{
    id object = [self.stackArray objectAtIndex:index];
    
    return object;
}

-(NSUInteger)lastObjectIndex
{
    return [self.stackArray indexOfObject:[self.stackArray lastObject]];
}

-(NSInteger)countOfStackArray
{
    return [self.stackArray count];
}

-(void)eraseStack
{
    [self.stackArray removeAllObjects];
}
@end
