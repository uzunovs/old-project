//
//  Matrix.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/9/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "Matrix.h"

@implementation Matrix

-(id)initWithSizeX:(NSInteger)n andY:(NSInteger)m
{
    self = [super init];
    if (self) {
        self.n = n;
        self.m = m;
        self.data = [[TwoDArray alloc] initWithRows:n andColumns:m];
    }
    
    return self;
}

-(void)getElement
{
    
}

-(Matrix*)add:(Matrix *)leftMatrix to:(Matrix *)rightMatrix
{
    Matrix *result;
    if (leftMatrix.n != rightMatrix.n || leftMatrix.m != rightMatrix.m) {
        result = [[Matrix alloc] initWithSizeX:0 andY:0];
        result.error_msg = @"Wrong matrix size";
        return result;
    }
    
    result = [[Matrix alloc] initWithSizeX:leftMatrix.n andY:leftMatrix.m];
    for (NSInteger i = leftMatrix.n - 1; i >= 0; --i) {
        for (NSInteger j = leftMatrix.m - 1; j >= 0; --j) {
            Terminal *newTerminal = [[Terminal alloc] init];
            newTerminal = [newTerminal add:[leftMatrix.data objectInRow:i andColumn:j] to:[rightMatrix.data objectInRow:i andColumn:j]];
            [result.data setObject:newTerminal inRow:i andColumn:j];
        }
    }
    
    return result;
}

-(Matrix*)addTo:(Matrix *)matrix
{
    Matrix *result;
    if (self.n != matrix.n || self.m != matrix.m) {
        result = [[Matrix alloc] initWithSizeX:0 andY:0];
        result.error_msg = @"Wrong matrix size";
        return result;
    }
    
    result = [[Matrix alloc] initWithSizeX:self.n andY:matrix.m];
    for (NSInteger i = self.n - 1; i >= 0; --i) {
        for (NSInteger j = self.m - 1; j >= 0; --j) {
            Terminal *newTerminal = [[Terminal alloc] init];
            newTerminal = [newTerminal add:[self.data objectInRow:i andColumn:j] to:[matrix.data objectInRow:i andColumn:j]];
            [result.data setObject:newTerminal inRow:i andColumn:j];
        }
    }
    
    return result;
}

-(Matrix*)subtract:(Matrix *)leftMatrix from:(Matrix *)rightMatrix
{
    Matrix *result;
    if (leftMatrix.n != rightMatrix.n || leftMatrix.m != rightMatrix.m) {
        result = [[Matrix alloc] initWithSizeX:0 andY:0];
        result.error_msg = @"Wrong matrix size";
        return result;
    }
    
    result = [[Matrix alloc] initWithSizeX:leftMatrix.n andY:leftMatrix.m];
    
    for (NSInteger i = leftMatrix.n - 1; i >= 0; --i) {
        for (NSInteger j = leftMatrix.m - 1; j >= 0; --j) {
            Terminal *newTerminal = [[Terminal alloc] init];
            newTerminal = [newTerminal subtract:[leftMatrix.data objectInRow:i andColumn:j] from:[rightMatrix.data objectInRow:i andColumn:j]];
            [result.data setObject:newTerminal inRow:i andColumn:j];
        }
    }
    
    return result;
}

-(Matrix*)subtractMatrix:(Matrix*)matrix
{
    Matrix *result;
    if (self.n != matrix.n || self.m != matrix.m) {
        result = [[Matrix alloc] initWithSizeX:0 andY:0];
        result.error_msg = @"Wrong matrix size";
        return result;
    }
    
    result = [[Matrix alloc] initWithSizeX:self.n andY:self.m];
    
    for (NSInteger i = self.n - 1; i >= 0; --i) {
        for (NSInteger j = self.m - 1; j >= 0; --j) {
            Terminal *newTerminal = [[Terminal alloc] init];
            newTerminal = [newTerminal subtract:[self.data objectInRow:i andColumn:j] from:[matrix.data objectInRow:i andColumn:j]];
            [result.data setObject:newTerminal inRow:i andColumn:j];
        }
    }
    
    return result;
}

-(Matrix*)multiply:(Matrix *)leftMatrix with:(Matrix *)rightMatrix
{
    Matrix *result;
    if (leftMatrix.m != rightMatrix.n) {
        result = [[Matrix alloc] initWithSizeX:0 andY:0];
        result.error_msg = @"Wrong matrix size";
        return result;
    }
    
    result = [[Matrix alloc] initWithSizeX:leftMatrix.n andY:rightMatrix.m];
    for (NSInteger i = 0; i < leftMatrix.n; ++i) {
        for (NSInteger j = 0; j < rightMatrix.m; ++j) {
            for (NSInteger k = 0; k < leftMatrix.m; ++k) {
                Terminal *multiply = [[Terminal alloc] init];
                multiply = [multiply multiply:[leftMatrix.data objectInRow:i andColumn:k] with:[rightMatrix.data objectInRow:k andColumn:j]];
                Terminal *newTerminal = [[Terminal alloc] init];
                newTerminal = [newTerminal add:[result.data objectInRow:i andColumn:j] to:multiply];
                [result.data setObject:newTerminal inRow:i andColumn:j];
            }
        }
    }
    
    return result;
}

-(Matrix*)multiplyBy:(Matrix*)matrix
{
    Matrix *result;
    if (self.m != matrix.n) {
        result = [[Matrix alloc] initWithSizeX:0 andY:0];
        result.error_msg = @"Wrong matrix size";
        return result;
    }
    
    result = [[Matrix alloc] initWithSizeX:self.n andY:matrix.m];
    for (NSInteger i = 0; i < self.n; ++i) {
        for (NSInteger j = 0; j < matrix.m; ++j) {
            for (NSInteger k = 0; k < self.m; ++k) {
                Terminal *multiply = [[Terminal alloc] init];
                multiply = [multiply multiply:[self.data objectInRow:i andColumn:k] with:[matrix.data objectInRow:k andColumn:j]];
                Terminal *newTerminal = [[Terminal alloc] init];
                newTerminal = [newTerminal add:[result.data objectInRow:i andColumn:j] to:multiply];
                [result.data setObject:newTerminal inRow:i andColumn:j];
            }
        }
    }
    
    return result;
}

-(void)setNDimension:(NSInteger)n
{
    TwoDArray *oldData = self.data;
    self.data = [[TwoDArray alloc] initWithRows:n andColumns:self.m];
    for (NSInteger i = 0; i < n; ++i) {
        for (NSInteger j = 0; j < self.m; ++j) {
            if (i < self.n) {
                [self.data setObject:[oldData objectInRow:i andColumn:j] inRow:i andColumn:j];
            }
            else
            {
                [self.data setObject:[[Terminal alloc] initWithNumbers:0 and:0] inRow:i andColumn:j];
            }
        }
    }
    
    self.n = n;
}

-(void)setMDimension:(NSInteger)m
{
    TwoDArray *oldData = self.data;
    self.data = [[TwoDArray alloc] initWithRows:self.n andColumns:m];
    for (NSInteger i = 0; i < self.n; ++i) {
        for (NSInteger j = 0; j < m; ++j) {
            if (j < self.m) {
                [self.data setObject:[oldData objectInRow:i andColumn:j] inRow:i andColumn:j];
            }
            else
            {
                [self.data setObject:[[Terminal alloc] initWithNumbers:0 and:0] inRow:i andColumn:j];
            }
        }
    }
    self.m = m;
}

@end
