//
//  AboutView.m
//  RZCalc
//
//  Created by Hristo Uzunov on 2/3/14.
//  Copyright (c) 2014 Hristo Uzunov. All rights reserved.
//

#import "AboutView.h"

@interface AboutView ()

@property (nonatomic, strong) UITextView *aboutTextView;
@property (nonatomic, strong) UIButton *backButton;

@end

@implementation AboutView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        [self addSubview:self.aboutTextView];
        [self addSubview:self.backButton];
    }
    return self;
}

-(UITextView*)aboutTextView
{
    if (!_aboutTextView) {
        BOOL pad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
        CGRect frame = self.frame;
        frame.origin.x = 10;
        frame.origin.y = 10;
        frame.size.width -= 20;
        frame.size.height -= 50;
        UIFont *font;
        if (pad) {
            font = [UIFont fontWithName:@"Arial" size:35];
        }
        else
        {
            font = [UIFont fontWithName:@"Arial" size:15];
        }
        
        _aboutTextView = [[UITextView alloc] initWithFrame:frame];
        _aboutTextView.delegate = self;
        _aboutTextView.backgroundColor = [UIColor clearColor];
        [_aboutTextView setText:@"1. To close Instructions, press OK button\n2. Name of each function must be written before its argument. E.g. if you want \"sin(1)\" you first have to write \"sin(\", then \"1\" and finally \")\"\n3. Each function argument should be closed by brackets. E.g. mod(). First bracket is automatically written for you, but you have to write the closing one.\n4. Complex numbers have the following form (real,imaginary). E.g. (2,3) is complex number with real part = 2 and imaginary = 3. The two parts are separated by COMMA \",\" not by DOT \".\"\n5. Each Complex number should be closed in brackets.\n6. You can write nested expressions like sin((1,5) + 2).\n7. Answer is always in decimal and radians\n8. Unless you choose complex, you might get an error message white calculating complex numbers.\n9. When working with matrices, you need to provide number of rows and columns first, between 1 and 25.\n10. The matrix calculator always works with complex numbers. If you did not provide imaginary part, it assumes 0.\n11. If you did not put any number in the provided boxes, it assumes (0,0).\n12. The created matrix is shown to your left. If you select it, you can either edit it, or perform some of the single-matrix actions: transpose, inverse, etc.\n13. To create a new matrix, deselect the currently selected one, if any, and define size for the matrix in the Row: Column: fields.\n14. If you select two matrices, you can perform actions like adding and multiplying.\n15. Each time you perform an operation which creates a new matrix, the last one is added below the previous ones and so on.\n16. You can review and edit all the matrices in the list."];
        _aboutTextView.textColor = [UIColor whiteColor];
        _aboutTextView.font = font;
        _aboutTextView.editable = NO;
        _aboutTextView.textAlignment = NSTextAlignmentJustified;
    }
    
    return _aboutTextView;
}

-(UIButton*)backButton
{
    if (!_backButton) {
        CGRect frame = self.frame;
        frame.origin.x = frame.size.width / 2 - 30;
        frame.origin.y = frame.size.height - 35;
        frame.size.width = 60;
        frame.size.height = 30;
        _backButton = [[UIButton alloc] initWithFrame:frame];
        [_backButton setTitle:@"OK" forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundColor:[UIColor clearColor]];
    }
    
    return _backButton;
}

-(void)backButtonPressed
{
    [self.delegate back];
}

-(void)displayAbourFrame
{
    NSLog(@"frame: %@", NSStringFromCGRect(self.aboutTextView.frame));
}

-(void)addAboutView
{
    [self addSubview:self.aboutTextView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
