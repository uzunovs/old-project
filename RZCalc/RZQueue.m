//
//  RZQueue.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/24/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "RZQueue.h"

@implementation RZQueue

-(id)init
{
    self = [super init];
    if (self) {
        self.queueArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

-(void)pushObject:(id)object
{
    [self.queueArray addObject:object];
}

-(id)popObject
{
    id object = [self.queueArray firstObject];
    for (NSInteger i = 0; i < [self.queueArray count] - 1; ++i) {
        [self.queueArray replaceObjectAtIndex:i withObject:self.queueArray[i + 1]];
    }
    
    [self.queueArray removeLastObject];
    return object;
}

-(id)peekFirst
{
    return [self.queueArray firstObject];
}

-(id)peekLast
{
    return [self.queueArray lastObject];
}

-(id)peekAtIndex:(NSInteger)index
{
    return index < [self.queueArray count] ? self.queueArray[index] : nil;
}

-(NSInteger)lastObjectIndex
{
    return [self.queueArray indexOfObject:[self peekLast]];
}

-(NSUInteger)countOfQueueArray
{
    return [self.queueArray count];
}

-(void)eraseQueue
{
    [self.queueArray removeAllObjects];
}

@end
