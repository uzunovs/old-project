//
//  RZQueue.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/24/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RZQueue : NSObject

@property (nonatomic, strong) NSMutableArray *queueArray;

-(void)pushObject:(id)object;
-(id)popObject;
-(id)peekLast;
-(id)peekFirst;
-(id)peekAtIndex:(NSInteger)index;
-(NSInteger)lastObjectIndex;
-(NSUInteger)countOfQueueArray;
-(void)eraseQueue;
-(id)init;

@end
