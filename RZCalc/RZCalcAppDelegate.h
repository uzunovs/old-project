//
//  RZCalcAppDelegate.h
//  RZCalc
//
//  Created by Hristo Uzunov on 9/30/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RZCalcAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
