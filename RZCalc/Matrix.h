//
//  Matrix.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/9/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Terminal.h"
#import "TwoDArray.h"

@interface Matrix : NSObject

@property (nonatomic) NSInteger n;
@property (nonatomic) NSInteger m;
@property (nonatomic, strong) NSString *error_msg;
@property (nonatomic, strong) TwoDArray *data;

-(id)initWithSizeX:(NSInteger)n andY:(NSInteger)m;

-(Matrix*)add:(Matrix*)leftMatrix to:(Matrix*)rightMatrix;
-(Matrix*)addTo:(Matrix*)matrix;
-(Matrix*)subtract:(Matrix *)leftMatrix from:(Matrix *)rightMatrix;
-(Matrix*)subtractMatrix:(Matrix*)matrix;
-(Matrix*)multiply:(Matrix *)leftMatrix with:(Matrix *)rightMatrix;
-(Matrix*)multiplyBy:(Matrix*)matrix;
-(void)setNDimension:(NSInteger)n;
-(void)setMDimension:(NSInteger)m;
-(void)getElement;

@end
