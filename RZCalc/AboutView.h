//
//  AboutView.h
//  RZCalc
//
//  Created by Hristo Uzunov on 2/3/14.
//  Copyright (c) 2014 Hristo Uzunov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AboutViewDelegate <NSObject>

-(void)back;

@end

@interface AboutView : UIView <UITextViewDelegate>

@property (nonatomic, weak) id <AboutViewDelegate> delegate;

-(void)displayAbourFrame;
-(void)addAboutView;

@end
