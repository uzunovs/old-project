//
//  TwoDArray.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/9/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "TwoDArray.h"
#import "Terminal.h"

@implementation TwoDArray

-(id)initWithRows:(NSUInteger)rows andColumns:(NSUInteger)columns
{
    self = [super init];
    if (self) {
        self.rows = [[NSMutableArray alloc] initWithCapacity:rows];
        for (NSInteger i = 0; i < rows; ++i) {
            NSMutableArray *column = [NSMutableArray arrayWithCapacity:columns];
            for (NSInteger j = 0; j < columns; ++j) {
                [column setObject:[[Terminal alloc] initWithNumbers:0.0 and:0.0] atIndexedSubscript:j];
            }
            [self.rows addObject:column];
        }
    }
    
    return self;
}

+(id)sectionArrayWithRows:(NSUInteger)rows andColumns:(NSUInteger)columns
{
    return [[self alloc] initWithRows:rows andColumns:columns];
}

-(id)objectInRow:(NSUInteger)row andColumn:(NSUInteger)column
{
    return [[self.rows objectAtIndex:row] objectAtIndex:column];
}

-(void)setObject:(id)obj inRow:(NSUInteger)row andColumn:(NSUInteger)column
{
    [[self.rows objectAtIndex:row] replaceObjectAtIndex:column withObject:obj];
}

@end
