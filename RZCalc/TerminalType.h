//
//  TerminalType.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/9/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>

static const int NUMBER = 1;
static const int PLUS = 2;
static const int MINUS = 3;
static const int MULTIPLY = 4;
static const int DIVIDE = 5;
static const int POWER = 6;
static const int LEFT_BRACKET = 7;
static const int RIGHT_BRACKET = 8;

@interface TerminalType : NSObject

@end
