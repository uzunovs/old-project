//
//  MatrixViewController.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/14/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "MatrixViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RZCustomKeyboard.h"
#define ROW_COLUMN_LIMIT 26
#define ROW_TAG 1000
#define COLUMN_TAG 1001
#define TEXT_FIELD_WIDTH 80
#define TEXT_FIELD_HEIGHT 30
#define TEXT_FIELD_INDENT 10
#define MATRIX_BUTTON_WIDTH 64
#define MATRIX_BUTTON_HEIGHT 30
#define MATRIX_BUTTON_INDENT 5
#define TEXT_FIELD_WIDTH_PAD 120
#define TEXT_FIELD_HEIGHT_PAD 50
#define TEXT_FIELD_INDENT_PAD 20

@interface MatrixViewController ()
@property (strong, nonatomic) IBOutlet UITextField *rowsTextField;
@property (strong, nonatomic) IBOutlet UITextField *columnsTextField;
@property (strong, nonatomic) IBOutlet UIScrollView *matrixInputScrollView; //where the user gets the interface fot matrix input
@property (nonatomic) NSInteger rows;
@property (nonatomic) NSInteger columns;
@property (strong, nonatomic) IBOutlet UIButton *doneMatrixButton; //confirm user finished matrix editing
@property (strong, nonatomic) IBOutlet UIScrollView *buttonsScrollView; //to hold the matrix buttons
@property (nonatomic) NSInteger buttonCount; //how many mstrix buttons onscreen
@property (nonatomic) NSInteger followButtons; //how many selected matrix buttons
@property (nonatomic, strong) NSString *buttonLabel; //previously selected button label
@property (strong, nonatomic) IBOutlet UIButton *editButton; //activated when user selects a matrix button
@property (strong, nonatomic) RZQueue *queue;
@property (strong, nonatomic) IBOutlet UIButton *transposeButton;
@property (strong, nonatomic) IBOutlet UIButton *inverseButton;
@property (strong, nonatomic) IBOutlet UIButton *determinantButton;
@property (strong, nonatomic) IBOutlet UIButton *gauseEliminationButton;
@property (strong, nonatomic) IBOutlet UIButton *zeroMatrix;
@property (strong, nonatomic) IBOutlet UIButton *identityMatrixButton;
@property (strong, nonatomic) IBOutlet UIButton *rankButton;
@property (strong, nonatomic) IBOutlet UIButton *addMatrixButton;
@property (strong, nonatomic) IBOutlet UIButton *subtractMatrixButton;
@property (strong, nonatomic) IBOutlet UIButton *multiplyMatrixButton;
@property (strong, nonatomic) IBOutlet UIButton *divideMatrixButton;
@property (nonatomic) CGFloat viewWidth;
@property (nonatomic) BOOL isEditing;
@property (nonatomic) BOOL isInitialEditing;
@property (nonatomic, getter = isRows) BOOL textFieldToEdit;
@property (nonatomic, strong) RZCustomKeyboard *keyboardView;

- (IBAction)createMatrix;
- (IBAction)transposeMatrix;
- (IBAction)inverseMatrix;
- (IBAction)findMatrixDeterminant;
- (IBAction)gauseElimination;
- (IBAction)createZeroMatrix;
- (IBAction)createIdentityMatrix;
- (IBAction)findMatrixRank;
- (IBAction)addMatrices;
- (IBAction)subtractMatrices;
- (IBAction)multiplyMatrices;
- (IBAction)divideMatrices;
- (IBAction)editMatrix;
- (IBAction)backButtonAction;

@end

@implementation MatrixViewController

-(RZQueue*)queue;
{
    if (!_queue) {
        _queue = [[RZQueue alloc] init];
    }
    
    return _queue;
}

-(NSInteger)buttonCount
{
    if (!_buttonCount) {
        _buttonCount = 0;
    }
    
    return _buttonCount;
}

-(void)setRows:(NSInteger)rows
{
    _rows = rows;
    if (!self.isEditing) { //every time the rows get set, the input matrix interface gets redrawn
        [self setScrollViewSize];
    }
    else if (self.isEditing && !self.isInitialEditing)
    {
        [self matrixScrollViewEditMode];
    }
    
    self.doneMatrixButton.enabled = YES;
}

-(void)setColumns:(NSInteger)columns
{
    _columns = columns;
    if (self.rows != 0) {
        if (!self.isEditing) {
            [self setScrollViewSize]; //every time the columns get set, if the rows are not 0, the matrix input interface gets redrawn
        }
        else
        {
            [self matrixScrollViewEditMode];
        }
        
        self.doneMatrixButton.enabled = YES;
    }
}

-(UIView*)keyboardView
{
    if (!_keyboardView) {
        _keyboardView = [[RZCustomKeyboard alloc] init];
        _keyboardView.keyboardDelegate = self;
    }
    
    return _keyboardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)input:(NSString *)string
{
    if (self.isRows) {
        self.rowsTextField.text = [NSString stringWithFormat:@"%@%@", self.rowsTextField.text, string];
    }
    else
    {
        self.columnsTextField.text = [NSString stringWithFormat:@"%@%@", self.columnsTextField.text, string];
    }
}

-(void)deleteInput
{
    NSString *temString;
    if (self.isRows) {
        temString = self.rowsTextField.text;
    }
    else
    {
        temString = self.columnsTextField.text;
    }
    
    if (![temString isEqualToString:@""]) {
        temString = [temString substringToIndex:[temString length] - 1];
    }
    
    if (self.isRows) {
        self.rowsTextField.text = temString;
    }
    else
    {
        self.columnsTextField.text = temString;
    }
}

-(NSInteger)followButtons
{
    if (!_followButtons) {
        _followButtons = 0;
    }
    
    return _followButtons;
}

-(UIScrollView*)matrixInputScrollView
{
    if (!_matrixInputScrollView) {
        _matrixInputScrollView = [[UIScrollView alloc] init];
    }
    
    return _matrixInputScrollView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rowsTextField.keyboardType = UIKeyboardTypeNumberPad;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.rowsTextField.inputView = self.keyboardView;
        self.columnsTextField.inputView = self.keyboardView;
    }
    
    UIToolbar *keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    keyboardToolbar.barStyle = UIBarStyleDefault;
    keyboardToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneEditing)]];
    [keyboardToolbar sizeToFit];
    self.rowsTextField.tag = ROW_TAG;
    self.rowsTextField.inputAccessoryView = keyboardToolbar;
    self.columnsTextField.tag = COLUMN_TAG;
    self.columnsTextField.inputAccessoryView = keyboardToolbar;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTap.numberOfTouchesRequired = 1;
    singleTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTap];
    self.doneMatrixButton.enabled = NO;
    self.editButton.hidden = YES;
    [self singleMatrixButtons];
    [self dualMatrixButtons];
    self.isEditing = NO;
    self.isInitialEditing = NO;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGRect frame = self.rowsTextField.frame;
        frame.size.height = 50;
        frame.origin.y -= 10;
        self.rowsTextField.frame = frame;
        self.rowsTextField.font = [UIFont systemFontOfSize:25];
        self.columnsTextField.font = [UIFont systemFontOfSize:25];
        frame = self.columnsTextField.frame;
        frame.origin.y -= 10;
        frame.size.height = 50;
        self.columnsTextField.frame = frame;
    }
}

-(void)doneEditing
{
    [self.rowsTextField resignFirstResponder];
    [self.columnsTextField resignFirstResponder];
}

-(void)singleTap:(UIGestureRecognizer*)singleTap
{
    [self.rowsTextField resignFirstResponder];
    [self.columnsTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == ROW_TAG) {
        self.textFieldToEdit = YES;
    }
    else
    {
        self.textFieldToEdit = NO;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == ROW_TAG) {
        if ([textField.text integerValue] > 0 && [textField.text integerValue] < ROW_COLUMN_LIMIT) {
            self.rows = [textField.text integerValue];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Out of bounds" message:@"Rows must be between 1 and 25" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [textField resignFirstResponder];
            self.doneMatrixButton.enabled = NO;
            return;
        }
        
    }
    else if (textField.tag == COLUMN_TAG)
    {
        if ([self.rowsTextField.text integerValue] > 0 && [self.rowsTextField.text integerValue] < ROW_COLUMN_LIMIT) {
            if ([textField.text integerValue] > 0 && [textField.text integerValue] < ROW_COLUMN_LIMIT) {
                self.columns = [textField.text integerValue];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Out of bounds" message:@"Columns must be between 1 and 25" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [textField resignFirstResponder];
                return;
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No rows set" message:@"Rows cannot be less than 1 and more than 25" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [textField resignFirstResponder];
            return;
        }
    }
    
    [textField resignFirstResponder];
}

#pragma mark matrix I/O functions

-(void)setScrollViewSize
{
    [self clearMatrixScroller];
    
    BOOL pad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
    
    CGSize scrollVieSize;
    if (!self.columns) {
        if (pad) {
            scrollVieSize = CGSizeMake(self.rows * TEXT_FIELD_WIDTH_PAD + (self.rows - 1) * TEXT_FIELD_INDENT_PAD, TEXT_FIELD_HEIGHT_PAD);
        }
        else
        {
            scrollVieSize = CGSizeMake(self.rows * TEXT_FIELD_WIDTH + (self.rows - 1) * TEXT_FIELD_INDENT, TEXT_FIELD_HEIGHT);
        }
        
    }
    else
    {
        scrollVieSize = CGSizeMake(self.rows * TEXT_FIELD_WIDTH + (self.rows - 1) * TEXT_FIELD_INDENT, self.columns * TEXT_FIELD_HEIGHT + (self.columns - 1) * TEXT_FIELD_INDENT);
    }
    
    self.matrixInputScrollView.contentSize = scrollVieSize;
    
    if (!self.columns) {
        for (NSInteger i = 0; i < self.rows; ++i) {
            MatrixTextView *textField;
            if (pad) {
                textField = [[MatrixTextView alloc] initWithFrame:CGRectMake(i * (TEXT_FIELD_WIDTH_PAD + TEXT_FIELD_INDENT_PAD), 0, TEXT_FIELD_WIDTH_PAD, TEXT_FIELD_HEIGHT_PAD)];
            }
            else
            {
                textField = [[MatrixTextView alloc] initWithFrame:CGRectMake(i * (TEXT_FIELD_WIDTH + TEXT_FIELD_INDENT), 0, TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT)];
            }
            
            textField.i = i;
            textField.j = 0;
            [self.matrixInputScrollView addSubview:textField];
        }
    }
    else
    {
        for (NSInteger i = 0; i < self.rows; ++i) {
            for (NSInteger j = 0; j < self.columns; ++j) {
                MatrixTextView *textField;
                if (pad) {
                    textField = [[MatrixTextView alloc] initWithFrame:CGRectMake(i * (TEXT_FIELD_WIDTH_PAD + TEXT_FIELD_INDENT_PAD), j * (TEXT_FIELD_HEIGHT_PAD + TEXT_FIELD_INDENT_PAD), TEXT_FIELD_WIDTH_PAD, TEXT_FIELD_HEIGHT_PAD)];
                }
                else
                {
                    textField = [[MatrixTextView alloc] initWithFrame:CGRectMake(i * (TEXT_FIELD_WIDTH + TEXT_FIELD_INDENT), j * (TEXT_FIELD_HEIGHT + TEXT_FIELD_INDENT), TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT)];
                }
                
                textField.i = i;
                textField.j = j;
                [self.matrixInputScrollView addSubview:textField];
            }
        }
    }
}

- (IBAction)createMatrix {
    NSArray *fields = [self.matrixInputScrollView subviews];
    NSInteger j = 1;
    if (self.columns) {
        j = self.columns;
    }
    
    Matrix *matrix = [[Matrix alloc] initWithSizeX:self.rows andY:j];
    
    for (MatrixTextView *view in fields) {
        if ([view isKindOfClass:[MatrixTextView class]]) {
            Terminal *terminal = [self createTerminal:view.text];
            if (terminal.error_msg) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@ at [%ld,%ld]", terminal.error_msg, (long)view.i + 1, (long)view.j + 1] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            else
            {
                [matrix.data setObject:terminal inRow:view.i andColumn:view.j];
            }
        }
    }
    
    if (self.isEditing) {
        for (MatrixButton *button in [self.buttonsScrollView subviews]) {
            if ([button isKindOfClass:[MatrixButton class]]) {
                if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                    button.matrix = matrix;
                    self.isEditing = NO;
                    [self selectedButton:button];
                }
            }
        }
    }
    else
    {
        [self placeMatrixButton:matrix];
    }
    
    [self clearMatrixScroller];
    
    self.rows = 0;
    self.columns = 0;
    self.rowsTextField.text = @"";
    self.columnsTextField.text = @"";
    self.doneMatrixButton.enabled = NO;
    self.matrixInputScrollView.contentSize = CGSizeZero;
}

-(Terminal*)createTerminal:(NSString*)textFieldString
{
    Terminal *terminal = [[Terminal alloc] init];
    
    NSArray *numbers = [textFieldString componentsSeparatedByString:@","];
    if ([numbers count] == 1) {
        if ([[numbers objectAtIndex:0] isEqualToString:@""]) {
            terminal.realPart = [NSNumber numberWithDouble:0.0];
        }
        else
        {
            terminal.realPart = [NSNumber numberWithDouble:[[numbers objectAtIndex:0] doubleValue]];
        }
        
        terminal.imaginaryPart = [NSNumber numberWithDouble:0.0];
    }
    else if ([numbers count] == 2)
    {
        if ([[numbers objectAtIndex:0] isEqualToString:@""]) {
            terminal.realPart = [NSNumber numberWithDouble:0.0];
            if ([[numbers objectAtIndex:1] isEqualToString:@""]) {
                terminal.imaginaryPart = [NSNumber numberWithDouble:0.0];
            }
            else
            {
                terminal.imaginaryPart = [NSNumber numberWithDouble:[[numbers objectAtIndex:1] doubleValue]];
            }
        }
        else
        {
            terminal.realPart = [NSNumber numberWithDouble:[[numbers objectAtIndex:0] doubleValue]];
            terminal.imaginaryPart = [NSNumber numberWithDouble:[[numbers objectAtIndex:1] doubleValue]];
        }
    }
    else
    {
        terminal.realPart = [NSNumber numberWithDouble:0.0];
        terminal.imaginaryPart = [NSNumber numberWithDouble:0.0];
        terminal.error_msg = @"Wrong input";
    }
    
    return terminal;
}

-(void)clearMatrixScroller
{
    NSArray *subviews = [self.matrixInputScrollView subviews];
    for (MatrixTextView *subview in subviews) {
        if ([subview respondsToSelector:@selector(doneEditing)]) {
            [subview removeFromSuperview];
        }
    }
    
    for (UILabel *label in [self.matrixInputScrollView subviews]) {
        if ([label isKindOfClass:[UILabel class]]) {
            [label removeFromSuperview];
        }
    }
}

#pragma mark matrix buttons I/O functions

-(void)placeMatrixButton:(Matrix*)matrix
{
    self.buttonsScrollView.contentSize = CGSizeZero;
    CGSize buttonScrollSize = CGSizeMake(self.buttonsScrollView.frame.size.width, (MATRIX_BUTTON_HEIGHT + TEXT_FIELD_INDENT) * self.buttonCount);
    
    self.buttonsScrollView.contentSize = buttonScrollSize;
    
    MatrixButton *button;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        button = [[MatrixButton alloc] initWithFrame:CGRectMake(MATRIX_BUTTON_INDENT, MATRIX_BUTTON_INDENT + (MATRIX_BUTTON_INDENT + MATRIX_BUTTON_HEIGHT) * self.buttonCount, MATRIX_BUTTON_WIDTH, MATRIX_BUTTON_HEIGHT) andIndex:self.buttonCount];
    }
    else
    {
        button = [[MatrixButton alloc] initWithFrame:CGRectMake(20, (60 + 8) * self.buttonCount, 138, 60) andIndex:self.buttonCount];
    }
    
    button.matrix = matrix;
    
    [button addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = self.buttonCount;
    
    [self.buttonsScrollView addSubview:button];
    ++self.buttonCount;
}

-(void)selectedButton:(id)sender
{
    MatrixButton *receiver = (MatrixButton*)sender;
    receiver.selected = !receiver.selected;
    if (receiver.selected && self.followButtons == 0) {
        [receiver setBackgroundColor:[UIColor colorWithRed:1 green:208/255.0f blue:0 alpha:1]];
        self.editButton.hidden = NO;
        [self singleMatrixButtons];
        [self.queue pushObject:receiver.titleLabel.text];
        ++self.followButtons;
        [self showMatrixButtonContent];
    }
    else if (receiver.selected && self.followButtons == 1)
    {
        [receiver setBackgroundColor:[UIColor colorWithRed:1 green:208/255.0f blue:0 alpha:1]];
        self.editButton.hidden = YES;
        [self singleMatrixButtons];
        [self dualMatrixButtons];
        [self.queue pushObject:receiver.titleLabel.text];
        [self clearMatrixScroller];
        ++self.followButtons;
    }
    else if (receiver.selected && self.followButtons == 2)
    {
        [receiver setBackgroundColor:[UIColor colorWithRed:1 green:208/255.0f blue:0 alpha:1]];
        [self manipulateButtonsScroller:[self.queue popObject]];
        [self.queue pushObject:receiver.titleLabel.text];
        self.editButton.hidden = YES;
    }
    else if (!receiver.selected && self.followButtons == 2)
    {
        [receiver setBackgroundColor:[UIColor clearColor]];
        self.editButton.hidden = NO;
        [self singleMatrixButtons];
        [self dualMatrixButtons];
        --self.followButtons;
        [self.queue popObject];
        [self showMatrixButtonContent];
    }
    else if (!receiver.selected && self.followButtons == 1)
    {
        [receiver setBackgroundColor:[UIColor clearColor]];
        self.editButton.hidden = YES;
        [self singleMatrixButtons];
        --self.followButtons;
        [self.queue popObject];
        [self clearMatrixScroller];
    }
}

-(void)manipulateButtonsScroller:(NSString*)receiverLabel
{
    NSArray *buttonsArray = [self.buttonsScrollView subviews];
    for (MatrixButton *matrixButton in buttonsArray) {
        if ([matrixButton isKindOfClass:[MatrixButton class]]) {
            if ([matrixButton.titleLabel.text isEqualToString:receiverLabel]) {
                matrixButton.selected = NO;
                [matrixButton setBackgroundColor:[UIColor clearColor]];
                return;
            }
        }
    }
}

#pragma mark UI functions


-(void)singleMatrixButtons
{
    self.transposeButton.enabled = !self.transposeButton.enabled;
    self.inverseButton.enabled = !self.inverseButton.enabled;
    self.determinantButton.enabled = !self.determinantButton.enabled;
    self.rankButton.enabled = !self.rankButton.enabled;
    self.gauseEliminationButton.enabled = !self.gauseEliminationButton.enabled;
}

-(void)dualMatrixButtons
{
    self.addMatrixButton.enabled = !self.addMatrixButton.enabled;
    self.subtractMatrixButton.enabled = !self.subtractMatrixButton.enabled;
    self.multiplyMatrixButton.enabled = !self.multiplyMatrixButton.enabled;
    self.divideMatrixButton.enabled = !self.divideMatrixButton.enabled;
}

-(void)alertShow:(NSString*)alert
{
    UIAlertView *newAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:alert delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [newAlert show];
}

#pragma mark math functions

- (IBAction)transposeMatrix {
    Matrix *matrix;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                Core *core = [[Core alloc] init];
                matrix = [core matrixTranspose:button.matrix];
                if (matrix.error_msg) {
                    [self alertShow:matrix.error_msg];
                    return;
                }
            }
        }
    }
    
    [self placeMatrixButton:matrix];
}

- (IBAction)inverseMatrix {
    Matrix *matrix;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                Core *core = [[Core alloc] init];
                matrix = [core matrixInverse:button.matrix];
                if (matrix.error_msg) {
                    [self alertShow:matrix.error_msg];
                    return;
                }
            }
        }
    }
    
    [self placeMatrixButton:matrix];
}

- (IBAction)findMatrixDeterminant {
    Terminal *terminal;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                Core *core = [[Core alloc] init];
                terminal = [core matrixDeterminant:button.matrix];
                if (terminal.error_msg) {
                    [self alertShow:terminal.error_msg];
                    return;
                }
            }
        }
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Determinant" message:[NSString stringWithFormat:@"The deterninant for the selected matrix is: %@, %@", terminal.realPart, terminal.imaginaryPart] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)gauseElimination {
    Matrix *matrix;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                Core *core = [[Core alloc] init];
                matrix = [core gauseElimination:button.matrix];
                if (matrix.error_msg) {
                    [self alertShow:matrix.error_msg];
                    return;
                }
            }
        }
    }
    
    [self placeMatrixButton:matrix];
}

- (IBAction)createZeroMatrix {
    for (MatrixTextView *textView in [self.matrixInputScrollView subviews]) {
        if ([textView isKindOfClass:[MatrixTextView class]]) {
            if ([textView.text isEqualToString:@""]) {
                textView.text = @"0";
            }
        }
    }
}

- (IBAction)createIdentityMatrix {
    for (MatrixTextView *textView in [self.matrixInputScrollView subviews]) {
        if ([textView isKindOfClass:[MatrixTextView class]]) {
            if (textView.i == textView.j) {
                textView.text = @"1,0";
            }
            else
            {
                textView.text = @"0,0";
            }
        }
    }
}

- (IBAction)findMatrixRank {
    NSInteger rank = 0;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                Core *core = [[Core alloc] init];
                rank = [core matrixRank:button.matrix];
            }
        }
    }
    
    NSString *messageString = [NSString stringWithFormat:@"The rank of the seelcted matrix is: %ld", (long)rank];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rank" message:messageString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)addMatrices {
    Matrix *result, *firstMatrix, *secondMatrix;
    for (NSInteger i = 0; i < [self.queue.queueArray count]; ++i) {
        for (MatrixButton *button in [self.buttonsScrollView subviews]) {
            if ([button isKindOfClass:[MatrixButton class]]) {
                if ([button.titleLabel.text isEqualToString:self.queue.queueArray[i]]) {
                    if (i == 0) {
                        firstMatrix = button.matrix;
                    }
                    else
                    {
                        secondMatrix = button.matrix;
                    }
                }
            }
        }
    }
    
    result = [firstMatrix addTo:secondMatrix];
    if (result.error_msg) {
        [self alertShow:result.error_msg];
        return;
    }
    
    [self placeMatrixButton:result];
}

- (IBAction)subtractMatrices {
    Matrix *result, *firstMatrix, *secondMatrix;
    for (NSInteger i = 0;  i < [self.queue.queueArray count]; ++i) {
        for (MatrixButton *button in [self.buttonsScrollView subviews]) {
            if ([button isKindOfClass:[MatrixButton class]]) {
                if ([button.titleLabel.text isEqualToString:self.queue.queueArray[i]]) {
                    if (i == 0) {
                        firstMatrix = button.matrix;
                    }
                    else
                    {
                        secondMatrix = button.matrix;
                    }
                }
            }
        }
    }
    
    result = [firstMatrix subtractMatrix:secondMatrix];
    if (result.error_msg) {
        [self alertShow:result.error_msg];
        return;
    }
    
    [self placeMatrixButton:result];
}

- (IBAction)multiplyMatrices {
    Matrix *result, *firstMatrix, *secondMatrix;
    for (NSInteger i = 0; i < [self.queue.queueArray count]; ++i) {
        for (MatrixButton *button in [self.buttonsScrollView subviews]) {
            if ([button isKindOfClass:[MatrixButton class]]) {
                if ([button.titleLabel.text isEqualToString:self.queue.queueArray[i]]) {
                    if (i == 0) {
                        firstMatrix = button.matrix;
                    }
                    else
                    {
                        secondMatrix = button.matrix;
                    }
                }
            }
        }
    }
    
    result = [firstMatrix multiplyBy:secondMatrix];
    if (result.error_msg) {
        [self alertShow:result.error_msg];
        return;
    }
    
    [self placeMatrixButton:result];
}

- (IBAction)divideMatrices {
    Matrix *result, *firstMatrix, *secondMatrix;
    for (NSInteger i = 0; i < [self.queue.queueArray count]; ++i) {
        for (MatrixButton *button in [self.buttonsScrollView subviews]) {
            if ([button isKindOfClass:[MatrixButton class]]) {
                if ([button.titleLabel.text isEqualToString:self.queue.queueArray[i]]) {
                    if (i == 0) {
                        firstMatrix = button.matrix;
                    }
                    else
                    {
                        secondMatrix = button.matrix;
                    }
                }
            }
        }
    }
    
    Core *core = [[Core alloc] init];
    secondMatrix = [core matrixInverse:secondMatrix];
    if (secondMatrix.error_msg) {
        [self alertShow:secondMatrix.error_msg];
        return;
    }
    result = [firstMatrix multiplyBy:secondMatrix];
    if (result.error_msg) {
        [self alertShow:result.error_msg];
        return;
    }
    
    [self placeMatrixButton:result];
}

#define LABEL_WIDTH 60
#define LABEL_HEIGHT 30
#define LABEL_WIDTH_PAD 100
#define LABEL_HEIGHT_PAD 50


- (IBAction)editMatrix {
    self.isEditing = YES;
    self.isInitialEditing = YES;
    MatrixButton *matrixButton;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                matrixButton = button;
                self.editButton.hidden = YES;
            }
        }
    }
    
    self.rows = matrixButton.matrix.n; //редовете винаги трябава да се инициализират преди колоните!
    self.columns = matrixButton.matrix.m;
    self.rowsTextField.text = [NSString stringWithFormat:@"%ld", (long)matrixButton.matrix.n];
    self.columnsTextField.text = [NSString stringWithFormat:@"%ld", (long)matrixButton.matrix.m];
}

- (IBAction)backButtonAction {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)matrixScrollViewEditMode
{
    BOOL pad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
    [self clearMatrixScroller];
    Matrix *matrix;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                matrix = button.matrix;
            }
        }
    }
    
    Matrix *tempMatrix = [[Matrix alloc] initWithSizeX:self.rows andY:self.columns];
    
    for (NSInteger i = 0; i < self.rows; ++i) {
        for (NSInteger j = 0; j < self.columns; ++j) {
            if (i >= matrix.n || j >= matrix.m) {
                continue;
            }
            else
            {
                [tempMatrix.data setObject:[matrix.data objectInRow:i andColumn:j] inRow:i andColumn:j];
            }
        }
    }
    
    for (NSInteger i = 0; i < self.rows; ++i) {
        for (NSInteger j = 0; j < self.columns; ++j) {
            MatrixTextView *textView;
            if (pad) {
                textView = [[MatrixTextView alloc] initWithFrame:CGRectMake((self.viewWidth + TEXT_FIELD_INDENT_PAD) * i, (TEXT_FIELD_HEIGHT_PAD + TEXT_FIELD_HEIGHT_PAD) * j, self.viewWidth, TEXT_FIELD_HEIGHT_PAD)];
            }
            else
            {
                textView = [[MatrixTextView alloc] initWithFrame:CGRectMake((self.viewWidth + TEXT_FIELD_INDENT) * i, (TEXT_FIELD_HEIGHT + TEXT_FIELD_HEIGHT) * j, self.viewWidth, TEXT_FIELD_HEIGHT)];
            }
            
            textView.text = [NSString stringWithFormat:@"%@,%@", ((Terminal*)[tempMatrix.data objectInRow:i andColumn:j]).realPart, ((Terminal*)[tempMatrix.data objectInRow:i andColumn:j]).imaginaryPart];
            textView.i = i;
            textView.j = j;
            [self.matrixInputScrollView addSubview:textView];
        }
    }
    
    self.isInitialEditing = NO;
}

-(void)showMatrixButtonContent
{
    BOOL pad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
    self.viewWidth = 0.0;
    self.matrixInputScrollView.contentSize = CGSizeZero;
    NSInteger labelRows = 0;
    NSInteger labelColumns = 0;
    UIFont *font;
    if (!pad) {
        font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    }
    else
    {
        font = [UIFont systemFontOfSize:25];
    }

    Matrix *matrix;
    for (MatrixButton *button in [self.buttonsScrollView subviews]) {
        if ([button isKindOfClass:[MatrixButton class]]) {
            if ([button.titleLabel.text isEqualToString:[self.queue peekLast]]) {
                matrix = button.matrix;
                labelRows = button.matrix.n;
                labelColumns = button.matrix.m;
            }
        }
    }
    
    for (NSInteger i = 0; i < labelRows; ++i) {
        for (NSInteger j = 0; j < labelColumns; ++j) {
            NSString *labelString = [NSString stringWithFormat:@"%@,%@", ((Terminal*)[matrix.data objectInRow:i andColumn:j]).realPart, ((Terminal*)[matrix.data objectInRow:i andColumn:j]).imaginaryPart];
            CGSize mySize = /*[labelString sizeWithFont:font];*/ [labelString sizeWithAttributes:[NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName]];
            if (mySize.width > self.viewWidth) {
                self.viewWidth = ceilf(mySize.width);
            }
        }
    }
    
    self.viewWidth += 20;
    CGSize editSize;
    if (pad) {
        editSize = CGSizeMake((self.viewWidth + TEXT_FIELD_INDENT_PAD) * labelRows, (LABEL_HEIGHT_PAD + TEXT_FIELD_INDENT_PAD) * labelColumns);
    }
    else
    {
        editSize = CGSizeMake((self.viewWidth + TEXT_FIELD_INDENT) * labelRows, (LABEL_HEIGHT + TEXT_FIELD_INDENT) * labelColumns);
    }
    self.matrixInputScrollView.contentSize = editSize;
    
    for (NSInteger i = 0; i < labelRows; ++i) {
        for (NSInteger j = 0; j < labelColumns; ++j) {
            UILabel *label;
            if (pad) {
                label = [[UILabel alloc] initWithFrame:CGRectMake((self.viewWidth + TEXT_FIELD_INDENT_PAD) * i, (LABEL_HEIGHT_PAD + TEXT_FIELD_INDENT_PAD) * j, self.viewWidth, LABEL_HEIGHT_PAD)];
            }
            else
            {
                label = [[UILabel alloc] initWithFrame:CGRectMake((self.viewWidth + TEXT_FIELD_INDENT) * i, (LABEL_HEIGHT + TEXT_FIELD_INDENT) * j, self.viewWidth, LABEL_HEIGHT)];
            }
            
            [label setTextAlignment:NSTextAlignmentCenter];
            label.text = [NSString stringWithFormat:@"%@,%@", ((Terminal*)[matrix.data objectInRow:i andColumn:j]).realPart, ((Terminal*)[matrix.data objectInRow:i andColumn:j]).imaginaryPart];
            label.font = font;
            label.textColor = [UIColor whiteColor];
            [self.matrixInputScrollView addSubview:label];
        }
    }
}
@end