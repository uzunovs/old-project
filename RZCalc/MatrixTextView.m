//
//  MatrixTextView.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/16/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "MatrixTextView.h"

@interface MatrixTextView ()

@property (nonatomic, strong) RZCustomKeyboard *keyboard;

@end

@implementation MatrixTextView

-(RZCustomKeyboard*)keyboard
{
    if (!_keyboard) {
        _keyboard = [[RZCustomKeyboard alloc] init];
        _keyboard.keyboardDelegate = self;
    }
    
    return _keyboard;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIToolbar *keyboardToolbar;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            self.keyboardType = UIKeyboardTypeNumberPad;
            keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        }
        else
        {
            self.inputView = self.keyboard;
            self.font = [UIFont systemFontOfSize:25];
            keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 768, 50)];
        }
        
        self.backgroundColor = [UIColor colorWithRed:0 green:(CGFloat)119/255 blue:(CGFloat)206/255 alpha:1];
        self.borderStyle = UITextBorderStyleLine;
        self.textColor = [UIColor whiteColor];
        [self setTintColor:[UIColor whiteColor]];
        self.textAlignment = NSTextAlignmentCenter;
        
        keyboardToolbar.barStyle = UIBarStyleDefault;
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneEditing)];
        
        keyboardToolbar.items = @[[[UIBarButtonItem alloc] initWithTitle:@"  ,  " style:UIBarButtonSystemItemAction target:self action:@selector(addComma)], [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil], [[UIBarButtonItem alloc] initWithTitle:@"  .  " style:UIBarButtonSystemItemAction target:self action:@selector(addDecimalPoint)], [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton];
        [keyboardToolbar sizeToFit];
        self.inputAccessoryView = keyboardToolbar;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)doneEditing
{
    [self resignFirstResponder];
}

-(void)addComma
{
    self.text = [NSString stringWithFormat:@"%@,", self.text];
}

-(void)addDecimalPoint
{
    self.text = [NSString stringWithFormat:@"%@.", self.text];
}

-(void)input:(NSString *)string
{
    self.text = [NSString stringWithFormat:@"%@%@", self.text, string];
}

-(void)deleteInput
{
    if (![self.text isEqualToString:@""]) {
        self.text = [self.text substringToIndex:[self.text length] - 1];
    }
}

@end
