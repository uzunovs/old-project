//
//  RZStack.h
//  RZCalc
//
//  Created by Hristo Uzunov on 11/5/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RZStack : NSObject

@property (nonatomic, strong) NSMutableArray *stackArray;

-(void)pushObject:(id)object;
-(id)popObject;
-(id)peekLast;
-(id)peekFirst;
-(id)peekAtIndex:(NSInteger)index;
-(NSUInteger)lastObjectIndex;
-(NSInteger)countOfStackArray;
-(void)eraseStack;
-(id)init;

@end
