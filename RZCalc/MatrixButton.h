//
//  MatrixButton.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/22/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Matrix.h"

@interface MatrixButton : UIButton

@property (nonatomic, strong) Matrix *matrix;

- (id)initWithFrame:(CGRect)frame andIndex:(NSInteger)number;

@end
