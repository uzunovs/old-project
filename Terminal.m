//
//  Terminal.m
//  RZCalc
//
//  Created by Hristo Uzunov on 10/9/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import "Terminal.h"


@implementation Terminal

-(NSString*)description
{
    return [NSString stringWithFormat:@"\nTerminal real part: %@\nimaginary part: %@\ntype: %ld\npriority: %ld\n", self.realPart, self.imaginaryPart, (long)self.type, (long)self.priority];
}

+(Terminal*)terminalWithNumbers:(double)n and:(double)m
{
    Terminal *newTerminal = [[Terminal alloc] initWithNumbers:n and:m];
    return newTerminal;
}

+(Terminal*)newTerminalWithTerminal:(Terminal *)terminal
{
    Terminal *newTerminal = terminal;
    
    return newTerminal;
}

-(id)initWithNumbers:(double)n and:(double)m
{
    self = [super init];
    if (self) {
        self.realPart = [NSNumber numberWithDouble:n];
        self.imaginaryPart = [NSNumber numberWithDouble:m];
        self.type = NUMBER;
    }
    
    return self;
}

-(id)initWithString:(NSString *)value
{
    self = [super init];
    if (self) {
        self.realPart = [NSNumber numberWithDouble:[value doubleValue]];
        self.imaginaryPart = [NSNumber numberWithDouble:0.0];
        self.type = NUMBER;
        self.priority = 0;
    }
    
    return self;
}

-(id)initWithSymbolString:(NSString*)value
{
    self = [super init];
    if (self) {
        self.realPart = [NSNumber numberWithDouble:0.0];
        self.imaginaryPart = [NSNumber numberWithDouble:0.0];
        if ([value isEqualToString:@"+"]) {
            self.type = PLUS;
            self.priority = 2;
            }
        else if ([value isEqualToString:@"-"])
        {
            self.type = MINUS;
            self.priority = 2;
        }
        else if ([value isEqualToString:@"×"])
        {
            self.type = MULTIPLY;
            self.priority = 3;
        }
        else if ([value isEqualToString:@"÷"])
        {
            self.type = DIVIDE;
            self.priority = 3;
        }
        else if ([value isEqualToString:@"^"])
        {
            self.type = POWER;
            self.priority = 5;
        }
        else if ([value isEqualToString:@"("])
        {
            self.type = LEFT_BRACKET;
            self.priority = 6;
        }
        else if ([value isEqualToString:@")"])
        {
            self.type = RIGHT_BRACKET;
            self.priority = 1;
        }
    }
    
    return self;
}

-(Terminal*)add:(Terminal *)leftTerminal to:(Terminal *)rightTerminal
{
    Terminal *result = [[Terminal alloc] initWithNumbers:0 and:0];
    NSNumber *realParts = [NSNumber numberWithDouble:[leftTerminal.realPart doubleValue] + [rightTerminal.realPart doubleValue]];
    NSNumber *imaginaryParts = [NSNumber numberWithDouble:[leftTerminal.imaginaryPart doubleValue] + [rightTerminal.imaginaryPart doubleValue]];
    result.realPart = realParts;
    result.imaginaryPart = imaginaryParts;
    
    return result;
}

-(Terminal*)add:(Terminal *)terminal
{
    Terminal *result = [[Terminal alloc] initWithNumbers:0 and:0];
    NSNumber *realParts = [NSNumber numberWithDouble:[self.realPart doubleValue] + [terminal.realPart doubleValue]];
    NSNumber *imaginaryParts = [NSNumber numberWithDouble:[self.imaginaryPart doubleValue] + [terminal.imaginaryPart doubleValue]];
    result.realPart = realParts;
    result.imaginaryPart = imaginaryParts;
    
    return result;
}

-(Terminal*)subtract:(Terminal *)leftTerminal from:(Terminal *)rightTerminal
{
    Terminal *result = [[Terminal alloc] initWithNumbers:0 and:0];
    NSNumber *realParts = [NSNumber numberWithDouble:[leftTerminal.realPart doubleValue] - [rightTerminal.realPart doubleValue]];
    NSNumber *imaginaryParts = [NSNumber numberWithDouble:[leftTerminal.imaginaryPart doubleValue] - [rightTerminal.imaginaryPart doubleValue]];
    result.realPart = realParts;
    result.imaginaryPart = imaginaryParts;
    
    return result;
}

-(Terminal*)subtract:(Terminal *)terminal
{
    Terminal *result = [[Terminal alloc] initWithNumbers:0 and:0];
    NSNumber *realParts = [NSNumber numberWithDouble:[self.realPart doubleValue] - [terminal.realPart doubleValue]];
    NSNumber *imaginaryParts = [NSNumber numberWithDouble:[self.imaginaryPart doubleValue] - [terminal.imaginaryPart doubleValue]];
    result.realPart = realParts;
    result.imaginaryPart = imaginaryParts;
    
    return result;
}

-(Terminal*)multiply:(Terminal *)leftTerminal with:(Terminal *)rightTerminal
{
    Terminal *result = [[Terminal alloc] initWithNumbers:0 and:0];
    NSNumber *realParts = [NSNumber numberWithDouble:([leftTerminal.realPart doubleValue] * [rightTerminal.realPart doubleValue]) - ([leftTerminal.imaginaryPart doubleValue] * [rightTerminal.imaginaryPart doubleValue])];
    NSNumber *imaginaryParts = [NSNumber numberWithDouble:([leftTerminal.imaginaryPart doubleValue] * [rightTerminal.realPart doubleValue]) + ([leftTerminal.realPart doubleValue] * [rightTerminal.imaginaryPart doubleValue])];
    result.realPart = realParts;
    result.imaginaryPart = imaginaryParts;
    
    return result;
}

-(Terminal*)multiplyBy:(Terminal *)terminal
{
    Terminal *result = [[Terminal alloc] initWithNumbers:0 and:0];
    NSNumber *realParts = [NSNumber numberWithDouble:([self.realPart doubleValue] * [terminal.realPart doubleValue]) - ([self.imaginaryPart doubleValue] * [terminal.imaginaryPart doubleValue])];
    NSNumber *imaginaryParts = [NSNumber numberWithDouble:([self.imaginaryPart doubleValue] * [terminal.realPart doubleValue]) + ([self.realPart doubleValue] * [terminal.imaginaryPart doubleValue])];
    result.realPart = realParts;
    result.imaginaryPart = imaginaryParts;
    
    return result;
}

-(Terminal*)divide:(Terminal *)leftTerminal into:(Terminal *)rightTerminal
{
    Terminal *result;
    double divider = [rightTerminal.realPart doubleValue] * [rightTerminal.realPart doubleValue] + [rightTerminal.imaginaryPart doubleValue] * [rightTerminal.imaginaryPart doubleValue];
    
    if (divider == 0) {
        result = [[Terminal alloc] initWithNumbers:0 and:0];
        result.error_msg = @"Wrong input!";
        return result;
    }
    
    result = [[Terminal alloc] initWithNumbers:[leftTerminal.realPart doubleValue] * [rightTerminal.realPart doubleValue] + [leftTerminal.imaginaryPart doubleValue] * [rightTerminal.imaginaryPart doubleValue] and:[self.realPart doubleValue] * -[rightTerminal.imaginaryPart doubleValue] + [leftTerminal.imaginaryPart doubleValue] * [rightTerminal.realPart doubleValue]];
    
    result.realPart = [NSNumber numberWithDouble:[result.realPart doubleValue] / divider];
    result.imaginaryPart = [NSNumber numberWithDouble:[result.imaginaryPart doubleValue] / divider];
    
    return result;
}

-(Terminal*)divideBy:(Terminal *)terminal
{
    Terminal *result;
    double divider = [terminal.realPart doubleValue] * [terminal.realPart doubleValue] + [terminal.imaginaryPart doubleValue] * [terminal.imaginaryPart doubleValue];
    
    if (divider == 0) {
        result = [[Terminal alloc] initWithNumbers:0 and:0];
        result.error_msg = @"Wrong input!";
        return result;
    }
    
    result = [[Terminal alloc] initWithNumbers:[self.realPart doubleValue] * [terminal.realPart doubleValue] + [self.imaginaryPart doubleValue] * [terminal.imaginaryPart doubleValue] and:[self.realPart doubleValue] * -[terminal.imaginaryPart doubleValue] + [self.imaginaryPart doubleValue] * [terminal.realPart doubleValue]];
    
    result.realPart = [NSNumber numberWithDouble:[result.realPart doubleValue] / divider];
    result.imaginaryPart = [NSNumber numberWithDouble:[result.imaginaryPart doubleValue] / divider];
    
    return result;
}

-(BOOL)isZero:(Terminal *)terminal
{
    return ([terminal.realPart doubleValue] == 0.0 && [terminal.imaginaryPart doubleValue] == 0.0) ? YES : NO;
}

-(void)printTerminal:(Terminal *)terminal
{
    NSLog(@"%@, %@i", terminal.realPart, terminal.imaginaryPart);
}

-(BOOL)isZeroSelfCheck
{
    return ([self.realPart doubleValue] == 0.0 && [self.imaginaryPart doubleValue] == 0.0) ? YES : NO;
}

+(Terminal*)logWithValue:(Terminal *)value andBase:(double)base
{
    double real, imaginary;
    
    real = log(sqrt([value.realPart doubleValue] * [value.realPart doubleValue] + [value.imaginaryPart doubleValue] * [value.imaginaryPart doubleValue]));
    
    if ([value.realPart doubleValue] != 0.0) {
        imaginary = atan([value.imaginaryPart doubleValue] / [value.realPart doubleValue]);
    }
    else
    {
        imaginary = M_PI * 0.5;
    }
    
    if ([value.realPart doubleValue] <= 0.0) {
        imaginary += M_PI;
    }
    
    return  [[[Terminal alloc] initWithNumbers:real and:imaginary] divideBy:[[Terminal alloc] initWithNumbers:log(base) and:0.0]];
}

+(Terminal*)logWithExponent:(Terminal *)exponent andBase:(double)base
{
    double complex c = [exponent.realPart doubleValue] + [exponent.imaginaryPart doubleValue] * I;
    
    double complex answer;
    answer = clog(c) / clog(base);
    
    Terminal *result = [[Terminal alloc] initWithNumbers:creal(answer) and:cimag(answer)];
    return result;
}

@end