using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace CalculatorCore {

    class Core {

        public static Terminal scientific(String exp) {
            int func_start, func_end, complex_start, complex_end, exp_length, terminals_size, complex_index, started = -1, offset = 0;
            double complex_r, complex_i;
            Terminal terminal, left, right;
            StringBuilder exp_builder = new StringBuilder();
            List < Terminal > terminals, computed_terminals;
            Stack < Terminal > rpn_stack, compute;
            Dictionary < int, int > complex_jump;
            Dictionary < int, Terminal > raw_complex_map, complex_map, function_map;

            terminals = new List < Terminal >();
            computed_terminals = new List < Terminal >();
            rpn_stack = new Stack < Terminal >();
            compute = new Stack < Terminal >();
            complex_jump = new Dictionary < int, int >();
            raw_complex_map = new Dictionary < int, Terminal >();
            complex_map = new Dictionary < int, Terminal >();
            function_map = new Dictionary < int, Terminal >();

            exp_length = exp.Length;
            for (int i = 0; i < exp_length;)
                if (exp[i] == ',') {
                    complex_start = Core.findComplexStart(exp, i);
                    complex_end = Core.findComplexEnd(exp, i);
                    complex_r = Core.scientific(exp.Substring(complex_start + 1, i - complex_start - 1)).r;
                    complex_i = Core.scientific(exp.Substring(i + 1, complex_end - i - 1)).r;
                    raw_complex_map.Add(complex_start, new Terminal(complex_r, complex_i));
                    i = complex_end + 1;
                    complex_jump.Add(complex_start, i);
                } else
                    ++i;

            for (int i = 0;  i < exp_length;) {
                if (raw_complex_map.ContainsKey(i)) {
                    complex_map.Add(exp_builder.Length, raw_complex_map[i]);
                    exp_builder.Append('~');
                    i = complex_jump[i];
                    continue;
                }
                if (Core.isSinh1(exp, i)) {
                    func_start = i + 7;
                    func_end = Core.findFuncEnd(exp, i + 6);
                    terminal = Core.sinh1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCosh1(exp, i)) {
                    func_start = i + 7;
                    func_end = Core.findFuncEnd(exp, i + 6);
                    terminal = Core.cosh1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isTgh1(exp, i)) {
                    func_start = i + 6;
                    func_end = Core.findFuncEnd(exp, i + 5);
                    terminal = Core.tgh1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCoth1(exp, i)) {
                    func_start = i + 7;
                    func_end = Core.findFuncEnd(exp, i + 6);
                    terminal = Core.coth1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isSinh(exp, i)) {
                    func_start = i + 5;
                    func_end = Core.findFuncEnd(exp, i + 4);
                    terminal = Core.sinh(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCosh(exp, i)) {
                    func_start = i + 5;
                    func_end = Core.findFuncEnd(exp, i + 4);
                    terminal = Core.cosh(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isTgh(exp, i)) {
                    func_start = i + 4;
                    func_end = Core.findFuncEnd(exp, i + 3);
                    terminal = Core.tgh(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCoth(exp, i)) {
                    func_start = i + 5;
                    func_end = Core.findFuncEnd(exp, i + 4);
                    terminal = Core.coth(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isSin1(exp, i)) {
                    func_start = i + 6;
                    func_end = Core.findFuncEnd(exp, i + 5);
                    terminal = Core.sin1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCos1(exp, i)) {
                    func_start = i + 6;
                    func_end = Core.findFuncEnd(exp, i + 5);
                    terminal = Core.cos1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isTg1(exp, i)) {
                    func_start = i + 5;
                    func_end = Core.findFuncEnd(exp, i + 4);
                    terminal = Core.tg1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCotg1(exp, i)) {
                    func_start = i + 7;
                    func_end = Core.findFuncEnd(exp, i + 6);
                    terminal = Core.cotg1(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isSin(exp, i)) {
                    func_start = i + 4;
                    func_end = Core.findFuncEnd(exp, i + 3);
                    terminal = Core.sin(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCos(exp, i)) {
                    func_start = i + 4;
                    func_end = Core.findFuncEnd(exp, i + 3);
                    terminal = Core.cos(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isTg(exp, i)) {
                    func_start = i + 3;
                    func_end = Core.findFuncEnd(exp, i + 2);
                    terminal = Core.tg(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isCotg(exp, i)) {
                    func_start = i + 5;
                    func_end = Core.findFuncEnd(exp, i + 4);
                    terminal = Core.cotg(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isMod(exp, i)) {
                    func_start = i + 4;
                    func_end = Core.findFuncEnd(exp, i + 3);
                    function_map.Add(exp_builder.Length, Core.mod(exp.Substring(func_start, func_end - func_start)));
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isLog(exp, i)) {
                    func_start = Core.findFuncStart(exp, i + 3);
                    func_end = Core.findFuncEnd(exp, i + 3);
                    function_map.Add(exp_builder.Length, Core.log(exp.Substring(func_start, func_end - func_start), Core.extractSubOrSuperScriptForward(exp, i, '$')));
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isLg(exp, i)) {
                    func_start = i + 3;
                    func_end = Core.findFuncEnd(exp, i + 2);
                    function_map.Add(exp_builder.Length, Core.lg(exp.Substring(func_start, func_end - func_start)));
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (Core.isLn(exp, i)) {
                    func_start = i + 3;
                    func_end = Core.findFuncEnd(exp, i + 2);
                    function_map.Add(exp_builder.Length, Core.ln(exp.Substring(func_start, func_end - func_start)));
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (exp[i] == '√') {
                    func_start = i + 2;
                    func_end = Core.findFuncEnd(exp, i + 1);
                    exp_builder.Length -= 3; // IT WILL ALWAYS BE %X%, So length will be always 3
                    function_map.Add(exp_builder.Length, Core.root(exp.Substring(func_start, func_end - func_start), Core.extractSuperScriptBackward(exp, i)));
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (exp[i] == '!') {
                    func_start = func_end = i;
                    for (int j = i - 1;  j >= -1;  --j)
                        if ((j != -1)  &&  ((exp[j] >= '0' && exp[j] <= '9') || exp[j] == '.'))
                            continue;
                        else {
                            func_start = j + 1;
                            break;
                        }
                    exp_builder.Length -= func_end - func_start;
                    terminal = Core.facturial(exp.Substring(func_start, func_end - func_start));
                    if (terminal.error_msg != null)
                        return terminal;
                    function_map.Add(exp_builder.Length, terminal);
                    exp_builder.Append('`');
                    i = func_end + 1;
                    continue;
                }
                if (exp[i] == 'e') {
                    exp_builder.Append("2.718281828459045");
                    ++i;
                    continue;
                }
                if (exp[i] == 'ᴨ') {
                    exp_builder.Append("3.14159265358979323846264");
                    ++i;
                    continue;
                }
                exp_builder.Append(exp[i]);
                ++i;
            }

            exp_length = exp_builder.Length;
            for (int i = 0;  i < exp_length;  ++i) {
                if ((exp_builder[i] >= '0'  &&  exp_builder[i] <= '9')  ||  exp_builder[i] == '.') {
                    if (started == -1)
                        started = i;
                    if (i + 1  !=  exp_length)
                        continue;
                    offset = 1;
                }
                if (started != -1) {
                    terminals.Add(new Terminal(exp_builder.ToString(started, i - started + offset)));
                    if (offset == 1)
                        break;
                    started = -1;
                }
                switch (exp_builder[i]) {
                    case '~':
                        terminals.Add(complex_map[i]);
                        break;
                    case '`':
                        terminals.Add(function_map[i]);
                        break;
                    default:
                        terminals.Add(new Terminal(exp_builder[i]));
                        break;
                }
            }

            terminals_size = terminals.Count;
            for (int i = 0;  i < terminals_size;  ++i) {
                if (terminals[i].type == TerminalType.NUMBER)
                    computed_terminals.Add(terminals[i]);
                else {
                    if (terminals[i].type == TerminalType.MINUS  &&  i + 1 < terminals_size) {
                        if (i == 0) {
                            terminals[i + 1].r = -terminals[i + 1].r;
                            terminals[i + 1].i = -terminals[i + 1].i;
                            continue;
                        } else if (terminals[i - 1].type != TerminalType.NUMBER) {
                            terminals[i + 1].r = -terminals[i + 1].r;
                            terminals[i + 1].i = -terminals[i + 1].i;
                            continue;
                        }
                    }

                    if (rpn_stack.Count() != 0) {
                        if (terminals[i].priority > rpn_stack.Peek().priority) {
                            Core.changePriority(terminals[i]);
                            rpn_stack.Push(terminals[i]);
                        } else {
                            terminal = rpn_stack.Pop();
                            if (terminal.type != TerminalType.LEFT_BRACKET)
                                computed_terminals.Add(terminal);
                            if (terminals[i].type != TerminalType.RIGHT_BRACKET  ||  terminal.type != TerminalType.LEFT_BRACKET)
                                --i;
                        }
                    } else {
                        Core.changePriority(terminals[i]);
                        rpn_stack.Push(terminals[i]);
                    }
                }
            }

            while (rpn_stack.Count != 0)
                computed_terminals.Add(rpn_stack.Pop());
    
                /*for (int i = 0; i < terminals.Count; ++i)
                    Console.WriteLine(terminals[i].type + "!" + terminals[i].r + "?" + terminals[i].i);
                Console.WriteLine("AFTER RPN");
                for (int i = 0; i < computed_terminals.Count; ++i)
                    Console.WriteLine(computed_terminals[i].type + "!" + computed_terminals[i].r + "?" + computed_terminals[i].i);*/

            for (int i = 0;  i < computed_terminals.Count;  ++i) {
                if (computed_terminals[i].type == TerminalType.NUMBER) {
                    compute.Push(computed_terminals[i]);
                } else {
                    right = compute.Pop();
                    if (compute.Count > 0)
                        left = compute.Pop();
                    else
                        left = new Terminal(0, 0);
                    switch (computed_terminals[i].type) {
                        case TerminalType.PLUS:
                            compute.Push(left + right);
                            break;
                        case TerminalType.MINUS:
                            compute.Push(left - right);
                            break;
                        case TerminalType.MULTIPLY:
                            compute.Push(left * right);
                            break;
                        case TerminalType.DIVIDE:
                            compute.Push(left / right);
                            break;
                        case TerminalType.POWER:
                            compute.Push(Core.power(left, right));
                            /*Complex c_left, c_right;

                            c_left = new Complex(left.r, left.i);
                            c_right = new Complex(right.r, right.i);
                            c_left = Complex.Pow(c_left, c_right);
                            
                            compute.Push(new Terminal(c_left.Real, c_left.Imaginary));*/
                            break;
                    }
                }
            }

            terminal = compute.Pop();
            //Console.WriteLine('(' + Convert.ToString(terminal.r) + ',' + Convert.ToString(terminal.i) + ')');
            return terminal;
        }

        public static Matrix matrixTranspose(Matrix a) {
            Matrix result = new Matrix(a.n, a.m);
            for (int i = 0;  i < a.n;  ++i)
                for (int j = 0;  j < a.m;  ++j)
                    result.data[i, j] = new Terminal(a.data[i, j].r, a.data[i, j].i);
            return result;
        }

        public static Matrix matrixInverse(Matrix a) {
            int right_row, size;
            Terminal zero, one, swap, divider;
            Matrix result = new Matrix(a.n, a.m << 1);

            zero = new Terminal(0.0, 0.0);
            one = new Terminal(1.0, 0.0);

            for (int i = a.n - 1;  i >= 0;  --i)
                for (int j = a.m - 1;  j >= 0;  --j)
                    result.data[i, j] = new Terminal(a.data[i, j].r, a.data[i, j].i);

            for (int i = a.n - 1;  i >= 0;  --i)
                for (int j = (a.m << 1) - 1;  j >= a.m;  --j)
                    if (i + a.m == j)
                        result.data[i, j] = one;
                    else
                        result.data[i, j] = zero;

            //UP-DOWN ELIMINATION
            for (int i = 0;  i < result.n;  ++i) {
                if (i >= result.n || i >= result.m)
                    break;

                right_row = i;
                while (right_row < result.n && result.data[right_row, i].isZero())
                    ++right_row;
                if (right_row == result.n)
                    continue;

                if (right_row != i)
                    for (int j = result.m - 1; j >= 0; --j) {
                        swap = result.data[i, j];
                        result.data[i, j] = result.data[right_row, j];
                        result.data[right_row, j] = swap;
                    }

                for (int j = i + 1;  j < result.n;  ++j) {
                    divider = result.data[j, i] / result.data[i, i];
                    for (int k = i; k < result.m; ++k) {
                        result.data[j, k] = result.data[j, k] - (result.data[i, k] * divider);
                    }
                }
            }

            //DOWN-UP ELIMINATION
            for (int i = 1;  i < result.n;  ++i) {
                if (i >= result.n || i >= result.m)
                    break;

                right_row = i;
                while (right_row >= 0 && result.data[right_row, i].isZero())
                    --right_row;
                if (right_row == -1)
                    continue;

                if (right_row != i)
                    for (int j = result.m - 1; j >= 0; --j) {
                        swap = result.data[i, j];
                        result.data[i, j] = result.data[right_row, j];
                        result.data[right_row, j] = swap;
                    }

                for (int j = i - 1;  j >= 0;  --j) {
                    divider = result.data[j, i] / result.data[i, i];
                    for (int k = i; k < result.m; ++k) {
                        result.data[j, k] = result.data[j, k] - (result.data[i, k] * divider);
                    }
                }
            }

            if (a.n < a.m)
                size = a.n;
            else
                size = a.m;

            for (int i = 0;  i < result.n;  ++i) {
                divider = one / result.data[i, i];
                for (int j = 0;  j < result.m;  ++j)
                    result.data[i, j] = result.data[i, j] * divider;
            }

            for (int i = result.n - 1;  i >= 0;  --i)
                for (int j = a.m - 1;  j >= 0;  --j)
                    result.data[i, j] = result.data[i, j + a.m];
            result.setM(a.m);

            return result;
        }

        public static Terminal matrixDeterminant(Matrix a) {
            Terminal result;
            Matrix temp;

            if (a.n != a.m) {
                result = new Terminal(0.0, 0.0);
                result.error_msg = "Matrix is now square";
                return result;
            }

            temp = Core.gauseElimination(a);
            result = new Terminal(1.0, 0.0);
            for (int i = a.n - 1;  i >= 0;  --i)
                result = result * temp.data[i, i];

            return result;
        }

        public static int matrixRank(Matrix a) {
            bool is_valid_row;
            int result = a.n;
            Matrix temp = Core.gauseElimination(a);

            for (int i = temp.n - 1;  i >= 0;  --i) {
                is_valid_row = false;
                for (int j = temp.m - 1;  j >= 0;  --j)
                    if (!temp.data[i, j].isZero()) {
                        is_valid_row = true;
                        break;
                    }
                if (!is_valid_row)
                    --result;
            }
            return result;
        }

        public static Matrix gauseElimination(Matrix a) {
            int right_row;
            Terminal divider, swap;
            Matrix result = new Matrix(a.n, a.m);

            for (int i = result.n - 1;  i >= 0;  --i)
                for (int j = result.m - 1;  j >= 0; --j)
                    result.data[i, j] = new Terminal(a.data[i, j].r, a.data[i, j].i);

            for (int i = 0;  i < result.n;  ++i) {
                if (i >= result.n || i >= result.m)
                    break;

                right_row = i;
                while (right_row < result.n  &&  result.data[right_row, i].isZero())
                    ++right_row;
                if (right_row == result.n)
                    continue;

                if (right_row != i)
                    for (int j = result.m - 1;  j >= 0;  --j) {
                        swap = result.data[i, j];
                        result.data[i, j] = result.data[right_row, j];
                        result.data[right_row, j] = swap;
                    }

                for (int j = i + 1;  j < result.n;  ++j) {
                    divider = result.data[j, i] / result.data[i, i];
                    for (int k = i;  k < result.m;  ++k) {
                        result.data[j, k] = result.data[j, k] - (result.data[i, k] * divider);
                    }
                }
            }

            return result;
        }

        public static Terminal power(Terminal base_value, Terminal power_value) {
            bool complex_method = false;
            int power_real_part_casted = 0;

            if (power_value.i != 0.0)
                complex_method = true;
            else {
                power_real_part_casted = (int) power_value.r;
                if (power_value.r - (double) power_real_part_casted != 0.0)
                    complex_method = true;
            }

            if (complex_method) {
                Complex c_power, c_base = new Complex(base_value.r, base_value.i);
                c_power = new Complex(power_value.r, power_value.i);
                c_base = Complex.Pow(c_base, c_power);

                return new Terminal(c_base.Real, c_base.Imaginary);
            }

            if (power_real_part_casted == 0)
                return new Terminal(1, 0);
            if (power_real_part_casted == 1)
                return base_value;
            if ((power_real_part_casted & 1) != 0) {
                --power_value.r;
                return base_value * Core.power(base_value, power_value);
            }
            power_value.r *= 0.5;
            Terminal arg = Core.power(base_value, power_value);
            return arg * arg;
        }

        private static Terminal sin(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Sin(terminal.r);
            else
                terminal.error_msg = "Complex number in sin() function";
            return terminal;
        }

        private static Terminal sin1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0  &&  terminal.r >= -1  &&  terminal.r <= 1)
                terminal.r = Math.Asin(terminal.r);
            else
                terminal.error_msg = "Complex number in sinˉ¹() function";
            return terminal;
        }

        private static Terminal sinh(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Sinh(terminal.r);
            else
                terminal.error_msg = "Complex number in sinh() function";
            return terminal;
        }

        private static Terminal sinh1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Log(terminal.r + Math.Sqrt(terminal.r * terminal.r + 1), Math.E);
            else
                terminal.error_msg = "Complex number in sinhˉ¹() function";
            return terminal;
        }

        private static Terminal cos(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Cos(terminal.r);
            else
                terminal.error_msg = "Complex number in cos() function";
            return terminal;
        }

        private static Terminal cos1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0  &&  terminal.r >= -1  &&  terminal.r <= 1)
                terminal.r = Math.Acos(terminal.r);
            else
                terminal.error_msg = "Complex number in cosˉ¹() function";
            return terminal;
        }

        private static Terminal cosh(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Cosh(terminal.r);
            else
                terminal.error_msg = "Complex number in cosh() function";
            return terminal;
        }

        private static Terminal cosh1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Log(terminal.r + Math.Sqrt(terminal.r * terminal.r - 1), Math.E);
            else
                terminal.error_msg = "Complex number in coshˉ¹() function";
            return terminal;
        }

        private static Terminal tg(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Tan(terminal.r);
            else
                terminal.error_msg = "Complex number in tg() function";
            return terminal;
        }

        private static Terminal tg1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Atan(terminal.r);
            else
                terminal.error_msg = "Complex number in tgˉ¹() function";
            return terminal;
        }

        private static Terminal tgh(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = Math.Tanh(terminal.r);
            else
                terminal.error_msg = "Complex number in tgh() function";
            return terminal;
        }

        private static Terminal tgh1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0  &&  terminal.r > -1  &&  terminal.r < 1)
                terminal.r = 0.5 * Math.Log((1 + terminal.r) / (1 - terminal.r), Math.E);
            else
                terminal.error_msg = "Complex number in tghˉ¹() function";
            return terminal;
        }

        private static Terminal cotg(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = 1.0 / Math.Tan(terminal.r);
            else
                terminal.error_msg = "Complex number in cotg() function";
            if (terminal.r > Double.MaxValue)
                terminal.r = Double.MaxValue;
            return terminal;
        }

        private static Terminal cotg1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = 0.5 * Math.PI - Math.Atan(terminal.r);
            else
                terminal.error_msg = "Complex number in cotgˉ¹() function";
            return terminal;
        }

        private static Terminal coth(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0)
                terminal.r = 1.0 / Math.Tanh(terminal.r);
            else
                terminal.error_msg = "Complex number in coth() function";
            if (terminal.r > Double.MaxValue)
                terminal.r = Double.MaxValue;
            return terminal;
        }

        private static Terminal coth1(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0 && (terminal.r > 1 || terminal.r < -1))
                terminal.r = 0.5 * Math.Log((terminal.r + 1) / (terminal.r - 1), Math.E);
            else
                terminal.error_msg = "Complex number in cothˉ¹() function";
            return terminal;
        }

        private static Terminal mod(String exp) {
            Terminal terminal = Core.scientific(exp);
            if (terminal.i == 0.0) {
                if (terminal.r < 0)
                    terminal.r = -terminal.r;
            } else {
                terminal.r = Math.Sqrt(terminal.r * terminal.r + terminal.i * terminal.i);
                terminal.i = 0.0;
            }
            return terminal;
        }

        private static Terminal log(String exp, String log_base) {
            Terminal terminal = Core.scientific(exp);
            Complex complex = new Complex(terminal.r, terminal.i);
            complex = Complex.Log(complex, Convert.ToDouble(log_base));
            terminal.r = complex.Real;
            terminal.i = complex.Imaginary;
            return terminal;
        }

        private static Terminal lg(String exp) {
            return Core.log(exp, "10");
        }

        private static Terminal ln(String exp) {
            return Core.log(exp, "2.718281828459045");
        }

        private static Terminal facturial(String exp) {
            double init_value;
            Terminal terminal = Core.scientific(exp);

            if (terminal.i == 0  &&  terminal.r >= 0) {
                if (((int) terminal.r) == terminal.r) {
                    init_value = terminal.r;
                    terminal.r = 1.0;
                    for (int i = 2;  i <= init_value;  ++i)
                        terminal.r *= i;
                } else
                    terminal.r = Core.gammaFunction(terminal.r + 1);
            } else
                terminal.error_msg = "Complex or negative number in Facturial() function";
            return terminal;
        }

        private static Terminal root(String exp, String root_power) {
            Terminal terminal = Core.scientific(exp);
            return Core.power(terminal, new Terminal(1.0 / Convert.ToInt32(root_power), 0.0));
        }

        private static double gammaFunction(double value) {
            int prec = 16;
            double accum;
            double[] c_space;

            c_space = new double[prec];
            double k_factor = 1.0;
            c_space[0] = Math.Sqrt(2.0 * Math.PI);
            for (int k = 1;  k < prec;  ++k) {
                c_space[k] = Math.Exp(prec - k) * Math.Pow(prec - k, k - 0.5) / k_factor;
                k_factor *= -k;
            }

            accum = c_space[0];
            for (int k = 1;  k < prec;  ++k)
                accum += c_space[k] / (value + k);
            accum *= Math.Exp(-(value + prec)) * Math.Pow(value + prec, value + 0.5);

            return accum / value;
        }

        private static bool isSinh1(String exp, int index) {
            if (index + 5 >= exp.Length)
                return false;
            return (exp[index] == 's' && exp[index + 1] == 'i' && exp[index + 2] == 'n' && exp[index + 3] == 'h'  &&  exp[index + 4] == 'ˉ' && exp[index + 5] == '¹');
        }

        private static bool isCosh1(String exp, int index) {
            if (index + 5 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 's' && exp[index + 3] == 'h' && exp[index + 4] == 'ˉ' && exp[index + 5] == '¹');
        }

        private static bool isTgh1(String exp, int index) {
            if (index + 4 >= exp.Length)
                return false;
            return (exp[index] == 't' && exp[index + 1] == 'g' && exp[index + 2] == 'h' && exp[index + 3] == 'ˉ' && exp[index + 4] == '¹');
        }

        private static bool isCoth1(String exp, int index) {
            if (index + 5 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 't' && exp[index + 3] == 'h' && exp[index + 4] == 'ˉ' && exp[index + 5] == '¹');
        }

        private static bool isSinh(String exp, int index) {
            if (index + 3 >= exp.Length)
                return false;
            return (exp[index] == 's' && exp[index + 1] == 'i' && exp[index + 2] == 'n' && exp[index + 3] == 'h');
        }

        private static bool isCosh(String exp, int index) {
            if (index + 3 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 's' && exp[index + 3] == 'h');
        }

        private static bool isTgh(String exp, int index) {
            if (index + 2 >= exp.Length)
                return false;
            return (exp[index] == 't' && exp[index + 1] == 'g' && exp[index + 2] == 'h');
        }

        private static bool isCoth(String exp, int index) {
            if (index + 3 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 't' && exp[index + 3] == 'h');
        }

        private static bool isSin1(String exp, int index) {
            if (index + 4 >= exp.Length)
                return false;
            return (exp[index] == 's' && exp[index + 1] == 'i' && exp[index + 2] == 'n' && exp[index + 3] == 'ˉ' && exp[index + 4] == '¹');
        }

        private static bool isCos1(String exp, int index) {
            if (index + 4 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 's' &&  exp[index + 3] == 'ˉ' && exp[index + 4] == '¹');
        }

        private static bool isTg1(String exp, int index) {
            if (index + 3 >= exp.Length)
                return false;
            return (exp[index] == 't' && exp[index + 1] == 'g' &&  exp[index + 2] == 'ˉ' && exp[index + 3] == '¹');
        }

        private static bool isCotg1(String exp, int index) {
            if (index + 5 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 't' && exp[index + 3] == 'g' && exp[index + 4] == 'ˉ' && exp[index + 5] == '¹');
        }

        private static bool isSin(String exp, int index) {
            if (index + 2 >= exp.Length)
                return false;
            return (exp[index] == 's' && exp[index + 1] == 'i' && exp[index + 2] == 'n');
        }

        private static bool isCos(String exp, int index) {
            if (index + 2 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 's');
        }

        private static bool isTg(String exp, int index) {
            if (index + 1 >= exp.Length)
                return false;
            return (exp[index] == 't' && exp[index + 1] == 'g');
        }

        private static bool isCotg(String exp, int index) {
            if (index + 3 >= exp.Length)
                return false;
            return (exp[index] == 'c' && exp[index + 1] == 'o' && exp[index + 2] == 't' && exp[index + 3] == 'g');
        }

        private static bool isMod(String exp, int index) {
            if (index + 2 >= exp.Length)
                return false;
            return (exp[index] == 'm' && exp[index + 1] == 'o' && exp[index + 2] == 'd');
        }

        private static bool isLog(String exp, int index) {
            if (index + 2 >= exp.Length)
                return false;
            return (exp[index] == 'l' && exp[index + 1] == 'o' && exp[index + 2] == 'g');
        }

        private static bool isLg(String exp, int index) {
            if (index + 1 >= exp.Length)
                return false;
            return (exp[index] == 'l' && exp[index + 1] == 'g');
        }

        private static bool isLn(String exp, int index) {
            if (index + 1 >= exp.Length)
                return false;
            return (exp[index] == 'l' && exp[index + 1] == 'n');
        }

        private static String extractSuperScriptBackward(String exp, int index) {
            int end_index = -1;

            for (int i = index;  i >= 0;  --i)
                if (exp[i] == '@') {
                    if (end_index != -1)
                        return exp.Substring(i + 1, end_index - i - 1);
                    else
                        end_index = i;
                }
            return "";
        }

        private static String extractSubOrSuperScriptForward(String exp, int index, char symbol) {
            int start_index = -1, exp_length = exp.Length;

            for (int i = index;  i < exp_length;  ++i)
                if (exp[i] == symbol) {
                    if (start_index != -1)
                        return exp.Substring(start_index, i - start_index);
                    else
                        start_index = i + 1;
                }
            return "";
        }

        private static int findFuncStart(String exp, int index) {
            int exp_length = exp.Length - 1;

            for (int i = index;  i < exp_length;  ++i)
                if (exp[i] == '(')
                    return i + 1;
            return index;
        }

        private static int findFuncEnd(String exp, int index) {
            int i, exp_length, bracket_count = 0;

            exp_length = exp.Length;
            for (i = index; i < exp_length;  ++i) {
                if (exp[i] == '(')
                    ++bracket_count;
                else if (exp[i] == ')') {
                    --bracket_count;
                    if (bracket_count == 0)
                        break;
                }
            }
            return i;
        }

        private static int findComplexStart(String s_builder, int index) {
            int i, bracket_count = 0;

            for (i = index;  i >= 0;  --i) {
                if (s_builder[i] == ')')
                    ++bracket_count;
                else if (s_builder[i] == '(') {
                    --bracket_count;
                    if (bracket_count == -1)
                        break;
                }
            }
            return i;
        }

        private static int findComplexEnd(String s_builder, int index) {
            int i, size, bracket_count = 0;

            size = s_builder.Length;
            for (i = index; i < size; ++i) {
                if (s_builder[i] == '(')
                    ++bracket_count;
                else if (s_builder[i] == ')') {
                    --bracket_count;
                    if (bracket_count == -1)
                        break;
                }
            }
            return i;
        }

        private static void changePriority(Terminal terminal) {
            switch (terminal.type) {
                case TerminalType.LEFT_BRACKET:
                    terminal.priority = 1;
                    break;
                case TerminalType.POWER:
                    terminal.priority = 4;
                    break;
            }
        }

    }

    class Terminal {

        public int type, priority;
        public double r, i;
        public String error_msg = null;

        public Terminal(String value) {
            this.r = Convert.ToDouble(value);
            this.i = 0.0;
            this.type = TerminalType.NUMBER;
            this.priority = 0;
        }

        public Terminal(Char value) {
            this.r = 0.0;
            this.i = 0.0;
            switch (value) {
                case '+':
                    this.type = TerminalType.PLUS;
                    this.priority = 2;
                    break;
                case '-':
                    this.type = TerminalType.MINUS;
                    this.priority = 2;
                    break;
                case 'x':
                    this.type = TerminalType.MULTIPLY;
                    this.priority = 3;
                    break;
                case '÷':
                    this.type = TerminalType.DIVIDE;
                    this.priority = 3;
                    break;
                case '^':
                    this.type = TerminalType.POWER;
                    this.priority = 5;
                    break;
                case '(':
                    this.type = TerminalType.LEFT_BRACKET;
                    this.priority = 6;
                    break;
                case ')':
                    this.type = TerminalType.RIGHT_BRACKET;
                    this.priority = 1;
                    break;
            }
        }

        public Terminal(double r_, double i_) {
            this.r = r_;
            this.i = i_;
            this.type = TerminalType.NUMBER;
        }

        public static Terminal operator +(Terminal left, Terminal right) {
            return new Terminal(left.r + right.r, left.i + right.i);
        }

        public static Terminal operator -(Terminal left, Terminal right) {
            return new Terminal(left.r - right.r, left.i - right.i);
        }

        public static Terminal operator *(Terminal left, Terminal right) {
            return new Terminal(left.r * right.r - left.i * right.i, left.r * right.i + left.i * right.r);
        }

        public static Terminal operator /(Terminal left, Terminal right) {
            double divider = right.r * right.r + right.i * right.i;
            Terminal terminal;

            terminal = new Terminal(left.r * right.r + left.i * right.i, left.r * -right.i + left.i * right.r);
            terminal.r /= divider;
            terminal.i /= divider;
            return terminal;
        }

        public bool isZero() {
            return this.r == 0  &&  this.i == 0;
        }

        public void show() {
            Console.Write("(" + r + "," + i + ") ");
        }
        
    }

    static class TerminalType {
        public const int NUMBER = 1;
        public const int PLUS = 2;
        public const int MINUS = 3;
        public const int MULTIPLY = 4;
        public const int DIVIDE = 5;
        public const int POWER = 6;
        public const int LEFT_BRACKET = 7;
        public const int RIGHT_BRACKET = 8;
    }

    class Matrix {

        public int n, m;
        public String error_msg = null;
        public Terminal[,] data;

        public Matrix(int n_, int m_) {
            this.n = n_;
            this.m = m_;
            this.data = new Terminal[this.n, this.m];
        }

        public static Matrix operator +(Matrix left, Matrix right) {
            Matrix result;

            if (left.n != right.n  ||  left.m != right.m) {
                result = new Matrix(0, 0);
                result.error_msg = "Wrong matrix size";
                return result;
            }

            result = new Matrix(left.n, left.m);
            for (int i = left.n - 1;  i >= 0;  --i)
                for (int j = left.m - 1;  i >= 0;  --j)
                    result.data[i, j] = left.data[i, j] + right.data[i, j];
            return result;
        }

        public static Matrix operator -(Matrix left, Matrix right) {
            Matrix result;

            if (left.n != right.n  ||  left.m != right.m) {
                result = new Matrix(0, 0);
                result.error_msg = "Wrong matrix size";
                return result;
            }

            result = new Matrix(left.n, left.m);
            for (int i = left.n - 1;  i >= 0;  --i)
                for (int j = left.m - 1;  i >= 0;  --j)
                    result.data[i, j] = left.data[i, j] - right.data[i, j];
            return result;
        }

        public static Matrix operator *(Matrix left, Matrix right) {
            Matrix result;

            if (left.m != right.n) {
                result = new Matrix(0, 0);
                result.error_msg = "Wrong matrix size";
                return result;
            }

            result = new Matrix(left.n, right.m);
            for (int i = 0;  i < left.n;  ++i)
                for (int j = 0; j < right.m;  ++j)
                    for (int k = 0;  k < left.m;  ++k)
                        result.data[i, j] = result.data[i, j] + (left.data[i, k] * right.data[k, j]);
            return result;
        }

        public void setN(int n_) {
            Terminal[,] old_data = this.data;

            this.data = new Terminal[n_, this.m];
            for (int i = 0;  i < n_;  ++i)
                for (int j = 0;  j < this.m;  ++j) {
                    if (i < this.n)
                        this.data[i, j] = old_data[i, j];
                    else
                        this.data[i, j] = new Terminal(0, 0);
                }
            this.n = n_;
        }

        public void setM(int m_) {
            Terminal[,] old_data = this.data;

            this.data = new Terminal[this.n, m_];
            for (int i = 0;  i < this.n;  ++i)
                for (int j = 0;  j < m_;  ++j) {
                    if (j < this.m)
                        this.data[i, j] = old_data[i, j];
                    else
                        this.data[i, j] = new Terminal(0, 0);
                }
            this.m = m_;
        }

    }

    class Program {

        public static void Main(string[] args) {
            String exp = System.IO.File.ReadAllText(@"input.txt");
            Terminal result = Core.scientific(exp);
            result.show();
            Console.WriteLine(result.error_msg);
            /*Matrix m = new Matrix(2, 2);
            m.data[0, 0] = new Terminal(2.0, 0.0);
            m.data[0, 1] = new Terminal(10.0, 0.0);
            m.data[1, 0] = new Terminal(5.0, 0.0);
            m.data[1, 1] = new Terminal(-1.0, 0.0);
            Matrix a = Core.gauseElimination(m);
            for (int i = 0; i < 2; ++i) {
                for (int j = 0; j < 2; ++j)
                    Console.Write(a.data[i, j].r + " ");
                Console.WriteLine();
            }*/
            //Console.WriteLine(Core.power(new Terminal(2, -2), new Terminal(2, 0)).i);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

    }

}
