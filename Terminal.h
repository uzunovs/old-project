//
//  Terminal.h
//  RZCalc
//
//  Created by Hristo Uzunov on 10/9/13.
//  Copyright (c) 2013 Hristo Uzunov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TerminalType.h"
#import <complex.h>

@interface Terminal : NSObject

@property (nonatomic) NSInteger type;
@property (nonatomic) NSInteger priority;
@property (nonatomic, strong) NSNumber *realPart;
@property (nonatomic, strong) NSNumber *imaginaryPart;
@property (nonatomic, strong) NSString *error_msg;
@property (nonatomic, strong) TerminalType *terminalType;

-(id)initWithString:(NSString*)value;
-(id)initWithSymbolString:(NSString*)value;
-(id)initWithNumbers:(double)n and:(double)m;
+(Terminal*)terminalWithNumbers:(double)n and:(double)m;
+(Terminal*)newTerminalWithTerminal:(Terminal*)terminal;
+(Terminal*)logWithValue:(Terminal*)value andBase:(double)base;
+(Terminal*)logWithExponent:(Terminal*)exponent andBase:(double)base;

-(Terminal*)add:(Terminal*)leftTerminal to:(Terminal*)rightTerminal;
-(Terminal*)add:(Terminal*)terminal;
-(Terminal*)subtract:(Terminal *)leftTerminal from:(Terminal *)rightTerminal;
-(Terminal*)subtract:(Terminal*)terminal;
-(Terminal*)multiply:(Terminal *)leftTerminal with:(Terminal *)rightTerminal;
-(Terminal*)multiplyBy:(Terminal*)terminal;
-(Terminal*)divide:(Terminal *)leftTerminal into:(Terminal *)rightTerminal;
-(Terminal*)divideBy:(Terminal*)terminal;
-(BOOL)isZero:(Terminal*)terminal;
-(BOOL)isZeroSelfCheck;
-(void)printTerminal:(Terminal*)terminal;

-(NSString*)description;


@end
